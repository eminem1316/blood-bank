@extends('backend.admin-master')
@section('site-title')
    {{__('Our Available Donors')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Our Available Donors Settings')}}</h4>
                        <form action="{{route('admin.valuable.donor')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="valuable_donors_section_title">{{__('Section Title')}}</label>
                                <input type="text" class="form-control"  id="valuable_donors_section_title"  value="{{get_static_option('valuable_donors_section_title')}}" name="valuable_donors_section_title" placeholder="{{__('Section Title')}}">
                            </div>
                            <div class="form-group">
                                <label for="valuable_donors_button_text">{{__('Button Text')}}</label>
                                <input type="text" class="form-control"  id="valuable_donors_button_text"  value="{{get_static_option('valuable_donors_button_text')}}" name="valuable_donors_button_text" placeholder="{{__('Button Text')}}">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
