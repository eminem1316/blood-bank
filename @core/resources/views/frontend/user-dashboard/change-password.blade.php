@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Change Password')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Change Password')}}</h2>
                        @include('backend.partials.message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('user.change.password')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="password">{{__('Old Password')}}</label>
                                <input type="password" name="old_password" placeholder="Old Password"  class="form-control" required="" aria-required="true">
                            </div>
                            <div class="form-group">
                                <label for="password">{{__('New Password')}}</label>
                                <input type="password" name="password" placeholder="New Password"  class="form-control" required="" aria-required="true">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">{{__('Confirm New Password')}}</label>
                                <input type="password" name="password_confirmation" placeholder="Confirm Password"  class="form-control" required="" aria-required="true">
                            </div>
                            <div class="form-group">
                               <button type="submit" class="submit-btn">{{__(('Change Password'))}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
