<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationProcess extends Model
{
    protected $table = 'donation_processes';
    protected $fillable = ['number','title'];
}
