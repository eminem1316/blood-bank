<?php

namespace App\Providers;

use App\Blog;
use App\DonorsCategory;
use App\Importantlink;
use App\Language;
use App\SocialIcons;
use App\SupportInfo;
use App\UsefulLink;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(191);
        $all_social_item = SocialIcons::all();
        $all_support_item = SupportInfo::all();
        $all_recent_blogs = Blog::orderBy('id','desc')->take(get_static_option('recent_post_widget_item'))->get();
        $all_useful_link = UsefulLink::all();
        $all_important_link = Importantlink::all();
        $all_language = Language::all();
       View::share([
           'all_support_item' => $all_support_item,
           'all_social_item' => $all_social_item,
           'all_recent_blogs' => $all_recent_blogs,
           'all_useful_link' => $all_useful_link,
           'all_important_link' => $all_important_link,
           'all_language' => $all_language
       ]);

    }
}
