@extends('backend.admin-master')
@section('site-title')
    {{__('Donors Page')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Donors Page Settings')}}</h4>
                        <form action="{{route('admin.donor.page')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="donor_page_title">{{__('Page Title')}}</label>
                                <input type="text" class="form-control"  id="donor_page_title" value="{{get_static_option('donor_page_title')}}" name="donor_page_title" placeholder="{{__('Page Title')}}">
                            </div>
                            <div class="form-group">
                                <label for="donor_show_number">{{__('Donor Number')}}</label>
                                <input type="text" class="form-control"  id="donor_show_number" value="{{get_static_option('donor_show_number')}}" name="donor_show_number" placeholder="{{__('Donor Number')}}">
                                <small class="text-danger">{{__('set how many number of donors you want to show in donors page')}}</small>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Donors Page Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
