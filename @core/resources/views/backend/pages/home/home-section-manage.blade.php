@extends('backend.admin-master')

@section('site-title')
    {{__('Home Page Section Manage')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Home Page Section Manage')}}</h4>
                        <form action="{{route('admin.home.section.manage')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_donor_search_section_status"><strong>{{__('Donor Search Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_donor_search_section_status"  @if(!empty(get_static_option('home_donor_search_section_status'))) checked @endif id="home_donor_search_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_blood_donation_process_section_status"><strong>{{__('Blood Donation Process Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_blood_donation_process_section_status"  @if(!empty(get_static_option('home_blood_donation_process_section_status'))) checked @endif id="home_blood_donation_process_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_available_donor_section_status"><strong>{{__('Available Donors Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_available_donor_section_status"  @if(!empty(get_static_option('home_available_donor_section_status'))) checked @endif id="home_available_donor_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_recent_donor_section_status"><strong>{{__('Recent Donors Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_recent_donor_section_status"  @if(!empty(get_static_option('home_recent_donor_section_status'))) checked @endif id="home_recent_donor_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_health_benefit_section_status"><strong>{{__('Health Benefit Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_health_benefit_section_status"  @if(!empty(get_static_option('home_health_benefit_section_status'))) checked @endif id="home_health_benefit_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_our_volunteer_section_status"><strong>{{__('Our Volunteer Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_our_volunteer_section_status"  @if(!empty(get_static_option('home_our_volunteer_section_status'))) checked @endif id="home_our_volunteer_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_counterup_section_status"><strong>{{__('Counterup Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_counterup_section_status"  @if(!empty(get_static_option('home_counterup_section_status'))) checked @endif id="home_counterup_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_testimonial_section_status"><strong>{{__('Testimonial Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_testimonial_section_status"  @if(!empty(get_static_option('home_testimonial_section_status'))) checked @endif id="home_testimonial_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_call_to_action_section_status"><strong>{{__('Call To Action Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_call_to_action_section_status"  @if(!empty(get_static_option('home_call_to_action_section_status'))) checked @endif id="home_call_to_action_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_news_update_section_status"><strong>{{__('News Update Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_news_update_section_status"  @if(!empty(get_static_option('home_news_update_section_status'))) checked @endif id="home_news_update_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_request_for_blood_section_status"><strong>{{__('Request For Blood Section Show/Hide')}}</strong></label>
                                        <label class="switch">
                                            <input type="checkbox" name="home_request_for_blood_section_status"  @if(!empty(get_static_option('home_request_for_blood_section_status'))) checked @endif id="home_request_for_blood_section_status">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

