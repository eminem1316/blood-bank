<section class="dedicated-team-area padding-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title margin-bottom-60">
                    <h2 class="title">{{__('Our Volunteer Members')}}</h2>
                    <span class="separator"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">

                <div class="our-dedicated-team-carousel owl-carousel">
                @foreach($all_volunteer as $volunteer)
                    <div class="single-team-member">
                        <div class="thumb">
                            <span class="blood-group">{{$volunteer->blood_group}}</span>
                            @if(file_exists('assets/uploads/volunteers/volunteers-pic-'.$volunteer->id.'.'.$volunteer->image))
                                <img style="max-width: 200px" src="{{asset('assets/uploads/volunteers/volunteers-pic-'.$volunteer->id.'.'.$volunteer->image)}}" alt="{{__($volunteer->name)}}">
                            @else
                                <img style="max-width: 200px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                            @endif
                        </div>
                        <div class="content">
                            <h4 class="title">{{ucfirst($volunteer->name)}}</h4>
                            <span class="designation">{{ucfirst($volunteer->designation)}}</span>
                            <ul class="social-icon">
                                <li><a href="{{$volunteer->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="{{$volunteer->twitter}}"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="{{$volunteer->instagram}}"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="{{$volunteer->linkedin}}"><i class="fab fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                 @endforeach
            </div>
        </div>
    </div>
    </div>
</section>
