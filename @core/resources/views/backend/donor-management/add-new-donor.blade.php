@extends('backend.admin-master')
@section('site-title')
    {{__('Add New Donor')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12">
        <div class="row">
            <!-- basic form start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Create New Donor')}}</h4>
                        @include('backend/partials/message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('admin.new.donor')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="{{__('Enter Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}" placeholder="{{__('Enter Email')}}">
                            </div>
                            <div class="form-group">
                                <label for="username">{{__('Username')}}</label>
                                <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}" placeholder="{{__('Enter Username')}}">
                            </div>
                            <div class="form-group">
                                <label for="father_name">{{__('Father Name')}}</label>
                                <input type="text" class="form-control" id="father_name" name="father_name"  value="{{old('father_name')}}" placeholder="{{__('Father Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="mother_name">{{__('Mother Name')}}</label>
                                <input type="text" class="form-control" id="mother_name" name="mother_name" value="{{old('mother_name')}}" placeholder="{{__('Mother Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="present_address">{{__('Present Address')}}</label>
                                <textarea class="form-control" id="present_address" name="present_address" placeholder="{{__('Present Address')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="permanent_address">{{__('Permanent Address')}}</label>
                                <textarea class="form-control" id="permanent_address" name="permanent_address" placeholder="{{__('Permanent Address')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="mobile">{{__('Mobile')}}</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__('Mobile')}}">
                            </div>
                            <div class="form-group">
                                <label for="birthday">{{__('Birthday')}}</label>
                                <input type="date" class="form-control" id="birthday" name="birthday" placeholder="{{__('Birthday')}}">
                            </div>
                            <div class="form-group">
                                <label for="blood_group">{{__('Blood Group')}}</label>
                                <select name="blood_group"  id="blood_group" class="form-control">
                                    <option value="o+">O+</option>
                                    <option value="o-">O-</option>
                                    <option value="b+">B+</option>
                                    <option value="b-">B-</option>
                                    <option value="a+">A+</option>
                                    <option value="a-">A-</option>
                                    <option value="ab+">AB+</option>
                                    <option value="ab-">AB-</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="areas_id">{{__('Donor Area')}}</label>
                                <select name="areas_id" class="form-control" id="areas_id">
                                    @foreach($all_donor_area as $data)
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="password">{{__('Password')}}</label>
                                <input type="password" class="form-control" id="password" name="password" value="{{old('password')}}" placeholder="{{__('Enter Password')}}">
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" id="image">
                                <label class="custom-file-label" for="image">{{__('Image')}}</label>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Donor')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
