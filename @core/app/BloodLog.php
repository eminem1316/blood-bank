<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloodLog extends Model
{
    protected $table = 'blood_logs';
    protected $fillable = [
        'name',
        'donors_id',
        'blood_group',
        'receiver_name',
        'receiver_address',
        'receiver_mobile'
    ];

    public function donor(){
        return $this->belongsTo('App\Donor','donors_id');
    }
}
