<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Login')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="contact-page-form-wrap login-page">
                            <h2 class="title"><?php echo e(__('Login')); ?></h2>
                            <?php echo $__env->make('backend.partials.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form action="<?php echo e(route('login')); ?>" method="post" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="form-group">
                                    <input type="text" name="username" placeholder="Your Username" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Your Password" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="<?php echo e(__('Login')); ?>" class="submit-btn register-as-donor">
                                </div>
                                <div class="extra-links form-group">
                                    <a href="<?php echo e(route('user.forget.password')); ?>"><?php echo e(__('Forget Password ?')); ?></a>
                                    <a href="<?php echo e(route('register')); ?>"><?php echo e(__('Don\'t Have Account ?')); ?></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/login.blade.php ENDPATH**/ ?>