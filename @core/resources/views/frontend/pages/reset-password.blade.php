@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Reset Password')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="contact-page-form-wrap login-page">
                            <h2 class="title">{{__('Reset Password')}}</h2>
                            @include('backend.partials.message')
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('user.password.reset')}}" method="post" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="token" value="{{$token}}">
                                <div class="form-group">
                                    <label for="username">{{__('Your Username Or Email')}}</label>
                                    <input type="text" name="username" id="username" readonly value="{{$username}}" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <label for="password">{{__('Password')}}</label>
                                    <input type="password" name="password" id="password" placeholder="{{__('Enter Password')}}" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">{{__('Confirm Password')}}</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="{{__('Confirm Password')}}" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="{{__('Reset Password')}}" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
