<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Donors Search')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- header bottom start  -->
    <div class="header-bottom-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="blood-search-warpper">
                         <?php if($errors->any()): ?>
                            <ul class="alert alert-danger">
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        <?php endif; ?>
                        <form action="<?php echo e(route('frontend.search.donors')); ?>" class="blood-search-form" method="get">
                            <?php echo csrf_field(); ?>
                            <ul class="fields-list">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control nice-select wide" name="area">
                                            <option value=""><?php echo e(__('Areas')); ?></option>
                                            <?php $__currentLoopData = $all_areas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php if($area == $data->id): ?> selected  <?php endif; ?> value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group ">
                                        <select class="form-control nice-select wide" name="blood_group">
                                            <option value=""><?php echo e(__('Blood Group')); ?></option>
                                            <option <?php if($blood_group == 'o+'): ?> selected  <?php endif; ?> value="o+">O+</option>
                                            <option <?php if($blood_group == 'o-'): ?> selected  <?php endif; ?> value="o-">O-</option>
                                            <option <?php if($blood_group == 'b+'): ?> selected  <?php endif; ?> value="b+">B+</option>
                                            <option <?php if($blood_group == 'b-'): ?> selected  <?php endif; ?> value="b-">B-</option>
                                            <option <?php if($blood_group == 'a+'): ?> selected  <?php endif; ?> value="a+">A+</option>
                                            <option <?php if($blood_group == 'a-'): ?> selected  <?php endif; ?> value="a-">A-</option>
                                            <option <?php if($blood_group == 'ab+'): ?> selected  <?php endif; ?> value="ab+">AB+</option>
                                            <option <?php if($blood_group == 'ab-'): ?> selected  <?php endif; ?> value="ab-">AB-</option>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <input type="submit" value="Search Donor" class="submit-btn">
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header bottom end -->

    <!-- our dedicated team area start  -->
    <section class="dedicated-team-area padding-120 ">
        <div class="container">
            <div class="row">
                <?php if(count($all_donors) < 1): ?>
                    <div class="col-lg-12">
                        <div class="alert alert-danger"><?php echo e(__('Sorry No Donor Available !!')); ?></div>
                    </div>
                <?php endif; ?>
                <?php $__currentLoopData = $all_donors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-donors-item margin-bottom-30">
                            <div class="thumb">
                                <?php if(file_exists('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)): ?>
                                    <img src="<?php echo e(asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)); ?>" alt="<?php echo e(__($data->name)); ?>">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('assets/uploads/no-image.png')); ?>" alt="no image available">
                                <?php endif; ?>
                            </div>
                            <div class="content">
                                <a href="<?php echo e(route('frontend.donors.profile',['id' => $data->id,'slug' => Str::slug($data->user->name)])); ?>"> <h4 class="title"><?php echo e($data->user->name); ?></h4></a>
                                <span class="blood-group"><?php echo e(__('Blood Group:')); ?> <strong><?php echo e(strtoupper($data->blood_group)); ?></strong></span>
                                <span class="total-donate"><?php echo e(__('Total Donate:')); ?> <strong><?php echo e(get_total_donate($data->id)); ?></strong> <?php echo e(__('Times')); ?></span>
                            </div>
                        </div>
                 </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-12">
                    <nav class="pagination-wrapper" aria-label="Page navigation ">
                        <?php echo e($all_donors->links()); ?>

                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- our dedicated team area end -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/search.blade.php ENDPATH**/ ?>