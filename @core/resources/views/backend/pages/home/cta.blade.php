@extends('backend.admin-master')

@section('site-title')
    {{__('Call To Action')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Call To Action Settings')}}</h4>
                        <form action="{{route('admin.cta')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="home_cta_title">{{__('Title')}}</label>
                                <input type="text" name="home_cta_title" value="{{get_static_option('home_cta_title')}}" class="form-control" id="home_cta_title">
                            </div>
                            <div class="form-group">
                                <label for="home_cta_text">{{__('Button Text')}}</label>
                                <input type="text" name="home_cta_text" class="form-control" value="{{get_static_option('home_cta_text')}}" id="home_cta_text">
                            </div>
                            <div class="form-group">
                                <label for="home_cta_btn_url">{{__('Button URL')}}</label>
                                <input type="text" name="home_cta_btn_url" class="form-control" value="{{get_static_option('home_cta_btn_url')}}" id="home_cta_btn_url">
                            </div>
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('home_cta_bg')))
                                <img  style="max-width: 200px; margin-top:20px;" src="{{asset('assets/uploads/'.get_static_option('home_cta_bg'))}}" alt="">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="home_cta_bg">{{__('Background Image')}}</label>
                                <input type="file" class="form-control"  id="home_cta_bg" name="home_cta_bg">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

