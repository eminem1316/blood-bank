@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Donor Dashboard')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="donor-sidebar">
                        <div class="profile-img">
                            @if(file_exists('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image))
                                <img src="{{asset('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image)}}" alt="{{auth()->user()->name}}">
                                @php $img_url = asset('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image) @endphp
                            @else
                                <img src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                @php $img_url = asset('assets/uploads/no-image.png') @endphp
                            @endif
                        </div>
                        <div class="donor-info">
                            <h4 class="username"><strong>{{__('Username:')}}</strong> {{auth()->user()->username}}</h4>
                            <h4 class="email"><strong>{{__('Email:')}}</strong> {{auth()->user()->email}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Profile Information')}}</h2>
                        <ul>
                            <li><strong>{{__('Name:')}} </strong> <span class="right">{{auth()->user()->name}}</span></li>
                            <li><strong>{{__('Birth Date:')}} </strong> <span class="right">{{auth()->user()->donor->birthday}}</span></li>
                            <li><strong>{{__('Blood Group:')}} </strong> <span class="right">{{strtoupper(auth()->user()->donor->blood_group)}}</span></li>
                            <li><strong>{{__('Total Blood Donate:')}} </strong> <span class="right">{{auth()->user()->donor->blood_donate_count}} {{__('Times')}}</span></li>
                            <li><strong>{{__('Mobile:')}} </strong> <span class="right">{{auth()->user()->donor->mobile}}</span></li>
                            <li><strong>{{__('Father Name:')}} </strong> <span class="right">{{auth()->user()->donor->father_name}}</span></li>
                            <li><strong>{{__('Mother Name:')}} </strong> <span class="right">{{auth()->user()->donor->mother_name}}</span></li>
                            <li><strong>{{__('Present Address:')}} </strong> <span class="right">{{auth()->user()->donor->present_address}}</span></li>
                            <li><strong>{{__('Permanent Address:')}} </strong> <span class="right">{{auth()->user()->donor->permanent_address}}</span></li>
                            <li><strong>{{__('Area:')}} </strong> <span class="right">{{auth()->user()->donor->area->name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
