<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Donor Details')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $__env->make('backend.partials.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="col-lg-4">
                    <div class="donor-sidebar">
                        <div class="profile-img">
                            <?php if(file_exists('assets/uploads/donors/donor-pic-'.$donor_info->id.'.'.$donor_info->image)): ?>
                                <img src="<?php echo e(asset('assets/uploads/donors/donor-pic-'.$donor_info->id.'.'.$donor_info->image)); ?>" alt="<?php echo e($donor_info->user->name); ?>">
                            <?php else: ?>
                                <img src="<?php echo e(asset('assets/uploads/no-image.png')); ?>" alt="no image available">
                            <?php endif; ?>
                        </div>
                        <div class="donor-info">
                            <h4 class="username"> <?php echo e($donor_info->user->name); ?></h4>
                            <div class="btn-wrapper">
                                <a href="#" data-toggle="modal" data-target="#donor_contact" class="boxed-btn"><?php echo e(__("Contact")); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title"><?php echo e(__('Donor Information')); ?></h2>
                        <ul>
                            <li><strong><?php echo e(__('Birth Date:')); ?> </strong> <span class="right"><?php echo e($donor_info->birthday); ?></span></li>
                            <li><strong><?php echo e(__('Blood Group:')); ?> </strong> <span class="right"><?php echo e(strtoupper($donor_info->blood_group)); ?></span></li>
                            <li><strong><?php echo e(__('Total Blood Donate:')); ?> </strong> <span class="right"><?php echo e($donor_info->blood_donate_count); ?> <?php echo e(__("Times")); ?></span></li>
                            <li><strong><?php echo e(__('Mobile:')); ?> </strong> <span class="right"><?php echo e($donor_info->mobile); ?></span></li>
                            <li><strong><?php echo e(__('Father Name:')); ?> </strong> <span class="right"><?php echo e($donor_info->father_name); ?></span></li>
                            <li><strong><?php echo e(__('Mother Name:')); ?> </strong> <span class="right"><?php echo e($donor_info->mother_name); ?></span></li>
                            <li><strong><?php echo e(__('Present Address:')); ?> </strong> <span class="right"><?php echo e($donor_info->present_address); ?></span></li>
                            <li><strong><?php echo e(__('Permanent Address:')); ?> </strong> <span class="right"><?php echo e($donor_info->permanent_address); ?></span></li>
                            <li><strong><?php echo e(__('Area:')); ?> </strong> <span class="right"><?php echo e($donor_info->area->name); ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="donor_contact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo e(__('Contact With Donor')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo e(route('frontend.donor.contact')); ?>" method="post">
                <div class="modal-body">
                    <?php echo csrf_field(); ?>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <input type="hidden" name="user_id" value="<?php echo e($donor_info->user->id); ?>">
                    <div class="form-group">
                        <label for="subject"><?php echo e(__('Subject')); ?></label>
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="<?php echo e(__("Subject")); ?>">
                    </div>
                    <div class="form-group">
                        <label for="email"><?php echo e(__('Email')); ?></label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="<?php echo e(__("Email")); ?>">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" class="form-control max-height-150" cols="30" rows="10" placeholder="<?php echo e(__("Message")); ?>"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__("Close")); ?></button>
                    <button type="submit" class="btn btn-primary"><?php echo e(__("Send Mail")); ?></button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/donor-details.blade.php ENDPATH**/ ?>