@extends('backend.admin-master')
@section('site-title')
    {{__('Blood Donation Process Item')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Blood Donation Process Settings')}}</h4>
                        <form action="{{route('admin.blood.donation')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="blood_donation_process_section_title">{{__('Section Title')}}</label>
                                <input type="text" class="form-control"  id="blood_donation_process_section_title"  value="{{get_static_option('blood_donation_process_section_title')}}" name="blood_donation_process_section_title" placeholder="{{__('Section Title')}}">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Process')}}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('All Blood Donation Process Items')}}</h4>
                        <table class="table table-default">
                            <thead>
                            <th>{{__('ID')}}</th>
                            <th>{{__('Number')}}</th>
                            <th>{{__('Title')}}</th>
                            <th>{{__('Action')}}</th>
                            </thead>
                            <tbody>
                            @foreach($all_process as $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->number}}</td>
                                    <td>{{$data->title}}</td>
                                    <td>
                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1"
                                           role="button"
                                           data-toggle="popover"
                                           data-trigger="focus"
                                           data-html="true"
                                           title=""
                                           data-content="
                                               <h6>Are you sure to delete this process item?</h6>
                                               <form method='post' action='{{route('admin.blood.donation.delete',$data->id)}}'>
                                               <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                               <br>
                                                <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                </form>
                                                ">
                                            <i class="ti-trash"></i>
                                        </a>
                                        <a href="#"
                                           data-toggle="modal"
                                           data-target="#blood_donation_process_edit_modal_form"
                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 blood_donation_process_edit_btn"
                                           data-id="{{$data->id}}"
                                           data-number="{{$data->number}}"
                                           data-title="{{$data->title}}"
                                        >
                                            <i class="ti-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('New Process')}}</h4>
                        <form action="{{route('admin.blood.donation.new')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="number">{{__('Number')}}</label>
                                <input type="text" class="form-control"  id="number"  name="number" placeholder="{{__('Number')}}">
                            </div>
                            <div class="form-group">
                                <label for="title">{{__('Title')}}</label>
                                <input type="text" class="form-control"  id="title"  name="title" placeholder="{{__('Title')}}">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Process')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="blood_donation_process_edit_modal_form" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Edit Process Item')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <form action="{{route('admin.blood.donation.update')}}" id="blood_donation_process_edit_modal_form"  method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="process_id" value="">
                        <div class="form-group">
                            <label for="edit_number">{{__('Number')}}</label>
                            <input type="text" class="form-control"  id="edit_number"  name="number" placeholder="{{__('Number')}}">
                        </div>
                        <div class="form-group">
                            <label for="edit_title">{{__('Title')}}</label>
                            <input type="text" class="form-control"  id="edit_title"  name="title" placeholder="{{__('Title')}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save Changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script>
        $(document).ready(function () {
            $(document).on('click','.blood_donation_process_edit_btn',function(){
                var el = $(this);
                var id = el.data('id');
                var number = el.data('number');
                var title = el.data('title');
                var action = el.data('action');

                var form = $('#blood_donation_process_edit_modal_form');
                form.find('#process_id').val(id);
                form.find('#edit_number').val(number);
                form.find('#edit_title').val(title);
            });
        });
    </script>
@endsection
