<?php

namespace App\Http\Controllers;

use App\Counterup;
use Illuminate\Http\Request;

class CounterUpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $all_contuerup = Counterup::all();
        return view('backend.pages.counterup')->with([
            'all_counterup' => $all_contuerup
        ]);
    }
    public function store(Request $request){
        $this->validate($request,[
            'icon' => 'required|string',
            'title' => 'required|string|max:191',
            'number' => 'required|string|max:191'
        ]);

        Counterup::create($request->all());

        return redirect()->back()->with(['msg' => 'New Counterup Item Added....','type' => 'success']);
    }

    public function update(Request $request){
        $this->validate($request,[
            'icon' => 'required|string',
            'title' => 'required|string|max:191',
            'number' => 'required|string|max:191'
        ]);

        Counterup::find($request->id)->update($request->all());

        return redirect()->back()->with(['msg' => 'Counterup Item Updated....','type' => 'success']);
    }

    public function delete(Request $request, $id){
        Counterup::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Counterup Item Deleted....','type' => 'danger']);
    }
}
