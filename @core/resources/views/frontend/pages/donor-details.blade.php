@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Donor Details')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('backend.partials.message')
                </div>
                <div class="col-lg-4">
                    <div class="donor-sidebar">
                        <div class="profile-img">
                            @if(file_exists('assets/uploads/donors/donor-pic-'.$donor_info->id.'.'.$donor_info->image))
                                <img src="{{asset('assets/uploads/donors/donor-pic-'.$donor_info->id.'.'.$donor_info->image)}}" alt="{{$donor_info->user->name}}">
                            @else
                                <img src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                            @endif
                        </div>
                        <div class="donor-info">
                            <h4 class="username"> {{$donor_info->user->name}}</h4>
                            <div class="btn-wrapper">
                                <a href="#" data-toggle="modal" data-target="#donor_contact" class="boxed-btn">{{__("Contact")}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Donor Information')}}</h2>
                        <ul>
                            <li><strong>{{__('Birth Date:')}} </strong> <span class="right">{{$donor_info->birthday}}</span></li>
                            <li><strong>{{__('Blood Group:')}} </strong> <span class="right">{{strtoupper($donor_info->blood_group)}}</span></li>
                            <li><strong>{{__('Total Blood Donate:')}} </strong> <span class="right">{{$donor_info->blood_donate_count}} {{__("Times")}}</span></li>
                            <li><strong>{{__('Mobile:')}} </strong> <span class="right">{{$donor_info->mobile}}</span></li>
                            <li><strong>{{__('Father Name:')}} </strong> <span class="right">{{$donor_info->father_name}}</span></li>
                            <li><strong>{{__('Mother Name:')}} </strong> <span class="right">{{$donor_info->mother_name}}</span></li>
                            <li><strong>{{__('Present Address:')}} </strong> <span class="right">{{$donor_info->present_address}}</span></li>
                            <li><strong>{{__('Permanent Address:')}} </strong> <span class="right">{{$donor_info->permanent_address}}</span></li>
                            <li><strong>{{__('Area:')}} </strong> <span class="right">{{$donor_info->area->name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="donor_contact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Contact With Donor')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('frontend.donor.contact')}}" method="post">
                <div class="modal-body">
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="user_id" value="{{$donor_info->user->id}}">
                    <div class="form-group">
                        <label for="subject">{{__('Subject')}}</label>
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="{{__("Subject")}}">
                    </div>
                    <div class="form-group">
                        <label for="email">{{__('Email')}}</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="{{__("Email")}}">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" class="form-control max-height-150" cols="30" rows="10" placeholder="{{__("Message")}}"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__("Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{__("Send Mail")}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
