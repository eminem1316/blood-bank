<div class="coutnerup-area red-bg padding-80">
    <div class="container">
        <div class="row">
            <?php $__currentLoopData = $all_counterup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3 col-md-6">
                <div class="single-counterup-item">
                    <div class="icon">
                        <i class="<?php echo e($data->icon); ?>"></i>
                    </div>
                    <div class="content">
                        <span class="count-num"><?php echo e($data->number); ?></span>
                        <h4 class="title"><?php echo e($data->title); ?></h4>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/partials/counterup.blade.php ENDPATH**/ ?>