<?php

namespace App\Http\Controllers;

use App\BloodLog;
use App\Donor;
use Illuminate\Http\Request;

class BloodLogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function all_blood_logs(){
        $all_blood_logs = BloodLog::all();
        return view('backend.blood-logs.all-blood-logs')->with(['all_blood_logs'=>$all_blood_logs]);
    }
    public function new_blood_log(){
        return view('backend.blood-logs.add-new-log');
    }
    public function store_new_blood_log(Request $request){
        $this->validate($request,[
            'blood_group' => 'required|string',
            'donors_id' => 'required',
            'name' => 'required|max:190',
            'receiver_address' => 'required',
            'receiver_mobile' => 'required',
            'receiver_name' => 'required'
        ]);
        $donor_details = Donor::find($request->donors_id);
        $donor_blood_donate_count = $donor_details->blood_donate_count + 1;
        Donor::find($request->donors_id)->update(['blood_donate_count' => $donor_blood_donate_count]);
        BloodLog::create($request->all());

        return redirect()->back()->with(['msg'=> 'New Blood Log Added','type' => 'success']);
    }

    public function update_blood_log(Request $request){
        $this->validate($request,[
            'receiver_name' => 'required|string|max:190',
            'receiver_address' => 'required|string',
            'receiver_mobile' => 'required|string|max:191',
        ]);
        BloodLog::where('id',$request->blood_log_id)->update(
            [
                'receiver_name' => $request->receiver_name,
                'receiver_address' => $request->receiver_address,
                'receiver_mobile' => $request->receiver_mobile
            ]
        );
        return redirect()->back()->with(['msg'=> 'Blood Log Edited','type' => 'success']);
    }
    public function delete_blood_log(Request $request,$id){
        BloodLog::find($id)->delete();
        return redirect()->back()->with(['msg'=> 'Blood Log Deleted','type' => 'danger']);
    }
}
