
<footer class="footer-area">
    <div class="footer-top-area padding-top-100 padding-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-top-area-inner">
                        <div class="left-content-area">
                            <ul class="contact-info">
                                <?php $__currentLoopData = $all_support_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li >
                                        <div class="single-contact-info">
                                            <div class="icon">
                                                <i class="<?php echo e($data->icon); ?>"></i>
                                            </div>
                                            <div class="content">
                                                <h4 class="title"><?php echo e($data->title); ?></h4>
                                                <span class="detials"><?php echo e($data->details); ?></span>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <div class="right-content-area">
                            <div class="share-widget">
                                <h4 class="title"><?php echo e(__('Let\'s Connect')); ?></h4>
                                <ul class="social-share">
                                    <?php $__currentLoopData = $all_social_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e($data->url); ?>"><i class="<?php echo e($data->icon); ?>"></i></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="widget footer-widget about_widget">
                        <div class="footer-logo">
                            <?php if(file_exists('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo'))): ?>
                                <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo'))); ?>" alt=" <?php echo e(__("Footer Logo")); ?>"></a>
                            <?php endif; ?>
                        </div>
                        <p><?php echo e(get_static_option('about_widget_description')); ?></p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="widget footer-widget">
                        <h2 class="widget-title"><?php echo e(get_static_option('useful_link_widget_title')); ?></h2>
                        <ul class="pages-list">
                            <?php $__currentLoopData = $all_useful_link; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($data->url); ?>"><?php echo e($data->title); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="widget footer-widget">
                        <h2 class="widget-title"><?php echo e(get_static_option('recent_post_widget_title')); ?></h2>
                        <ul class="recent_post_item">
                            <?php $__currentLoopData = $all_recent_blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="single-recent-post-item">
                                    <div class="thumb">
                                        <?php if(file_exists('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)): ?>
                                            <img src="<?php echo e(asset('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)); ?>" alt="<?php echo e($data->title); ?>">
                                        <?php endif; ?>
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="<?php echo e(route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])); ?>"><?php echo e($data->title); ?></a></h4>
                                        <span class="times"><?php echo e($data->created_at->diffForHumans()); ?></span>
                                    </div>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class=" widget footer-widget">
                        <h2 class="widget-title"><?php echo e(get_static_option('important_link_widget_title')); ?></h2>
                        <ul class="pages-list">
                            <?php $__currentLoopData = $all_important_link; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($data->url); ?>"><?php echo e($data->title); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        $footer_copyright_text = get_static_option('site_footer_copyright');
                        $footer_copyright_text = str_replace('{copy}','&copy;',$footer_copyright_text);
                        $footer_copyright_text = str_replace('{year}',date('Y'),$footer_copyright_text);
                    ?>
                    <?php echo $footer_copyright_text; ?>

                </div>
            </div>
        </div>
    </div>
</footer>
<?php if(request()->path() != 'blood-request'): ?>
<div class="request-for-blood">
    <a href="<?php echo e(route('frontend.blood.request')); ?>" target="_blank" class="request-link"><i class="fas fa-tint"></i><?php echo e(__('Request For Blood')); ?></a>
</div>
<?php endif; ?>
<!-- All scripts -->
<script src="<?php echo e(asset('assets/frontend/js/jquery-3.4.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/jquery-migrate-3.1.0.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/jquery.waypoints.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/jquery.rcounter.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/jquery.nice-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/frontend/js/main.js')); ?>"></script>
<script>
    (function($){
       "use strict";
       $(document).ready(function(){
           $(document).on('change','#langchange',function(e){
               $.ajax({
                   url : "<?php echo e(route('frontend.langchange')); ?>",
                   type: "GET",
                   data:{
                       'lang' : $(this).val()
                   },
                   success:function (data) {
                       location.reload();
                   }
               })
           });
       });
    }(jQuery));
</script>
<?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/partials/footer.blade.php ENDPATH**/ ?>