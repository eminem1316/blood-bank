@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 0 !important;
        }
        div.dataTables_wrapper div.dataTables_length select {
            width: 60px;
            display: inline-block;
        }
    </style>
@endsection
@section('site-title')
    {{__('All Volunteer')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    @include('backend/partials/message')
                                    <h4 class="header-title">{{__('All Volunteer')}}</h4>
                                    <div class="data-tables datatable-primary">
                                        <table id="all_volunteer_table" class="text-center">
                                            <thead class="text-capitalize">
                                            <tr>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Image')}}</th>
                                                <th>{{__('Mobile')}}</th>
                                                <th>{{__('Blood Group')}}</th>
                                                <th>{{__('Designation')}}</th>
                                                <th>{{__('Action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($all_volunteer as $data)
                                                @php $img_url = null;  @endphp
                                                <tr>
                                                    <td>{{$data->id}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <td>
                                                        @if(file_exists('assets/uploads/volunteers/volunteers-pic-'.$data->id.'.'.$data->image))
                                                            <img style="max-width: 80px" src="{{asset('assets/uploads/volunteers/volunteers-pic-'.$data->id.'.'.$data->image)}}" alt="{{__($data->name)}}">
                                                            @php $img_url = asset('assets/uploads/volunteers/volunteers-pic-'.$data->id.'.'.$data->image) @endphp
                                                        @else
                                                            <img style="max-width: 80px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                                            @php $img_url = asset('assets/uploads/no-image.png') @endphp
                                                        @endif
                                                    </td>
                                                    <td>{{$data->mobile}}</td>
                                                    <td>{{strtoupper($data->blood_group)}}</td>
                                                    <td>{{$data->designation}}</td>
                                                    <td>
                                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="" data-content="
                                                       <h6>Are you sure to delete this Volunteer Information?</h6>
                                                       <form method='post' action='{{route('admin.volunteer.delete',$data->id)}}'>
                                                       <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                                       <br>
                                                        <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                        </form>
                                                        " data-original-title="">
                                                            <i class="ti-trash"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="{{$data->id}}"
                                                           data-name="{{$data->name}}"
                                                           data-email="{{$data->email}}"
                                                           data-mobile="{{$data->mobile}}"
                                                           data-designation="{{$data->designation}}"
                                                           data-facebook="{{$data->facebook}}"
                                                           data-twitter="{{$data->twitter}}"
                                                           data-instagram="{{$data->instagram}}"
                                                           data-linkedin="{{$data->linkedin}}"
                                                           data-bloodGroup="{{$data->blood_group}}"
                                                           data-image="{{$img_url}}"
                                                           data-toggle="modal"
                                                           data-target="#volunteer_edit_modal"
                                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 volunteer_edit_btn"
                                                        >
                                                            <i class="ti-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Primary table end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="volunteer_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Volunteer Edit Details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('admin.volunteer.update')}}" id="volunteer_edit_modal_form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="volunteer_id" id="volunteer_id">
                        <div class="form-group">
                            <label for="name">{{__('Name')}}</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{__('Enter Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="email">{{__('Email')}}</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="{{__('Email')}}">
                        </div>
                        <div class="form-group">
                            <label for="designation">{{__('Designation')}}</label>
                            <input type="text" class="form-control" id="designation" name="designation" placeholder="{{__('Designation')}}">
                        </div>
                        <div class="form-group">
                            <label for="mobile">{{__('Mobile')}}</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__('Mobile')}}">
                        </div>
                        <div class="form-group">
                            <label for="facebook">{{__('Facebook')}}</label>
                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="{{__('Facebook')}}">
                        </div>
                        <div class="form-group">
                            <label for="twitter">{{__('Twitter')}}</label>
                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="{{__('Twitter')}}">
                        </div>
                        <div class="form-group">
                            <label for="instagram">{{__('Instagram')}}</label>
                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="{{__('Instagram')}}">
                        </div>
                        <div class="form-group">
                            <label for="linkedin">{{__('Linkedin')}}</label>
                            <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="{{__('Linkedin')}}">
                        </div>
                        <div class="form-group">
                            <label for="blood_group">{{__('Blood Group')}}</label>
                            <select name="blood_group"  id="blood_group" class="form-control">
                                <option value="o+">O+</option>
                                <option value="o-">O-</option>
                                <option value="b+">B+</option>
                                <option value="b-">B-</option>
                                <option value="a+">A+</option>
                                <option value="a-">A-</option>
                                <option value="ab+">AB+</option>
                                <option value="ab-">AB-</option>
                            </select>
                        </div>
                        <img src="" id="volunteer_edit_modal_prev_img" style="width:80px; margin-bottom: 10px;" alt="edit volunteer">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="image" id="image">
                            <label class="custom-file-label" for="image">{{__('Image')}}</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_volunteer_table').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
            $(document).on('click','.volunteer_approve_btn',function(e){
                e.preventDefault();
                $(this).next('form').submit();
            });

            $(document).on('click','.volunteer_edit_btn',function(e){
                e.preventDefault();
                var form = $('#volunteer_edit_modal_form');
                var el = $(this);
                form.find('#volunteer_id').val(el.data('id'));
                form.find('#name').val(el.data('name'));
                form.find('#email').val(el.data('email'));
                form.find('#designation').val(el.data('designation'));
                form.find('#facebook').val(el.data('facebook'));
                form.find('#twitter').val(el.data('twitter'));
                form.find('#instagram').val(el.data('instagram'));
                form.find('#linkedin').val(el.data('linkedin'));
                form.find('#mobile').val(el.data('mobile'));
                form.find('#blood_group').val(el.data('bloodgroup'));
                $('#volunteer_edit_modal_prev_img').attr('src',el.data('image'));
            })

        } );
    </script>
@endsection
