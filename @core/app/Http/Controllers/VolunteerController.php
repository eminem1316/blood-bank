<?php

namespace App\Http\Controllers;

use App\Volunteer;
use Illuminate\Http\Request;

class VolunteerController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function all_volunteer(){
        $all_volunteer = Volunteer::all();
        return view('backend.volunteer-management.all-volunteer')->with(
            [
                'all_volunteer' => $all_volunteer
            ]
        );
    }

    public function new_volunteer(){
        return view('backend.volunteer-management.add-new-volunteer');
    }
    public function store_volunteer(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'mobile' => 'required|string|max:191',
            'designation' => 'required|string|max:191',
            'email' => 'string|max:191',
            'facebook' => 'string|nullable|max:191',
            'twitter' => 'string|nullable|max:191',
            'instagram' => 'string|nullable|max:191',
            'linkedin' => 'string|nullable|max:191',
            'blood_group' => 'required|string',
            'image' => 'mimes:jpg,jpeg,png|max:2060',
        ]);

        $id = Volunteer::create([
            'name' => $request->name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'designation' => $request->designation,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'blood_group' => $request->blood_group
        ])->id;

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'volunteers-pic-'.$id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/volunteers',$image_name);
                Volunteer::where('id',$id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'New volunteer Added Success','type' => 'success']);
    }

    public function update_volunteer(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'mobile' => 'nullable|string|max:191',
            'blood_group' => 'required|string',
            'designation' => 'required|string',
            'email' => 'required|string',
            'facebook' => 'nullable|string',
            'twitter' => 'nullable|string',
            'instagram' => 'nullable|string',
            'linkedin' => 'nullable|string',
            'image' => 'mimes:jpg,jpeg,png|max:2060',
        ]);

        Volunteer::find($request->volunteer_id)->update([
            'name' => $request->name,
            'mobile' => $request->mobile,
            'blood_group' => $request->blood_group,
            'designation' => $request->designation,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'linkedin' => $request->instagraminstagram,
        ]);

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'volunteers-pic-'.$request->donor_id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/volunteers',$image_name);
                Volunteer::where('id',$request->donor_id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'volunteer Update Success','type' => 'success']);
    }

    public function delete_volunteer(Request $request, $id){
        Volunteer::find($id)->delete();
        return redirect()->back()->with(['msg' => 'volunteer Delete Success','type' => 'danger']);
    }

}
