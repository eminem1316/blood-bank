@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Donors Search')}}
@endsection
@section('content')
    <!-- header bottom start  -->
    <div class="header-bottom-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="blood-search-warpper">
                         @if($errors->any())
                            <ul class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="{{route('frontend.search.donors')}}" class="blood-search-form" method="get">
                            @csrf
                            <ul class="fields-list">
                                <li>
                                    <div class="form-group">
                                        <select class="form-control nice-select wide" name="area">
                                            <option value="">{{__('Areas')}}</option>
                                            @foreach($all_areas as $data)
                                            <option @if($area == $data->id) selected  @endif value="{{$data->id}}">{{$data->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-group ">
                                        <select class="form-control nice-select wide" name="blood_group">
                                            <option value="">{{__('Blood Group')}}</option>
                                            <option @if($blood_group == 'o+') selected  @endif value="o+">O+</option>
                                            <option @if($blood_group == 'o-') selected  @endif value="o-">O-</option>
                                            <option @if($blood_group == 'b+') selected  @endif value="b+">B+</option>
                                            <option @if($blood_group == 'b-') selected  @endif value="b-">B-</option>
                                            <option @if($blood_group == 'a+') selected  @endif value="a+">A+</option>
                                            <option @if($blood_group == 'a-') selected  @endif value="a-">A-</option>
                                            <option @if($blood_group == 'ab+') selected  @endif value="ab+">AB+</option>
                                            <option @if($blood_group == 'ab-') selected  @endif value="ab-">AB-</option>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <input type="submit" value="Search Donor" class="submit-btn">
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header bottom end -->

    <!-- our dedicated team area start  -->
    <section class="dedicated-team-area padding-120 ">
        <div class="container">
            <div class="row">
                @if(count($all_donors) < 1)
                    <div class="col-lg-12">
                        <div class="alert alert-danger">{{__('Sorry No Donor Available !!')}}</div>
                    </div>
                @endif
                @foreach($all_donors as $data)
                    <div class="col-lg-3 col-md-6">
                        <div class="single-donors-item margin-bottom-30">
                            <div class="thumb">
                                @if(file_exists('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image))
                                    <img src="{{asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)}}" alt="{{__($data->name)}}">
                                @else
                                    <img src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                @endif
                            </div>
                            <div class="content">
                                <a href="{{route('frontend.donors.profile',['id' => $data->id,'slug' => Str::slug($data->user->name)])}}"> <h4 class="title">{{$data->user->name}}</h4></a>
                                <span class="blood-group">{{__('Blood Group:')}} <strong>{{strtoupper($data->blood_group)}}</strong></span>
                                <span class="total-donate">{{__('Total Donate:')}} <strong>{{get_total_donate($data->id)}}</strong> {{__('Times')}}</span>
                            </div>
                        </div>
                 </div>
                @endforeach
                <div class="col-lg-12">
                    <nav class="pagination-wrapper" aria-label="Page navigation ">
                        {{$all_donors->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- our dedicated team area end -->
@endsection
