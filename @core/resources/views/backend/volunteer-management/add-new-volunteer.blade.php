@extends('backend.admin-master')
@section('site-title')
    {{__('Add New Volunteer')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <!-- basic form start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Create New Volunteer')}}</h4>
                        @include('backend/partials/message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('admin.new.volunteer')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" placeholder="{{__('Enter Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="{{__('Email')}}">
                            </div>
                            <div class="form-group">
                                <label for="mobile">{{__('Mobile')}}</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__('Mobile')}}">
                            </div>
                            <div class="form-group">
                                <label for="designation">{{__('Designation')}}</label>
                                <input type="text" class="form-control" id="designation" name="designation" placeholder="{{__('Designation')}}">
                            </div>
                            <div class="form-group">
                                <label for="facebook">{{__('Facebook')}}</label>
                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="{{__('Facebook')}}">
                            </div>
                            <div class="form-group">
                                <label for="twitter">{{__('Twitter')}}</label>
                                <input type="text" class="form-control" id="twitter" name="twitter" placeholder="{{__('Twitter')}}">
                            </div>
                            <div class="form-group">
                                <label for="instagram">{{__('Instagram')}}</label>
                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="{{__('Instagram')}}">
                            </div>
                            <div class="form-group">
                                <label for="linkedin">{{__('Linkedin')}}</label>
                                <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="{{__('Linkedin')}}">
                            </div>
                            <div class="form-group">
                                <label for="blood_group">{{__('Blood Group')}}</label>
                                <select name="blood_group"  id="blood_group" class="form-control">
                                    <option value="o+">O+</option>
                                    <option value="o-">O-</option>
                                    <option value="b+">B+</option>
                                    <option value="b-">B-</option>
                                    <option value="a+">A+</option>
                                    <option value="a-">A-</option>
                                    <option value="ab+">AB+</option>
                                    <option value="ab-">AB-</option>
                                </select>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" id="image">
                                <label class="custom-file-label" for="image">{{__('Image')}}</label>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Volunteer')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
