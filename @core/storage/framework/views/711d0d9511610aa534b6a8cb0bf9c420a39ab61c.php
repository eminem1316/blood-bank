<div class="header-bottom-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="blood-search-warpper">
                    <?php if($errors->any()): ?>
                        <ul class="alert alert-danger">
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php endif; ?>
                    <form action="<?php echo e(route('frontend.search.donors')); ?>" class="blood-search-form" method="get">
                        <ul class="fields-list">
                            <li>
                                <div class="form-group">
                                    <select class="form-control nice-select wide" name="area">
                                        <option value=""><?php echo e(__('Areas')); ?></option>
                                        <?php $__currentLoopData = $all_areas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="form-group ">
                                    <select class="form-control nice-select wide" name="blood_group">
                                        <option value=""><?php echo e(__('Blood Group')); ?></option>
                                        <option value="o+">O+</option>
                                        <option value="o-">O-</option>
                                        <option value="b+">B+</option>
                                        <option value="b-">B-</option>
                                        <option value="a+">A+</option>
                                        <option value="a-">A-</option>
                                        <option value="ab+">AB+</option>
                                        <option value="ab-">AB-</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <input type="submit" value="<?php echo e(__('Search Donor')); ?>" class="submit-btn">
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/partials/donor-search-form.blade.php ENDPATH**/ ?>