<?php

namespace App\Http\Controllers;

use App\Importantlink;
use App\UsefulLink;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function about_widget(){
        return view('backend.pages.footer.about');
    }
    public function update_about_widget(Request $request){
        $this->validate($request,[
            'about_widget_logo' => 'mimes:jpg,jpeg,png|max:2064',
            'about_widget_description' => 'required'
        ]);
        update_static_option('about_widget_description',$request->about_widget_description);

        if ($request->hasFile('about_widget_logo')){
            $image = $request->about_widget_logo;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'about-widget-logo.'.$image_extenstion;
            if (check_image_extension($image)){
                $image->move('assets/uploads/',$image_name);
                update_static_option('about_widget_logo',$image_extenstion);
            }
        }

        return redirect()->back()->with([
            'msg' => 'About Widget Update Success...',
            'type' => 'success'
        ]);
    }

    public function useful_links_widget(){
        $all_useful_link = UsefulLink::all();
        return view('backend.pages.footer.useful-link')->with([
            'all_useful_link' => $all_useful_link,
        ]);
    }
    public function new_useful_links_widget(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
        ]);
        UsefulLink::create($request->all());
        return redirect()->back()->with([
            'msg' => 'New Useful Link Added...',
            'type' => 'success'
        ]);
    }
    public function update_useful_links_widget(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
        ]);
        UsefulLink::where('id',$request->id)->update([
            'title' => $request->title,
            'url' => $request->url,
        ]);
        return redirect()->back()->with([
            'msg' => 'Useful Link Updated...',
            'type' => 'success'
        ]);
    }

    public function delete_useful_links_widget(Request $request,$id){
        UsefulLink::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Useful Link Delete...',
            'type' => 'danger'
        ]);
    }
    public function recent_post_widget(){
        return view('backend.pages.footer.recent-post');
    }
    public function update_recent_post_widget(Request $request){

        $this->validate($request,[
            'recent_post_widget_title' => 'required',
            'recent_post_widget_item' => 'required'
        ]);

        update_static_option('recent_post_widget_title',$request->recent_post_widget_title);
        update_static_option('recent_post_widget_item',$request->recent_post_widget_item);

        return redirect()->back()->with([
            'msg' => 'Recent Post Widget Update Success...',
            'type' => 'success'
        ]);
    }

    public function update_widget_useful_links(Request $request){
        $this->validate($request,[
            'useful_link_widget_title' => 'required',
        ]);

        update_static_option('useful_link_widget_title',$request->useful_link_widget_title);

        return redirect()->back()->with([
            'msg' => 'Useful Widget Settings Success...',
            'type' => 'success'
        ]);
    }
    public function update_widget_important_links(Request $request){
        $this->validate($request,[
            'important_link_widget_title' => 'required',
        ]);

        update_static_option('important_link_widget_title',$request->important_link_widget_title);

        return redirect()->back()->with([
            'msg' => 'Important Widget Settings Success...',
            'type' => 'success'
        ]);
    }


    public function important_links_widget(){
        $all_important_link = Importantlink::all();
        return view('backend.pages.footer.important-links')->with([
            'all_important_link' => $all_important_link,
        ]);
    }
    public function new_important_links_widget(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
        ]);
        Importantlink::create($request->all());
        return redirect()->back()->with([
            'msg' => 'New Important Link Added...',
            'type' => 'success'
        ]);
    }
    public function update_important_links_widget(Request $request){

        $this->validate($request,[
            'title' => 'required|string|max:191',
            'url' => 'required|string|max:191',
        ]);
        Importantlink::where('id',$request->id)->update([
            'title' => $request->title,
            'url' => $request->url,
        ]);
        return redirect()->back()->with([
            'msg' => 'Important Link Updated...',
            'type' => 'success'
        ]);
    }

    public function delete_important_links_widget(Request $request,$id){
        Importantlink::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Important Link Delete...',
            'type' => 'danger'
        ]);
    }
}
