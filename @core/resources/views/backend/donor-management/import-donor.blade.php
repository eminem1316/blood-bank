@extends('backend.admin-master')
@section('site-title')
    {{__('Import Donor')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12">
        <div class="row">
            <!-- basic form start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Import Donor From Excel File')}}</h4>
                        @include('backend/partials/message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('admin.import.donor')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="custom-file">
                                <input type="file" name="import_excel" id="import_excel" accept=".xlsx">
                                <br>
                                <small class="info-text">{{__('Only xlsx file accepted.')}}</small>
                                <small class="info-text">{{__('Download the example excel file to know how to organise your excel file for import')}}
                                    <a href="{{asset('assets/example-excel-file.xlsx')}}" download>example excel file</a></small>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Import Donors')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
