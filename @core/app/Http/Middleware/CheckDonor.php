<?php

namespace App\Http\Middleware;

use App\Donor;
use Closure;

class CheckDonor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $donor_info = Donor::where('user_id',auth()->user()->id)->first();
        if (!empty($donor_info)){
            return $next($request);
        }
        return redirect()->back();
    }
}
