<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 0 !important;
        }
        div.dataTables_wrapper div.dataTables_length select {
            width: 60px;
            display: inline-block;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('site-title'); ?>
    <?php echo e(__('All Blood Request')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <?php echo $__env->make('backend/partials/message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <h4 class="header-title"><?php echo e(__('All Blood Request')); ?></h4>
                                    <div class="data-tables datatable-primary">
                                        <table id="all_blood_log_table" class="text-center">
                                            <thead class="text-capitalize">
                                            <tr>
                                                <th><?php echo e(__('ID')); ?></th>
                                                <th><?php echo e(__('Name')); ?></th>
                                                <th><?php echo e(__('Email')); ?></th>
                                                <th><?php echo e(__('Blood Group')); ?></th>
                                                <th><?php echo e(__('Mobile')); ?></th>
                                                <th><?php echo e(__('Number Of Unit')); ?></th>
                                                <th><?php echo e(__('Illness')); ?></th>
                                                <th><?php echo e(__('Status')); ?></th>
                                                <th><?php echo e(__('Date')); ?></th>
                                                <th><?php echo e(__('Action')); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $__currentLoopData = $all_blood_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($data->id); ?></td>
                                                    <td><?php echo e($data->name); ?></td>
                                                    <td><?php echo e($data->email); ?></td>
                                                    <td><?php echo e(strtoupper($data->blood_group)); ?></td>
                                                    <td><?php echo e($data->mobile); ?></td>
                                                    <td><?php echo e($data->number_of_units); ?></td>
                                                    <td><?php echo e($data->illness); ?></td>
                                                    <td>
                                                        <?php if(!empty($data->status)): ?>
                                                            <div class="alert alert-success"><?php echo e(__('Donated')); ?></div>
                                                        <?php else: ?>
                                                            <from method="post" action="" enctype="multipart/form-data">
                                                                <?php echo csrf_field(); ?>
                                                                <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                                                                <button type="submit" class="btn btn-warning btn-xs"><?php echo e(__('Waiting For Donor')); ?></button>
                                                            </from>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php echo e($data->created_at->diffForHumans()); ?></td>
                                                    <td>
                                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="" data-content="
                                                       <h6>Are you sure to delete this Blood Request?</h6>
                                                       <form method='post' action='<?php echo e(route('admin.blood.request.delete',$data->id)); ?>'>
                                                       <input type='hidden' name='_token' value='<?php echo e(csrf_token()); ?>'>
                                                       <br>
                                                        <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                        </form>
                                                        " data-original-title="">
                                                            <i class="ti-trash"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="<?php echo e($data->id); ?>"
                                                           data-name="<?php echo e($data->name); ?>"
                                                           data-email="<?php echo e($data->email); ?>"
                                                           data-mobile="<?php echo e($data->mobile); ?>"
                                                           data-blood_group="<?php echo e($data->blood_group); ?>"
                                                           data-state="<?php echo e($data->state); ?>"
                                                           data-city="<?php echo e($data->city); ?>"
                                                           data-number_of_units="<?php echo e($data->number_of_units); ?>"
                                                           data-illness="<?php echo e($data->illness); ?>"
                                                           data-hospital_address="<?php echo e($data->hospital_address); ?>"
                                                           data-message="<?php echo e($data->message); ?>"
                                                           data-toggle="modal"
                                                           data-target="#blood_request_edit_modal"
                                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 blood_request_edit_btn"
                                                        >
                                                            <i class="ti-eye"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Primary table end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="blood_request_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php echo e(__('Request Details')); ?></h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body">
                    <ul>
                        <li class="name"><strong><?php echo e(__('Name:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="email"><strong><?php echo e(__('Email:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="mobile"><strong><?php echo e(__('Mobile:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="blood_group"><strong><?php echo e(__('Blood Group:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="state"><strong><?php echo e(__('State:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="city"><strong><?php echo e(__('City:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="number_of_units"><strong><?php echo e(__('Number Of Units:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="illness"><strong><?php echo e(__('Illness:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="hospital_address"><strong><?php echo e(__('Hospital Address:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                        <li class="message"><strong><?php echo e(__('Message:')); ?></strong> &nbsp; <span><?php echo e(__('dddd')); ?></span></li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_blood_log_table').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
            $(document).on('click','.blood_request_edit_btn',function(e){
                e.preventDefault();
                var allData = $(this).data();
                delete allData.id;
                delete allData.target;
                delete allData.toggle;
                $.each(allData,function(key,value){
                    $('#blood_request_edit_modal').find('.'+key).children('span').text(value);
                });

                console.log(allData)
            })
        } );
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/backend/blood-request/index.blade.php ENDPATH**/ ?>