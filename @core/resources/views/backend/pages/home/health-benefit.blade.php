@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/summernote-bs4.css')}}">
@endsection
@section('site-title')
    {{__('Health Benefit')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Health Benefit Settings')}}</h4>
                        <form action="{{route('admin.health.benefit')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="health_benefit_first_item_content">{{__('First Item Content')}}</label>
                                <input type="hidden" name="health_benefit_first_item_content" id="health_benefit_first_item_content">
                                <div class="summernote" data-content='{{get_static_option("health_benefit_first_item_content")}}'></div>

                            </div>
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('health_benefit_first_item_image')))
                                <img  style="max-width: 200px; margin-top:20px;" src="{{asset('assets/uploads/'.get_static_option('health_benefit_first_item_image'))}}" alt="">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="health_benefit_first_item_image">{{__('First Item Image')}}</label>
                                <input type="file" class="form-control"  id="health_benefit_first_item_image" name="health_benefit_first_item_image">
                            </div>
                            <div class="form-group">
                                <label for="health_benefit_second_item_content">{{__('Second Item Content')}}</label>
                                <input type="hidden" name="health_benefit_second_item_content" id="health_benefit_second_item_content">
                                <div class="summernote" data-content='{{get_static_option("health_benefit_second_item_content")}}'></div>

                            </div>
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('health_benefit_second_item_image')))
                                    <img  style="max-width: 200px; margin-top:20px;" src="{{asset('assets/uploads/'.get_static_option('health_benefit_second_item_image'))}}" alt="">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="health_benefit_second_item_image">{{__('Second Item Image')}}</label>
                                <input type="file" class="form-control"  id="health_benefit_second_item_image" name="health_benefit_second_item_image">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{asset('assets/backend/js/summernote-bs4.js')}}"></script>
    <script>

        $(document).ready(function(){
            $('.summernote').summernote({
                height: 150,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                },
                callbacks: {
                    onChange: function(contents, $editable) {
                        $(this).prev('input').val(contents);
                    }
                }
            });
            if($('.summernote').length > 1){
                $('.summernote').each(function(index,value){
                    $(this).summernote('code', $(this).data('content'));
                });
            }
        })

    </script>
@endsection
