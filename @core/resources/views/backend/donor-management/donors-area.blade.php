@extends('backend.admin-master')
@section('site-title')
    {{__('Donor Area')}}
@endsection
@section('content')
    <div class="werforyou-page-wrapper wrapper-padding">
        @include('backend/partials/message')
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('All Area')}}</h4>
                        <div class="single-table">
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">{{__('ID')}}</th>
                                        <th scope="col">{{__('Name')}}</th>
                                        <th scope="col">{{__('Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($all_area as $data)
                                        <tr>
                                            <th scope="row">{{$data->id}}</th>
                                            <td>{{$data->name}}</td>
                                            <td>
                                                <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1"
                                                   role="button"
                                                   data-toggle="popover"
                                                   data-trigger="focus"
                                                   data-html="true"
                                                   title=""
                                                   data-content="
                                               <h6>Are you sure to delete this area?</h6>
                                               <form method='post' action='{{route('admin.donor.area.destroy',$data->id)}}'>
                                               <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                               <br>
                                                <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                </form>
                                                ">
                                                    <i class="ti-trash"></i>
                                                </a>
                                                <a href="#"
                                                   data-toggle="modal"
                                                   data-target="#donor_category_edit_modal"
                                                   class="btn btn-lg btn-primary btn-sm mb-3 mr-1 donor_category_edit_btn"
                                                   data-id="{{$data->id}}"
                                                   data-name="{{$data->name}}"
                                                   data-action="{{route('admin.donor.area.update',$data->id)}}">
                                                   <i class="ti-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">

                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Create New Area')}}</h4>
                        @error('name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                        <form method="POST" action="{{route('admin.donor.area')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="{{__('Enter Area Name')}}">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Area')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="donor_category_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Edit Area')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <form action="" id="donor_category_edit_modal_form" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{__('Name')}}</label>
                            <input type="text" class="form-control" id="edit_name" name="name"
                                   placeholder="{{__('Enter Area Name')}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('click','.donor_category_edit_btn',function(){
               var el = $(this);
               var name = el.data('name');
               var action = el.data('action');
               var form = $('#donor_category_edit_modal_form');
                form.attr('action',action);
                form.find('#edit_name').val(name);
            });
        });
    </script>
@endsection
