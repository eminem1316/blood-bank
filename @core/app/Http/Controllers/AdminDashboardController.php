<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Area;
use App\BloodLog;
use App\Volunteer;
use App\ContactInfoItem;
use App\Donor;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminDashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }
    public function adminIndex(){
        $total_donor = Donor::count();
        $total_blood_donate = BloodLog::count();
        $total_volunteer_member = Volunteer::count();
        $all_blood_logs = BloodLog::orderBy('id','desc')->take(10)->get();
        return view('backend.admin-home')->with([
            'total_donor' => $total_donor,
            'total_blood_donate' => $total_blood_donate,
            'total_volunteer_member' => $total_volunteer_member,
            'all_blood_logs' => $all_blood_logs,
        ]);
    }
    public function site_identity(){
        return view('backend.general-settings.site-identity');
    }
    public function update_site_identity(Request $request){
        $this->validate($request,[
           'site_logo' => 'mimes:jpg,jpeg,png|max:2064' ,
           'site_favicon' => 'mimes:jpg,jpeg,png|max:2064' ,
           'site_breadcrumb_bg' => 'mimes:jpg,jpeg,png|max:2064'
        ]);
        if ($request->hasFile('site_logo')){
            $image = $request->site_logo;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'site-logo-'.rand(9999,99999).'.'.$image_extenstion;
            if (check_image_extension($image)){
                $image->move('assets/uploads/',$image_name);
                update_static_option('site_logo',$image_name);
            }
        }
        if ($request->hasFile('site_favicon')){
            $image = $request->site_favicon;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'site-favicon-'.rand(9999,99999).'.'.$image_extenstion;
            if (check_image_extension($image)){
                $image->move('assets/uploads/',$image_name);
                update_static_option('site_favicon',$image_name);
            }
        }
        if ($request->hasFile('site_breadcrumb_bg')){
            $image = $request->site_breadcrumb_bg;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'breadcrumb-bg-'.rand(9999,99999).'.'.$image_extenstion;
            if (check_image_extension($image)){
                $image->move('assets/uploads/',$image_name);
                update_static_option('site_breadcrumb_bg',$image_name);
            }
        }
        return redirect()->back()->with([
            'msg' => 'Site Identity Has Been Updated..',
            'type' => 'success'
        ]);
    }
    public function admin_settings(){
        return view('auth.admin.settings');
    }
    public function admin_profile_update(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'image' => 'mimes:jpg,jpeg,png|max:2064'
        ]);
        $image_ext = Auth::user()->image;
        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_ext = $image_extenstion;
            $image_name = 'admin-pic-'.Auth::user()->id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/admins',$image_name);
            }
        }
        Admin::find(Auth::user()->id)->update(['name'=>$request->name,'email' => $request->email ,'image' => $image_ext ]);
        return redirect()->back()->with(['msg' => 'Profile Update Success' , 'type' => 'success']);
    }
    public function admin_password_chagne(Request $request){
        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = Admin::findOrFail(Auth::id());

        if (Hash::check($request->old_password ,$user->password)){

            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();

            return redirect()->route('admin.login')->with(['msg'=>'Password Changed Successfully','type'=> 'success']);
        }

        return redirect()->back()->with(['msg'=>'Somethings Going Wrong! Please Try Again or Check Your Old Password','type'=> 'danger']);
    }
    public function adminLogout(){
        Auth::logout();
        return redirect()->route('admin.login')->with(['msg'=>'You Logged Out !!','type'=> 'danger']);
    }

    public function admin_profile(){
        return view('auth.admin.edit-profile');
    }
    public function admin_password(){
        return view('auth.admin.change-password');
    }
    public function contact(){
        $all_contact_info_items = ContactInfoItem::all();
        return view('backend.pages.contact')->with([
            'all_contact_info_item' => $all_contact_info_items
        ]);
    }
    public function update_contact(Request $request){
        $this->validate($request,[
           'page_title' => 'required|string|max:191',
           'get_title' => 'required|string|max:191',
           'get_description' => 'required|string',
           'latitude' => 'required',
           'longitude' => 'required',
        ]);

        update_static_option('contact_page_title',$request->page_title);
        update_static_option('contact_page_get_title',$request->get_title);
        update_static_option('contact_page_get_description',$request->get_description);
        update_static_option('contact_page_latitude',$request->latitude);
        update_static_option('contact_page_longitude',$request->longitude);

        return redirect()->back()->with(['msg'=>'Contact Page Info Update Success','type'=> 'success']);
    }

    public function donor_page(){
        return view('backend.pages.donors');
    }

    public function donor_page_update(Request $request){
        $this->validate($request,[
           'donor_page_title' => 'required|string',
           'donor_show_number' => 'required|string'
        ]);
        update_static_option('donor_page_title',$request->donor_page_title);
        update_static_option('donor_show_number',$request->donor_show_number);

        return redirect()->back()->with(['msg'=>'Donors Page Settings Update..','type'=> 'success']);
    }
    public function volunteer_page(){
        return view('backend.pages.volunteer');
    }

    public function volunteer_page_update(Request $request){
        $this->validate($request,[
           'volunteer_page_title' => 'required|string',
           'volunteer_show_number' => 'required|string'
        ]);
        update_static_option('volunteer_page_title',$request->volunteer_page_title);
        update_static_option('volunteer_show_number',$request->volunteer_show_number);

        return redirect()->back()->with(['msg'=>'Volunteer Page Settings Update..','type'=> 'success']);
    }

    public function about_page_update(Request $request){
        $this->validate($request,[
           'action' => 'required'
        ]);

        if ('about_us_page_settings_form' == $request->action){
            update_static_option('about_page_title',$request->about_page_title);
            return redirect()->back()->with(['msg'=>'About Us Page Settings Update..','type'=> 'success']);
        }elseif('about_us_content_form' == $request->action){
            if ($request->hasFile('about_us_image')){
                $this->validate($request,[
                    'about_us_image' => 'mimes:jpg,jpeg,png|max:2064'
                ]);
                $image = $request->about_us_image;
                $image_extenstion = $request->about_us_image->getClientOriginalExtension();
                $image_name = 'about-us-image-'.rand(999,999999).'.'.$image_extenstion;
                if (check_image_extension($request->about_us_image)){
                    $image->move('assets/uploads/',$image_name);
                    update_static_option('about_us_image',$image_name);
                }
            }
            update_static_option('about_us_content',$request->about_us_content);

            return redirect()->back()->with(['msg'=>'About Content Update..','type'=> 'success']);
        }else{
            if ($request->hasFile('why_donate_image')){
                $this->validate($request,[
                    'why_donate_image' => 'mimes:jpg,jpeg,png|max:2064'
                ]);
                $image = $request->why_donate_image;
                $image_extenstion = $request->why_donate_image->getClientOriginalExtension();
                $image_name = 'why-donate-image-'.rand(999,999999).'.'.$image_extenstion;
                if (check_image_extension($request->why_donate_image)){
                    $image->move('assets/uploads/',$image_name);
                    update_static_option('why_donate_image',$image_name);
                }
            }

            update_static_option('why_donate_content',$request->why_donate_content);
            update_static_option('why_us_title',$request->why_us_title);

            return redirect()->back()->with(['msg'=>'Why Donate Content Update..','type'=> 'success']);
        }

        return redirect()->back()->with(['msg'=>'Something Wrong.. Please try again later.','type'=> 'danger']);
    }
    public function about_page(){
        $all_volunteer = Volunteer::all();
        return view('backend.pages.about')->with([
            'all_volunteer' => $all_volunteer
        ]);
    }

    public function blog_page(){
        return view('backend.pages.blog');
    }

    public function blog_page_update(Request $request){
        $this->validate($request,[
            'blog_page_title' => 'required',
            'blog_page_item' => 'required',
            'blog_page_category_widget_title' => 'required',
            'blog_page_recent_post_widget_title' => 'required',
            'blog_page_recent_post_widget_item' => 'required',
        ]);
        update_static_option('blog_page_title', $request->blog_page_title);
        update_static_option('blog_page_item', $request->blog_page_item);
        update_static_option('blog_page_category_widget_title', $request->blog_page_category_widget_title);
        update_static_option('blog_page_recent_post_widget_title', $request->blog_page_recent_post_widget_title);
        update_static_option('blog_page_recent_post_widget_item', $request->blog_page_recent_post_widget_item);

        return redirect()->back()->with(['msg'=>'Blog Settings Update Success','type'=> 'success']);
    }

    public function basic_settings(){
        return view('backend.general-settings.basic');
    }
    public function update_basic_settings(Request $request){
        $this->validate($request,[
            'site_title' => 'required|string',
            'site_tag_line' => 'required|string',
            'site_footer_copyright' => 'required|string',
            'site_color' => 'required|string',
        ]);

        update_static_option('site_title', $request->site_title);
        update_static_option('site_tag_line', $request->site_tag_line);
        update_static_option('site_footer_copyright', $request->site_footer_copyright);
        update_static_option('site_color', $request->site_color);

        return redirect()->back()->with(['msg'=>'Basic Settings Update Success','type'=> 'success']);
    }

    public function seo_settings(){
        return view('backend.general-settings.seo');
    }
    public function update_seo_settings(Request $request){
        $this->validate($request,[
            'site_meta_tags' => 'required|string',
            'site_meta_description' => 'required|string'
        ]);

        update_static_option('site_meta_tags', $request->site_meta_tags);
        update_static_option('site_meta_description', $request->site_meta_description);

        return redirect()->back()->with(['msg'=>'SEO Settings Update Success','type'=> 'success']);
    }

    public function scripts_settings(){
        return view('backend.general-settings.thid-party');
    }

    public function update_scripts_settings(Request $request){

        $this->validate($request,[
            'site_disqus_key' => 'string'
        ]);

        update_static_option('site_disqus_key', $request->site_disqus_key);
        update_static_option('site_google_analytics', $request->site_google_analytics);

        return redirect()->back()->with(['msg'=>'Third Party Scripts Settings Updated..','type'=> 'success']);
    }
    public function email_template_settings(){
        return view('backend.general-settings.email-template');
    }

    public function update_email_template_settings(Request $request){

        $this->validate($request,[
            'site_global_email' => 'required|string',
            'site_global_email_template' => 'required|string',
        ]);

        update_static_option('site_global_email', $request->site_global_email);
        update_static_option('site_global_email_template', $request->site_global_email_template);

        return redirect()->back()->with(['msg'=>'Email Settings Updated..','type'=> 'success']);
    }

    public function custom_css_settings(){
        $custom_css = '/* Write Custom Css Here */';
        if (file_exists('assets/frontend/css/dynamic-style.css')){
            $custom_css = file_get_contents('assets/frontend/css/dynamic-style.css');
        }
        return view('backend.general-settings.custom-css')->with(['custom_css' => $custom_css]);
    }

    public function update_custom_css_settings(Request $request){
        file_put_contents('assets/frontend/css/dynamic-style.css',$request->custom_css_area);

        return redirect()->back()->with(['msg' => 'Custom Style Added Success...', 'type' => 'success']);
    }

    public function cache_settings()
    {
        return view('backend.general-settings.cache-settings');
    }

    public function update_cache_settings(Request $request)
    {

        $this->validate($request, [
            'cache_type' => 'required|string'
        ]);

        Artisan::call($request->cache_type . ':clear');

        return redirect()->back()->with(['msg' => 'Cache Cleaned...', 'type' => 'success']);
    }

}
