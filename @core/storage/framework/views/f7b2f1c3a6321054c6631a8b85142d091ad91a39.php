<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="<?php echo e(route('admin.home')); ?>">
                <?php if(file_exists('assets/uploads/'.get_static_option('site_logo'))): ?>
                    <img src="<?php echo e(asset('assets/uploads/'.get_static_option('site_logo'))); ?>" alt="<?php echo e(get_static_option('site_title')); ?>">
                <?php endif; ?>
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li class="<?php echo e(active_menu('admin-home')); ?>">
                        <a href="<?php echo e(route('admin.home')); ?>"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span><?php echo app('translator')->get('dashboard'); ?></span>
                        </a>
                    </li>
                    <li
                        class="
                            <?php echo e(active_menu('admin-home/new-donor')); ?>

                            <?php echo e(active_menu('admin-home/all-donor')); ?>

                            <?php echo e(active_menu('admin-home/donor-area')); ?>

                            "
                        >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span><?php echo e(__('Donors Manage')); ?></span></a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('admin-home/all-donor')); ?>"><a href="<?php echo e(route('admin.all.donor')); ?>"><?php echo e(__('All Donors')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/donor-area')); ?>"><a href="<?php echo e(route('admin.donor.area')); ?>"><?php echo e(__('Donors Area')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/new-donor')); ?>"><a href="<?php echo e(route('admin.new.donor')); ?>"><?php echo e(__('Add New Donor')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/import-donor')); ?>"><a href="<?php echo e(route('admin.import.donor')); ?>"><?php echo e(__('Import Donor')); ?></a></li>
                        </ul>
                    </li>
                    <li
                        class="
                        <?php echo e(active_menu('admin-home/new-volunteer')); ?>

                        <?php echo e(active_menu('admin-home/all-volunteer')); ?>

                         "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span><?php echo e(__('Volunteer Manage')); ?></span></a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('admin-home/all-volunteer')); ?>"><a href="<?php echo e(route('admin.all.volunteer')); ?>"><?php echo e(__('All Volunteer')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/new-volunteer')); ?>"><a href="<?php echo e(route('admin.new.volunteer')); ?>"><?php echo e(__('Add New Volunteer')); ?></a></li>
                        </ul>
                    </li>
                    <li
                        class="
                            <?php echo e(active_menu('admin-home/all-blood-log')); ?>

                            <?php echo e(active_menu('admin-home/new-blood-log')); ?>

                            "
                        >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-loop"></i> <span><?php echo e(__('Blood Logs')); ?></span></a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('admin-home/all-blood-log')); ?>"><a href="<?php echo e(__(route('admin.blood.index'))); ?>"><?php echo e(__('All Logs')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/new-blood-log')); ?>"><a href="<?php echo e(__(route('admin.new.blood.logs'))); ?>"><?php echo e(__('Add New Log')); ?></a></li>
                        </ul>
                    </li>
                    <li class="<?php echo e(active_menu('admin-home/request-for-blood')); ?>"><a href="<?php echo e(route('admin.blood.request')); ?>"><i class="ti-loop"></i> <span><?php echo e(__('All Blood Request')); ?></span></a></li>
                    <?php if('super_admin' == auth()->user()->role): ?>
                    <li
                        class="
                        <?php echo e(active_menu('admin-home/new-user')); ?>

                        <?php echo e(active_menu('admin-home/all-user')); ?>

                        "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i> <span><?php echo e(__('Admin Role Manage')); ?></span></a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('admin-home/all-user')); ?>"><a href="<?php echo e(route('admin.all.user')); ?>"><?php echo e(__('All Admin')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/new-user')); ?>"><a href="<?php echo e(route('admin.new.user')); ?>"><?php echo e(__('Add New Admin')); ?></a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/topbar')); ?>">
                            <a href="<?php echo e(route('admin.topbar')); ?>"
                               aria-expanded="true">
                                <i class="ti-dashboard"></i>
                                <span><?php echo e(__('Top Bar Settings')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="
                        <?php echo e(active_menu('admin-home/home-page')); ?>

                        <?php if(request()->is('admin-home/home-page/*')): ?>
                        <?php echo e(active_menu('admin-home/home-page/header')); ?>

                        active
                        <?php endif; ?>
                            ">
                            <a href="javascript:void(0)"
                               aria-expanded="true">
                                <i class="ti-home"></i>
                                <span><?php echo e(__('Home Page')); ?></span>
                            </a>
                            <ul class="collapse">
                                <li class="<?php echo e(active_menu('admin-home/home-page/header')); ?>"><a href="<?php echo e(route('admin.home.header')); ?>"><?php echo e(__('Header Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/blood-donation')); ?>"><a href="<?php echo e(route('admin.blood.donation')); ?>"><?php echo e(__('Blood Donation Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/our-valuable-donor')); ?>"><a href="<?php echo e(route('admin.valuable.donor')); ?>"><?php echo e(__('Available Donor Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/recent-request-for-blood')); ?>"><a href="<?php echo e(route('admin.recent.request.section')); ?>"><?php echo e(__('Recent Request For Blood Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/recent-donors')); ?>"><a href="<?php echo e(route('admin.recent.donors')); ?>"><?php echo e(__('Recent Donor Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/health-benefit')); ?>"><a href="<?php echo e(route('admin.health.benefit')); ?>"><?php echo e(__('Health Benefits Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/cta')); ?>"><a href="<?php echo e(route('admin.cta')); ?>"><?php echo e(__('Call To Action Area')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/home-page/news')); ?>"><a href="<?php echo e(route('admin.home.news')); ?>"><?php echo e(__('News Update Area')); ?></a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/home-section-manage')); ?>">
                            <a href="<?php echo e(route('admin.home.section.manage')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('Home Sections Manage')); ?></span>
                            </a>
                        </li>
                        <li class="<?php echo e(active_menu('admin-home/about-section-manage')); ?>">
                            <a href="<?php echo e(route('admin.about.section.manage')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('About Sections Manage')); ?></span>
                            </a>
                        </li>
                        <li class="<?php echo e(active_menu('admin-home/about-page')); ?>">
                            <a href="<?php echo e(route('admin.about.page')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('About Page')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/volunteer-page')); ?>">
                            <a href="<?php echo e(route('admin.volunteer.page')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('Volunteer Page')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/donors-page')); ?>">
                            <a href="<?php echo e(route('admin.donor.page')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('Donors Page')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/blog-page')); ?>">
                            <a href="<?php echo e(route('admin.blog.page')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('Blog Page')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php echo e(active_menu('admin-home/contact')); ?>">
                            <a href="<?php echo e(route('admin.contact')); ?>"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span><?php echo e(__('Contact Page')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                    <li
                        class="
                        <?php echo e(active_menu('admin-home/blog')); ?>

                        <?php echo e(active_menu('admin-home/blog-category')); ?>

                        <?php echo e(active_menu('admin-home/new-blog')); ?>

                        <?php if(request()->is('admin-home/blog-edit/*')): ?> active <?php endif; ?>
                        "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-write"></i> <span><?php echo e(__('Blog')); ?></span></a>
                        <ul class="collapse">
                            <li class="<?php echo e(active_menu('admin-home/blog')); ?>"><a href="<?php echo e(route('admin.blog')); ?>"><?php echo e(__('All Blog')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/blog-category')); ?>"><a href="<?php echo e(route('admin.blog.category')); ?>"><?php echo e(__('Category')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/new-blog')); ?>"><a href="<?php echo e(route('admin.blog.new')); ?>"><?php echo e(__('Add New Post')); ?></a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                    <li
                        class="
                        <?php echo e(active_menu('admin-home/testimonial')); ?>

                        "
                    >
                        <a href="<?php echo e(route('admin.testimonial')); ?>" aria-expanded="true"><i class="ti-control-forward"></i> <span><?php echo e(__('Testimonial')); ?></span></a>
                    </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                    <li
                        class="  <?php echo e(active_menu('admin-home/counterup')); ?> ">
                        <a href="<?php echo e(route('admin.counterup')); ?>" aria-expanded="true"><i class="ti-exchange-vertical"></i> <span><?php echo e(__('Counterup')); ?></span></a>
                    </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li class="<?php if(request()->is('admin-home/footer/*')): ?> active <?php endif; ?>">
                            <a href="javascript:void(0)"
                               aria-expanded="true">
                                <i class="ti-layout"></i>
                                <span><?php echo e(__('Footer Area')); ?></span>
                            </a>
                            <ul class="collapse">
                                <li class="<?php echo e(active_menu('admin-home/footer/about')); ?>"><a href="<?php echo e(route('admin.footer.about')); ?>"><?php echo e(__('About Widget')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/footer/useful-links')); ?>"><a href="<?php echo e(route('admin.footer.useful.link')); ?>"><?php echo e(__('Useful Links Widget')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/footer/recent-post')); ?>"><a href="<?php echo e(route('admin.footer.recent.post')); ?>"><?php echo e(__('Recent Posts Widget')); ?></a></li>
                                <li class="<?php echo e(active_menu('admin-home/footer/important-links')); ?>"><a href="<?php echo e(route('admin.footer.important.link')); ?>"><?php echo e(__('Important Links Widget')); ?></a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role || 'admin' == auth()->user()->role || 'volunteer' == auth()->user()->role): ?>
                    <li class="<?php if(request()->is('admin-home/general-settings/*')): ?> active <?php endif; ?>">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i> <span><?php echo e(__('General Settings')); ?></span></a>
                        <ul class="collapse ">
                            <li class="<?php echo e(active_menu('admin-home/general-settings/site-identity')); ?>"><a href="<?php echo e(route('admin.general.site.identity')); ?>"><?php echo e(__('Site Identity')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/basic-settings')); ?>"><a href="<?php echo e(route('admin.general.basic.settings')); ?>"><?php echo e(__('Basic Settings')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/seo-settings')); ?>"><a href="<?php echo e(route('admin.general.seo.settings')); ?>"><?php echo e(__('SEO Settings')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/scripts')); ?>"><a href="<?php echo e(route('admin.general.scripts.settings')); ?>"><?php echo e(__('Third Party Scripts')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/email-template')); ?>"><a href="<?php echo e(route('admin.general.email.template')); ?>"><?php echo e(__('Email Template')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/cache-settings')); ?>"><a href="<?php echo e(route('admin.general.cache.settings')); ?>"><?php echo e(__('Cache Settings')); ?></a></li>
                            <li class="<?php echo e(active_menu('admin-home/general-settings/custom-css')); ?>"><a href="<?php echo e(route('admin.general.custom.css')); ?>"><?php echo e(__('Custom Css')); ?></a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if('super_admin' == auth()->user()->role): ?>
                        <li
                            class="<?php if(request()->is('admin-home/languages/*') || request()->is('admin-home/languages') ): ?> active <?php endif; ?>">
                            <a href="<?php echo e(route('admin.languages')); ?>" aria-expanded="true"><i class="ti-signal"></i> <span><?php echo e(__('Languages')); ?></span></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/backend/partials/sidebar.blade.php ENDPATH**/ ?>