<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckEmailVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if (auth()->user()->email_verified == 1){
                return $next($request);
            }else{
                return redirect()->route('user.email.verify');
            }
        }
        return redirect()->route('login');
    }
}
