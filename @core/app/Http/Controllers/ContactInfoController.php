<?php

namespace App\Http\Controllers;

use App\ContactInfoItem;
use Illuminate\Http\Request;

class ContactInfoController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function new_contact_info(Request $request){

        $this->validate($request,[
           'icon' => 'required|string|max:191',
           'title' => 'required|string|max:191',
           'description' => 'required'
        ]);

        ContactInfoItem::create($request->all());

        return redirect()->back()->with(['msg' => 'New Contact Info Added Success', 'type' => 'success']);
    }

    public function update_contact_info(Request $request){
        $this->validate($request,[
            'icon' => 'required|string|max:191',
            'title' => 'required|string|max:191',
            'description' => 'required'
        ]);

        ContactInfoItem::find($request->id)->update($request->all());

        return redirect()->back()->with(['msg' => 'Contact Info Update Success', 'type' => 'success']);
    }

    public function delete_contact_info(Request $request,$id){

        ContactInfoItem::find($id)->delete();

        return redirect()->back()->with(['msg' => 'Contact Info Delete', 'type' => 'danger']);
    }
}
