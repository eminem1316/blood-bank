@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Request For Blood')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="right-content-area">
                        <div class="contact-page-form-wrap">
                            @include('backend.partials.message')
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('frontend.blood.request')}}" method="post" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="{{__('Your Name')}}" value="{{(old('name'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="{{__('Your Email')}}" value="{{(old('email'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" value="{{(old('mobile'))}}" name="mobile" placeholder="{{__("Mobile")}}">
                                        </div>
                                        <div class="form-group">
                                            <select name="blood_group" id="blood_group" class="form-control nice-select wide">
                                                <option value="">{{__('Blood Group')}}</option>
                                                <option value="o+">O+</option>
                                                <option value="o-">O-</option>
                                                <option value="b+">B+</option>
                                                <option value="b-">B-</option>
                                                <option value="a+">A+</option>
                                                <option value="a-">A-</option>
                                                <option value="ab+">AB+</option>
                                                <option value="ab-">AB-</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="state" value="{{(old('state'))}}" name="state" placeholder="{{__("State")}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="city" {{(old('city'))}} name="city" placeholder="{{__("City")}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" {{(old('number_of_units'))}} id="number_of_units" name="number_of_units" placeholder="{{__("Number Of Units")}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="illness" {{(old('illness'))}} name="illness" placeholder="{{__("Illness")}}">
                                        </div>
                                        <div class="form-group">
                                            <textarea name="hospital_address" id="hospital_address" class="form-control" cols="30" rows="10" placeholder="{{__('Hospital Address')}}"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" cols="30" rows="10" placeholder="{{__('Your Message')}}"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="{{__('Submit Request')}}" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
