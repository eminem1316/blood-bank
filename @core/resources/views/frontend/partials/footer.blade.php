
<footer class="footer-area">
    <div class="footer-top-area padding-top-100 padding-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-top-area-inner">
                        <div class="left-content-area">
                            <ul class="contact-info">
                                @foreach($all_support_item as $data)
                                    <li >
                                        <div class="single-contact-info">
                                            <div class="icon">
                                                <i class="{{$data->icon}}"></i>
                                            </div>
                                            <div class="content">
                                                <h4 class="title">{{$data->title}}</h4>
                                                <span class="detials">{{$data->details}}</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="right-content-area">
                            <div class="share-widget">
                                <h4 class="title">{{__('Let\'s Connect')}}</h4>
                                <ul class="social-share">
                                    @foreach($all_social_item as $data)
                                        <li><a href="{{$data->url}}"><i class="{{$data->icon}}"></i></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="widget footer-widget about_widget">
                        <div class="footer-logo">
                            @if(file_exists('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo')))
                                <a href="{{url('/')}}"><img src="{{asset('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo'))}}" alt=" {{__("Footer Logo")}}"></a>
                            @endif
                        </div>
                        <p>{{get_static_option('about_widget_description')}}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="widget footer-widget">
                        <h2 class="widget-title">{{get_static_option('useful_link_widget_title')}}</h2>
                        <ul class="pages-list">
                            @foreach($all_useful_link as $data)
                                <li><a href="{{$data->url}}">{{$data->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="widget footer-widget">
                        <h2 class="widget-title">{{get_static_option('recent_post_widget_title')}}</h2>
                        <ul class="recent_post_item">
                            @foreach($all_recent_blogs as $data)
                                <li class="single-recent-post-item">
                                    <div class="thumb">
                                        @if(file_exists('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image))
                                            <img src="{{asset('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)}}" alt="{{$data->title}}">
                                        @endif
                                    </div>
                                    <div class="content">
                                        <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                                        <span class="times">{{$data->created_at->diffForHumans()}}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class=" widget footer-widget">
                        <h2 class="widget-title">{{get_static_option('important_link_widget_title')}}</h2>
                        <ul class="pages-list">
                            @foreach($all_important_link as $data)
                                <li><a href="{{$data->url}}">{{$data->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @php
                        $footer_copyright_text = get_static_option('site_footer_copyright');
                        $footer_copyright_text = str_replace('{copy}','&copy;',$footer_copyright_text);
                        $footer_copyright_text = str_replace('{year}',date('Y'),$footer_copyright_text);
                    @endphp
                    {!! $footer_copyright_text !!}
                </div>
            </div>
        </div>
    </div>
</footer>
@if(request()->path() != 'blood-request')
<div class="request-for-blood">
    <a href="{{route('frontend.blood.request')}}" target="_blank" class="request-link"><i class="fas fa-tint"></i>{{__('Request For Blood')}}</a>
</div>
@endif
<!-- All scripts -->
<script src="{{asset('assets/frontend/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery-migrate-3.1.0.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.waypoints.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.rcounter.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>
<script>
    (function($){
       "use strict";
       $(document).ready(function(){
           $(document).on('change','#langchange',function(e){
               $.ajax({
                   url : "{{route('frontend.langchange')}}",
                   type: "GET",
                   data:{
                       'lang' : $(this).val()
                   },
                   success:function (data) {
                       location.reload();
                   }
               })
           });
       });
    }(jQuery));
</script>
@yield('scripts')
</body>
</html>
