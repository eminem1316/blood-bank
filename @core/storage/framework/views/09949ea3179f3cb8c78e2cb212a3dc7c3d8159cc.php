<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Request For Blood')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="right-content-area">
                        <div class="contact-page-form-wrap">
                            <?php echo $__env->make('backend.partials.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form action="<?php echo e(route('frontend.blood.request')); ?>" method="post" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="<?php echo e(__('Your Name')); ?>" value="<?php echo e((old('name'))); ?>" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="<?php echo e(__('Your Email')); ?>" value="<?php echo e((old('email'))); ?>" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" value="<?php echo e((old('mobile'))); ?>" name="mobile" placeholder="<?php echo e(__("Mobile")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <select name="blood_group" id="blood_group" class="form-control nice-select wide">
                                                <option value=""><?php echo e(__('Blood Group')); ?></option>
                                                <option value="o+">O+</option>
                                                <option value="o-">O-</option>
                                                <option value="b+">B+</option>
                                                <option value="b-">B-</option>
                                                <option value="a+">A+</option>
                                                <option value="a-">A-</option>
                                                <option value="ab+">AB+</option>
                                                <option value="ab-">AB-</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="state" value="<?php echo e((old('state'))); ?>" name="state" placeholder="<?php echo e(__("State")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="city" <?php echo e((old('city'))); ?> name="city" placeholder="<?php echo e(__("City")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" <?php echo e((old('number_of_units'))); ?> id="number_of_units" name="number_of_units" placeholder="<?php echo e(__("Number Of Units")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="illness" <?php echo e((old('illness'))); ?> name="illness" placeholder="<?php echo e(__("Illness")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <textarea name="hospital_address" id="hospital_address" class="form-control" cols="30" rows="10" placeholder="<?php echo e(__('Hospital Address')); ?>"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" cols="30" rows="10" placeholder="<?php echo e(__('Your Message')); ?>"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="<?php echo e(__('Submit Request')); ?>" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/request-for-blood.blade.php ENDPATH**/ ?>