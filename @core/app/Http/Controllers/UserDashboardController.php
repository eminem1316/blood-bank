<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Area;
use App\Donor;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('frontend.user-dashboard.index');
    }

    public function edit_profile(){
        return view('frontend.user-dashboard.edit-profile');
    }

    public function update_profile(Request $request){
        $this->validate($request,[
            'email' => 'required|string|max:191',
            'name' => 'required|string|max:191'
        ]);

        User::find(auth()->user()->id)->update([
            'email' => $request->email,
            'name' => $request->name
        ]);

        return redirect()->back()->with([
            'msg' => 'Profile Information Update Success',
            'type' => 'success'
        ]);
    }

    public function edit_donor_info(){
        $all_areas = Area::all();
        return view('frontend.user-dashboard.edit-donor-info')->with(['all_areas' => $all_areas]);
    }

    public function update_donor_info(Request $request){
        $this->validate($request,[
            'father_name' => 'max:191',
            'mother_name' => 'max:191',
            'present_address' => 'max:191',
            'permanent_address' => 'max:191',
            'birthday' => 'max:191',
            'blood_group' => 'required|string|max:191',
            'area_id' => 'required|string|max:191',
            'image' => 'mimes:jpg,jpeg,png',
        ]);
        Donor::find(auth()->user()->donor->id)->update([
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->area_id
        ]);
        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'donor-pic-'.auth()->user()->id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/donors',$image_name);
                Donor::where('id',auth()->user()->id)->update(['image'=>$image_extenstion]);
            }
        }
        return redirect()->back()->with([
            'msg' => 'Donor Information Update Success',
            'type' => 'success'
        ]);
    }
    public function change_password(){
        return view('frontend.user-dashboard.change-password');
    }
    public function update_password(Request $request){
        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = User::findOrFail(Auth::id());

        if (Hash::check($request->old_password ,$user->password)){

            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();

            return redirect()->route('login')->with(['msg'=>'Password Changed Successfully','type'=> 'success']);
        }

        return redirect()->back()->with(['msg'=>'Somethings Going Wrong! Please Try Again or Check Your Old Password','type'=> 'danger']);
    }

    public function join_as_donor(Request $request){

        $this->validate($request,[
            'mobile' => 'required|string|max:191',
            'birthday' => 'required|string|max:191',
            'blood_group' => 'required|string|max:191',
            'area_id' => 'required|string|max:191'
        ]);

        Donor::create([
            'mobile' => $request->mobile,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->area_id,
            'user_id' => auth()->user()->id
        ]);
        return redirect()->route('homepage');
    }
    public function join_as_donor_index(Request $request){
        $all_area = Area::all();
       return view('frontend.user-dashboard.join-as-donor')->with(['all_area' => $all_area]);
    }
}
