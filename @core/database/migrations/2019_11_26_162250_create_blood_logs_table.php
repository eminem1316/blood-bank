<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloodLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('donors_id',false,true);
            $table->string('blood_group');
            $table->string('receiver_name');
            $table->text('receiver_address');
            $table->string('receiver_mobile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blood_logs');
    }
}
