@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Join As Donor')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Join As Donor')}}</h2>
                        @include('backend.partials.message')
                        <form action="{{route('user.join.donor')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__("Mobile")}}">
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" id="birthday" name="birthday">
                            </div>
                            <div class="form-group margin-bottom-70">
                                <select name="blood_group" id="blood_group" class="form-control nice-select wide">
                                    <option value="">{{__('Blood Group')}}</option>
                                    <option value="o+">O+</option>
                                    <option value="o-">O-</option>
                                    <option value="b+">B+</option>
                                    <option value="b-">B-</option>
                                    <option value="a+">A+</option>
                                    <option value="a-">A-</option>
                                    <option value="ab+">AB+</option>
                                    <option value="ab-">AB-</option>
                                </select>
                            </div>
                            <div class="form-group margin-bottom-70">
                                <select name="area_id" class="form-control nice-select wide" id="area-id">
                                    <option value="">{{__('Select Area')}}</option>
                                    @foreach($all_area as $data)
                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="form-group">
                                <input type="submit" value="{{__('Register As Donor')}}" class="submit-btn register-as-donor">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
