@extends('frontend.frontend-page-master')
@section('page-title')
    {{__("All Available Donors In ".strtoupper(request()->route()->parameters['id']))}}
@endsection
@section('content')

    <section class="dedicated-team-area padding-120 ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @if(count($all_donors) < 1)
                        <div class="col-lg-12">
                            <div class="alert alert-danger">{{__('Sorry No '.strtoupper(request()->route()->parameters['id']).' Group Donor Available !!')}}</div>
                        </div>
                    @endif
                </div>
                @foreach($all_donors as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="single-donors-item margin-bottom-30">
                        <div class="thumb">
                            @if(file_exists('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image))
                                <img src="{{asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)}}" alt="{{__($data->name)}}">
                            @else
                                <img src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                            @endif
                        </div>
                        <div class="content">
                            <a href="{{route('frontend.donors.profile',['id' => $data->id,'slug' => Str::slug($data->user->name)])}}"> <h4 class="title">{{$data->user->name}}</h4></a>
                            <span class="blood-group">{{__('Blood Group:')}} <strong>{{strtoupper($data->blood_group)}}</strong></span>
                            <span class="total-donate">{{__('Total Donate:')}} <strong>{{get_total_donate($data->id)}}</strong> {{__('Times')}}</span>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-lg-12">
                    <nav class="pagination-wrapper" aria-label="Page navigation ">
                        {{$all_donors->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
