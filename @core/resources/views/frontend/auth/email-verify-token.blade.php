@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Email Token Verify')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Email Token Verify')}}</h2>
                        @include('backend.partials.message')
                        @if($errors->any())
                            @foreach($errors->all() as $error)
                                <span class="text-danger">{{$error}}</span>
                            @endforeach
                        @endif
                        <div class="alert alert-success">{{__('Please check your inbox, we send verification link to very your email address')}}</div>
                        <form action="{{route('user.email.verify')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" readonly value="{{auth()->user()->username}}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="submit-btn">{{__(('Send Verify Mail Again'))}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
