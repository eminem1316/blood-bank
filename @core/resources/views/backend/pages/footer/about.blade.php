@extends('backend.admin-master')
@section('site-title')
    {{__('About Widget Settings')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-t">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('About Widget Settings')}}</h4>
                        <form action="{{route('admin.footer.about')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="img-wrapper" style="margin: 20px">
                                @if(file_exists('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo')))
                                    <img src="{{asset('assets/uploads/about-widget-logo.'.get_static_option('about_widget_logo'))}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="about_widget_logo">{{__('Widget Logo')}}</label>
                                <input type="file" class="form-control"  id="about_widget_logo"  name="about_widget_logo" >
                            </div>
                            <div class="form-group">
                                <label for="about_widget_description">{{__('Widget Description')}}</label>
                                <textarea class="form-control"  id="about_widget_description" name="about_widget_description" >{{get_static_option('about_widget_description')}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
