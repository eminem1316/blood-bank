@extends('frontend.frontend-page-master')
@section('page-title')
    {{get_static_option('about_page_title')}}
@endsection
@section('content')
    @if(!empty(get_static_option('about_page_about_us_section_status')))
    <div class="about-us-content padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content-area">
                       {!! get_static_option("about_us_content") !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="img-wrapper">
                            @if(file_exists('assets/uploads/'.get_static_option('about_us_image')))
                                <img src="{{asset('assets/uploads/'.get_static_option('about_us_image'))}}" alt="">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(!empty(get_static_option('about_page_why_donate_section_status')))
    <section class="why-donate-blood-area gray-bg padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('why_us_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="img-wrapper">
                        @if(file_exists('assets/uploads/'.get_static_option('why_donate_image')))
                            <img src="{{asset('assets/uploads/'.get_static_option('why_donate_image'))}}" alt="">
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-area">
                        {!! get_static_option("why_donate_content") !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @if(!empty(get_static_option('about_page_volunteer_section_status')))
@include('frontend.partials.volunteers-area')
@endif
@if(!empty(get_static_option('about_page_counterup_section_status')))
@include('frontend.partials.counterup')
@endif
@if(!empty(get_static_option('about_page_testimonial_section_status')))
@include('frontend.partials.testimonial')
@endif

@endsection
