<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestForBlood;

class RequestForBloodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $all_request = RequestForBlood::all();
        return view('backend.blood-request.index')->with(['all_blood_requests' => $all_request ]);
    }
    public function delete($id){
        RequestForBlood::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Request For Blood Item is Deleted...' , 'type' => 'danger']);
    }
}
