<!DOCTYPE html>
<html lang="en">
<head>
    @if(!empty(get_static_option('site_google_analytics')))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{get_static_option('site_google_analytics')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', "{{get_static_option('site_google_analytics')}}");
    </script>
    @endif
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{{get_static_option('site_meta_description')}}">
    <meta name="tags" content="{{get_static_option('site_meta_tags')}}">
    <link rel="icon" href="{{asset('assets/uploads/'.get_static_option('site_favicon'))}}" type="image/png">
    <!-- all stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/dynamic-style.css')}}">
    <style>
        :root {
            --main-color-one: {{get_static_option('site_color')}};
        }
    </style>
    @yield('style')
    <title>{{get_static_option('site_title')}} - {{get_static_option('site_tag_line')}}</title>
</head>
<body>
<!-- top bar area start  -->
<div class="topbar-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="topbar-inner">
                    <div class="left-content-area">
                        <ul class="info-items">
                            @foreach($all_support_item as $data)
                                <li><i class="{{$data->icon}}"></i> {{$data->details}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="right-content-area">
                        <div class="right-inner">
                            <ul class="social-icons">
                                @foreach($all_social_item as $data)
                                    <li><a href="{{$data->url}}"><i class="{{$data->icon}}"></i></a></li>
                                @endforeach
                            </ul>
                            <select id="langchange">
                                @foreach($all_language as $lang)
                                <option @if(session()->get('lang') == $lang->slug) selected @endif value="{{$lang->slug}}">{{$lang->name}}</option>
                                @endforeach
                            </select>
                           @if(!auth()->check())
                                <div class="btn-wrapper">
                                    <a href="{{route('login')}}" class="boxed-btn">{{__('Login')}}</a>
                                </div>
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.partials.navbar')
