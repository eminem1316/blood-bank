<?php

namespace App\Http\Controllers;

use App\Area;
use App\Donor;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DonorDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('frontend.donor-dashboard.index');
    }

    public function edit_donor_info(){
        $all_areas = Area::all();
        return view('frontend.donor-dashboard.edit-donor-info')->with(['all_areas' => $all_areas]);
    }
    public function edit_profile(){
        return view('frontend.donor-dashboard.edit-profile');
    }

    public function update_donor_info(Request $request){
        $this->validate($request,[
            'father_name' => 'max:191',
            'mother_name' => 'max:191',
            'present_address' => 'max:191',
            'permanent_address' => 'max:191',
            'birthday' => 'max:191',
            'blood_group' => 'required|string|max:191',
            'area_id' => 'required|string|max:191',
            'image' => 'mimes:jpg,jpeg,png',
        ]);
        Donor::find(auth()->user()->donor->id)->update([
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->area_id
        ]);
        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'donor-pic-'.auth()->user()->id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/donors',$image_name);
                Donor::where('id',auth()->user()->id)->update(['image'=>$image_extenstion]);
            }
        }
        return redirect()->back()->with([
            'msg' => 'Donor Information Update Success',
            'type' => 'success'
        ]);
    }

}
