<?php $__env->startSection('page-title'); ?>
    <?php echo e(__('Join As Donor')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="righti-content-area">
                        <div class="contact-page-form-wrap">
                            <?php echo $__env->make('backend.partials.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form action="<?php echo e(route('frontend.register.donor')); ?>" method="post" id="contact_page_form" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="<?php echo e(__('Your Name')); ?>" value="<?php echo e((old('name'))); ?>" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="username" placeholder="<?php echo e(__('Your Username')); ?>" value="<?php echo e((old('username'))); ?>" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="<?php echo e(__('Your Email')); ?>" value="<?php echo e((old('email'))); ?>" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="<?php echo e(__("Mobile")); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="date" class="form-control" id="birthday" name="birthday">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="<?php echo e(__('Your Password')); ?>"  class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" placeholder="<?php echo e(__('Confirm Password')); ?>"  class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <select name="blood_group" id="blood_group" class="form-control nice-select wide">
                                                <option value=""><?php echo e(__('Blood Group')); ?></option>
                                                <option value="o+">O+</option>
                                                <option value="o-">O-</option>
                                                <option value="b+">B+</option>
                                                <option value="b-">B-</option>
                                                <option value="a+">A+</option>
                                                <option value="a-">A-</option>
                                                <option value="ab+">AB+</option>
                                                <option value="ab-">AB-</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="area_id" class="form-control nice-select wide" id="area-id">
                                                <option value=""><?php echo e(__('Select Area')); ?></option>
                                                <?php $__currentLoopData = $all_area; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($data->id); ?>"><?php echo e($data->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="<?php echo e(__('Register As Donor')); ?>" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/join-donor.blade.php ENDPATH**/ ?>