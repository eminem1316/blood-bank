<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailVerifyToken extends Model
{
    protected $table = 'email_verify_tokens';
    protected $fillable = ['token','username'];
}
