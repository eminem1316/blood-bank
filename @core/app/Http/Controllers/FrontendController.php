<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Area;
use App\Blog;
use App\BlogCategory;
use App\BloodLog;
use App\EmailVerifyToken;
use App\User;
use App\Volunteer;
use App\ContactInfoItem;
use App\Counterup;
use App\DonationProcess;
use App\Donor;
use App\RequestForBlood;
use App\SocialIcons;
use App\SupportInfo;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FrontendController extends Controller
{
    public function index()
    {
        $all_areas = Area::all();
        $all_volunteer = Volunteer::all();
        $all_counterup = Counterup::all();
        $all_testimonial = Testimonial::all();
        $all_donate_process = DonationProcess::all();
        $all_recent_donors = BloodLog::orderBy('id', 'desc')->take(get_static_option('recent_donors_number'))->get();
        $all_recent_donors = $all_recent_donors->unique('donors_id');
        $all_blog = Blog::orderBy('id', 'desc')->take(get_static_option('news_update_number'))->get();
        $recent_blood_request = RequestForBlood::orderBy('id', 'desc')->take(get_static_option('home_recent_request_for_blood_number'))->get();

        return view('frontend.frontend-home')->with([
            'all_areas' => $all_areas,
            'all_volunteer' => $all_volunteer,
            'all_counterup' => $all_counterup,
            'all_testimonial' => $all_testimonial,
            'all_donate_process' => $all_donate_process,
            'all_recent_donors' => $all_recent_donors,
            'all_blog' => $all_blog,
            'recent_blood_request' => $recent_blood_request,
        ]);
    }

    public function join_donor()
    {
        $all_area = Area::all();
        return view('frontend.pages.join-donor')->with([
            'all_area' => $all_area
        ]);
    }

    Public function store_new_donor(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'father_name' => 'required|string|max:191',
            'mother_name' => 'required|string|max:191',
            'present_address' => 'string',
            'permanent_address' => 'string',
            'mobile' => 'required|string|max:191|unique:donors',
            'birthday' => 'string',
            'blood_group' => 'required|string',
            'area_id' => 'required|string',
            'image' => 'mimes:jpg,jpeg,png|max:2060',
        ]);

        $id = Donor::create([
            'name' => $request->name,
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'mobile' => $request->mobile,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'donors_categories_id' => 1,
            'area_id' => $request->area_id,
            'status' => 0
        ])->id;

        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'donor-pic-' . $id . '.' . $image_extenstion;
            if (check_image_extension($request->image)) {
                $image->move('assets/uploads/donors', $image_name);
                Donor::where('id', $id)->update(['image' => $image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'Register Success. Waiting for approval', 'type' => 'success']);
    }

    public function contact_index()
    {

        $all_contact_info_items = ContactInfoItem::all();
        return view('frontend.pages.contact')->with([
            'contact_info_items' => $all_contact_info_items
        ]);
    }

    public function contact_mail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:191',
            'name' => 'required|string|max:191',
            'subject' => 'required|string|max:191',
            'message' => 'required'
        ]);
        $subject = 'From WeForYou ' . $request->subject;
        if (sendPlanEmail(get_static_option('site_global_email'), $request->name, $subject, $request->message, $request->email)) {
            return redirect()->back()->with(['msg' => 'Thanks for your contact!!', 'type' => 'success']);
        }
        return redirect()->back()->with(['msg' => 'Something goes wrong, Please try again later !!', 'type' => 'danger']);
    }

    public function donors_index()
    {
        $all_donors = Donor::where('status', 1)->orderBy('id', 'desc')->paginate(get_static_option('donor_show_number'));
        $all_areas = Area::all();
        return view('frontend.pages.donors')->with([
            'all_donors' => $all_donors,
            'all_areas' => $all_areas
        ]);
    }

    public function donor_search(Request $request)
    {
        $this->validate($request, [
            'area' => 'required|string',
            'blood_group' => 'required'
        ]);

        $all_donors = Donor::where([
            'area_id' => $request->area,
            'blood_group' => $request->blood_group,
        ])->orderBy('id', 'desc')->paginate(get_static_option('donor_show_number'));
        $all_areas = Area::all();
        return view('frontend.pages.search')->with([
            'all_donors' => $all_donors,
            'all_areas' => $all_areas,
            'area' => $request->area,
            'blood_group' => $request->blood_group,
        ]);
    }

    public function volunteer_index()
    {
        $all_volunteer = Volunteer::orderBy('id', 'desc')->paginate(get_static_option('volunteer_show_number'));
        return view('frontend.pages.volunteer')->with([
            'all_volunteer' => $all_volunteer
        ]);
    }

    public function about_index()
    {
        $all_volunteer = Volunteer::orderBy('id', 'desc')->get();
        $all_counterup = Counterup::all();
        $all_testimonial = Testimonial::all();
        return view('frontend.pages.about')->with([
            'all_volunteer' => $all_volunteer,
            'all_counterup' => $all_counterup,
            'all_testimonial' => $all_testimonial
        ]);
    }

    public function donors_by_blood_group($id)
    {
        $all_donors = Donor::where(['status' => 1, 'blood_group' => $id])->orderBy('id', 'desc')->paginate(get_static_option('donor_show_number'));

        return view('frontend.pages.donors-by-blood-group')->with([
            'all_donors' => $all_donors
        ]);
    }

    public function blog_page()
    {
        $all_recent_blogs = Blog::orderBy('id', 'desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_blogs = Blog::orderBy('id', 'desc')->paginate(get_static_option('blog_page_item'));
        $all_category = BlogCategory::where('status', 'publish')->orderBy('id', 'desc')->get();
        return view('frontend.pages.blog')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function category_wise_blog_page($id)
    {
        $all_blogs = Blog::where('blog_categories_id', $id)->orderBy('id', 'desc')->paginate(get_static_option('blog_page_item'));
        $all_recent_blogs = Blog::orderBy('id', 'desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_category = BlogCategory::where('status', 'publish')->orderBy('id', 'desc')->get();
        $category_name = BlogCategory::where(['id' => $id, 'status' => 'publish'])->first()->name;
        return view('frontend.pages.blog-category')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'category_name' => $category_name,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function blog_search_page(Request $request)
    {

        $all_recent_blogs = Blog::orderBy('id', 'desc')->take(get_static_option('blog_page_recent_post_widget_item'))->get();
        $all_category = BlogCategory::where('status', 'publish')->orderBy('id', 'desc')->get();
        $all_blogs = Blog::where('title', 'LIKE', '%' . $request->search . '%')
            ->orWhere('content', 'LIKE', '%' . $request->search . '%')
            ->orWhere('tags', 'LIKE', '%' . $request->search . '%')
            ->orderBy('id', 'desc')->paginate(get_static_option('blog_page_item'));

        return view('frontend.pages.blog-search')->with([
            'all_blogs' => $all_blogs,
            'all_categories' => $all_category,
            'search_term' => $request->search,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function blog_single_page($id, $any)
    {

        $blog_post = Blog::where('id', $id)->first();

        $all_recent_blogs = Blog::orderBy('id', 'desc')->paginate(get_static_option('blog_page_recent_post_widget_item'));
        $all_category = BlogCategory::where('status', 'publish')->orderBy('id', 'desc')->get();
        return view('frontend.pages.blog-single')->with([
            'blog_post' => $blog_post,
            'all_categories' => $all_category,
            'all_recent_blogs' => $all_recent_blogs,
        ]);
    }

    public function showAdminForgetPasswordForm()
    {
        return view('auth.admin.forget-password');
    }

    public function sendAdminForgetPasswordMail(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string:max:191'
        ]);
        $user_info = Admin::where('username', $request->username)->orWhere('email', $request->username)->first();
        $token_id = Str::random(30);
        $existing_token = DB::table('password_resets')->where('email', $user_info->email)->delete();
        if (empty($existing_token)) {
            DB::table('password_resets')->insert(['email' => $user_info->email, 'token' => $token_id]);
        }

        $message = 'Here is you password reset link, If you did not request to reset your password just ignore this mail. <a style="background-color:#444;color:#fff;text-decoration:none;padding: 10px 15px;border-radius: 3px;display: block;width: 130px;margin-top: 20px;" href="' . route('admin.reset.password', ['user' => $user_info->username, 'token' => $token_id]) . '">Click Reset Password</a>';
        if (sendEmail($user_info->email, $user_info->username, 'Reset Your Password', $message)) {
            return redirect()->back()->with([
                'msg' => 'Check Your Mail For Reset Password Link',
                'type' => 'success'
            ]);
        }
        return redirect()->back()->with([
            'msg' => 'Something Wrong, Please Try Again!!',
            'type' => 'danger'
        ]);
    }

    public function showAdminResetPasswordForm($username, $token)
    {
        return view('auth.admin.reset-password')->with([
            'username' => $username,
            'token' => $token
        ]);
    }

    public function AdminResetPassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'username' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ]);
        $user_info = Admin::where('username', $request->username)->first();
        $user = Admin::findOrFail($user_info->id);
        $token_iinfo = DB::table('password_resets')->where(['email' => $user_info->email, 'token' => $request->token])->first();
        if (!empty($token_iinfo)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('admin.login')->with(['msg' => 'Password Changed Successfully', 'type' => 'success']);
        }

        return redirect()->back()->with(['msg' => 'Somethings Going Wrong! Please Try Again or Check Your Old Password', 'type' => 'danger']);
    }

    public function lang_change(Request $request)
    {
        session()->put('lang', $request->lang);
        return redirect()->route('homepage');
    }

    public function user_forget_password()
    {
        return view('frontend.pages.forget-password');
    }

    public function user_send_password_reset_mail(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string:max:191'
        ]);
        $user_info = User::where('username', $request->username)->orWhere('email', $request->username)->first();
        $token_id = Str::random(30);
        $existing_token = DB::table('password_resets')->where('email', $user_info->email)->delete();
        if (empty($existing_token)) {
            DB::table('password_resets')->insert(['email' => $user_info->email, 'token' => $token_id]);
        }
        $message = 'Here is you password reset link, If you did not request to reset your password just ignore this mail. <a style="background-color:#444;color:#fff;text-decoration:none;padding: 10px 15px;border-radius: 3px;display: block;width: 130px;margin-top: 20px;" href="' . route('user.password.reset.mail', ['user' => $user_info->username, 'token' => $token_id]) . '">Click Reset Password</a>';
        if (sendEmail($user_info->email, $user_info->username, 'Reset Your Password', $message)) {
            return redirect()->back()->with([
                'msg' => 'Check Your Mail For Reset Password Link',
                'type' => 'success'
            ]);
        }
        return redirect()->back()->with([
            'msg' => 'Something Wrong, Please Try Again!!',
            'type' => 'danger'
        ]);
    }

    public function user_reset_password_form($username, $token)
    {
        return view('frontend.pages.reset-password')->with([
            'username' => $username,
            'token' => $token
        ]);
    }

    public function user_reset_password(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'username' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user_info = User::where('username', $request->username)->first();
        $user = User::findOrFail($user_info->id);
        $token_iinfo = DB::table('password_resets')->where(['email' => $user_info->email, 'token' => $request->token])->first();
        if (!empty($token_iinfo)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('login')->with(['msg' => 'Password Changed Successfully', 'type' => 'success']);
        }

        return redirect()->back()->with(['msg' => 'Somethings Going Wrong! Please Try Again or Check Your Old Password', 'type' => 'danger']);
    }

    public function donors_profile($id, $slug)
    {
        $donor_info = Donor::find($id);

        return view('frontend.pages.donor-details')->with(['donor_info' => $donor_info]);
    }

    public function send_mail_to_user(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:191',
            'subject' => 'required|string',
            'message' => 'required|string',
            'user_id' => 'required',
        ]);
        $user_info = User::find($request->user_id);
        $message = 'Here you have a message form ' . $request->email . '<br><br>' . $request->message;
        if (sendEmail($user_info->email, $user_info->name, $request->subject, $message)) {
            return redirect()->back()->with(['msg' => 'Thanks for your contact!!', 'type' => 'success']);
        }
        return redirect()->back()->with(['msg' => 'Something goes wrong, Please try again later !!', 'type' => 'danger']);
    }

    public function blood_request()
    {
        if (Auth::check()) {
            return view('frontend.pages.request-for-blood');
        } else {
            return redirect()->route('login');
        }
    }

    public function add_blood_request(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191',
            'mobile' => 'required|string|max:191',
            'blood_group' => 'required|string|max:191',
            'state' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'number_of_units' => 'required|string|max:191',
            'illness' => 'required|string',
            'message' => 'required|string',
            'hospital_address' => 'required|string',
        ]);

        RequestForBlood::create($request->all());

        return redirect()->back()->with(['msg' => 'Your Blood Request Is Received, It Will Be Published Soon.', 'type' => 'success']);
    }

    public function join_as_donor(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'username' => 'required|string|max:191|unique:users',
            'email' => 'required|string|max:191|unique:users',
            'mobile' => 'required|string|max:191|unique:donors',
            'birthday' => 'required|string|max:191',
            'blood_group' => 'required|string|max:191',
            'area_id' => 'required|string|max:191',
            'password' => 'required|min:8|confirmed',
        ]);

        $id = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ])->id;

        Donor::create([
            'user_id' => $id,
            'mobile' => $request->mobile,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->area_id
        ]);

        if (Auth::guard('web')->attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->intended('/donor-dashboard');
        }

        return redirect()->back()->with(['msg' => 'Something went wrong please try again later', 'type' => 'danger']);
    }

    public function email_verify_form(){
        $user_info = User::find(auth()->user()->id);
        $token_id = Str::random(30);
        $existing_token = EmailVerifyToken::where(['username' => $user_info->username])->delete();
        if (empty($existing_token)) {
            EmailVerifyToken::create([
               'token' => $token_id,
               'username' =>  $user_info->username
            ]);
        }
        $message = 'Here is you email verification link, click this link to verify your email address. <a style="background-color:#444;color:#fff;text-decoration:none;padding: 10px 15px;border-radius: 3px;display: block;width: 130px;margin-top: 20px;" href="' . route('email.verify.token', ['username' => $user_info->username, 'token' => $token_id]) . '">Verify Email</a>';
        sendEmail($user_info->email, $user_info->username, 'Verify Your Email Address', $message);

        return view('frontend.auth.email-verify-token');
    }

    public function email_verify_mail_send(Request $request){
        $user_info = User::find(auth()->user()->id);
        $token_id = Str::random(30);
        $existing_token = EmailVerifyToken::where(['username' => $user_info->username])->delete();
        if (empty($existing_token)) {
            EmailVerifyToken::create([
                'token' => $token_id,
                'username' =>  $user_info->username
            ]);
        }
        $message = 'Here is you email verification link, click this link to verify your email address. '. route('email.verify.token', ['username' => $user_info->username, 'token' => $token_id]) . '"';

        mail($user_info->email, 'Verify Your Email Address',$user_info->username, $message);

        return redirect()->back();

    }

    public function email_verify_token_form($token,$username){
        $dashboard_url = is_donor() ? 'donor.dashboard' : 'user.dashboard';
        EmailVerifyToken::where(['username' => $username,'token' => $token])->delete();
        User::where('username', $username)->update(['email_verified' => 1]);
        return redirect()->route($dashboard_url);
    }
}
