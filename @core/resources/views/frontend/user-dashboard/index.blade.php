@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('User Dashboard')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Profile Information')}}</h2>
                        <ul>
                            <li><strong>{{__('Name:')}} </strong> <span class="right">{{auth()->user()->name}}</span></li>
                            <li><strong>{{__('Email:')}} </strong> <span class="right">{{auth()->user()->email}}</span></li>
                            <li><strong>{{__('Username:')}} </strong> <span class="right">{{auth()->user()->username}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
