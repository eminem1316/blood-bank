<nav class="navbar navbar-area navbar-expand-lg">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper">
                <a href="<?php echo e(url('/')); ?>" class="logo">
                    <?php if(file_exists('assets/uploads/'.get_static_option('site_logo'))): ?>
                        <img src="<?php echo e(asset('assets/uploads/'.get_static_option('site_logo'))); ?>" alt="site logo">
                    <?php endif; ?>
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#weforyou_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="weforyou_main_menu">
            <ul class="navbar-nav">
                <li class="<?php echo e(active_menu_frontend('/')); ?>"><a href="<?php echo e(url('/')); ?>"><?php echo e(__('Home')); ?></a></li>
                <li class="<?php echo e(active_menu_frontend('donors')); ?>"><a href="<?php echo e(route('frontend.donors')); ?>"><?php echo e(__('Donors')); ?></a></li>
                <li class="<?php echo e(active_menu_frontend('about')); ?>"><a href="<?php echo e(route('frontend.about')); ?>"><?php echo e(__('About')); ?></a></li>
                <!-- <li class="<?php echo e(active_menu_frontend('volunteer')); ?>"><a href="<?php echo e(route('frontend.volunteer')); ?>"><?php echo e(__('Volunteer')); ?></a></li> -->
                
               <li class="<?php echo e(active_menu_frontend('blog')); ?>"><a href="<?php echo e(route('frontend.blog')); ?>"><?php echo e(__('Blog')); ?></a></li> 
                <li class="<?php echo e(active_menu_frontend('contact')); ?>"><a href="<?php echo e(route('frontend.contact')); ?>"><?php echo e(__('Contact')); ?></a></li>
                <?php if(Auth::check()): ?>
                <li class="menu-item-has-children">
                    <a href="javascript:void(0)"><?php echo e(auth()->user()->username); ?></a>
                    <ul class="sub-menu">
                        <?php if(is_donor()): ?>
                            <li><a href="<?php echo e(route('donor.dashboard')); ?>"><?php echo e(__('Dashboard')); ?></a></li>
                            <li><a href="<?php echo e(route('donor.edit.profile')); ?>"><?php echo e(__('Edit Profile')); ?></a></li>
                            <li><a href="<?php echo e(route('donor.edit.donor.info')); ?>"><?php echo e(__('Edit Donor Info')); ?></a></li>
                            <li><a href="<?php echo e(route('user.change.password')); ?>"><?php echo e(__('Change Password')); ?></a></li>
                        <?php else: ?>
                        <li><a href="<?php echo e(route('user.dashboard')); ?>"><?php echo e(__('Dashboard')); ?></a></li>
                        <li><a href="<?php echo e(route('user.edit.profile')); ?>"><?php echo e(__('Edit Profile')); ?></a></li>
                        <li><a href="<?php echo e(route('user.change.password')); ?>"><?php echo e(__('Change Password')); ?></a></li>
                        <li><a href="<?php echo e(route('user.join.donor')); ?>"><?php echo e(__('Join As Donor')); ?></a></li>
                        <?php endif; ?>
                       <li>
                           <a href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                               <?php echo e(__('Logout')); ?>

                           </a>
                       </li>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo csrf_field(); ?>
                        </form>
                    </ul>
                </li>
                <?php else: ?>
                <li class="menu-btn"><a href="<?php echo e(route('frontend.join.donor')); ?>"><?php echo e(__('Join as Donor')); ?></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/partials/navbar.blade.php ENDPATH**/ ?>