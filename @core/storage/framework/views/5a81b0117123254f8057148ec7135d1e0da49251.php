<div class="testimonial-area padding-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="section-title margin-bottom-60">
                    <h2 class="title"><?php echo e(__('What People Say\'s')); ?></h2>
                    <span class="separator"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="testimonial-carousel owl-carousel">
                    <?php $__currentLoopData = $all_testimonial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="single-testimonial-item">
                        <div class="thumb">
                            <?php if(file_exists('assets/uploads/testimonial/testimonial-pic-'.$data->id.'.'.$data->image)): ?>
                                <img style="max-width: 200px" src="<?php echo e(asset('assets/uploads/testimonial/testimonial-pic-'.$data->id.'.'.$data->image)); ?>" alt="<?php echo e(__($data->name)); ?>">

                            <?php else: ?>
                                <img style="max-width: 200px" src="<?php echo e(asset('assets/uploads/no-image.png')); ?>" alt="no image available">
                            <?php endif; ?>
                        </div>
                        <div class="content">
                            <p><?php echo e($data->description); ?></p>
                            <div class="author-meta">
                                <div class="ratings">
                                    <?php for($i = 0; $i < $data->rating;$i++): ?>
                                        <i class="fas fa-star"></i>
                                    <?php endfor; ?>
                                </div>
                                <h4 class="title"><?php echo e($data->name); ?></h4>
                                <span class="designation"><?php echo e($data->designation); ?></span>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/partials/testimonial.blade.php ENDPATH**/ ?>