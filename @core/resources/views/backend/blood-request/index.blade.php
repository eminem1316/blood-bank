@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 0 !important;
        }
        div.dataTables_wrapper div.dataTables_length select {
            width: 60px;
            display: inline-block;
        }
    </style>
@endsection
@section('site-title')
    {{__('All Blood Request')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    @include('backend/partials/message')
                                    <h4 class="header-title">{{__('All Blood Request')}}</h4>
                                    <div class="data-tables datatable-primary">
                                        <table id="all_blood_log_table" class="text-center">
                                            <thead class="text-capitalize">
                                            <tr>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Email')}}</th>
                                                <th>{{__('Blood Group')}}</th>
                                                <th>{{__('Mobile')}}</th>
                                                <th>{{__('Number Of Unit')}}</th>
                                                <th>{{__('Illness')}}</th>
                                                <th>{{__('Status')}}</th>
                                                <th>{{__('Date')}}</th>
                                                <th>{{__('Action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($all_blood_requests as $data)
                                                <tr>
                                                    <td>{{$data->id}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{strtoupper($data->blood_group)}}</td>
                                                    <td>{{$data->mobile}}</td>
                                                    <td>{{$data->number_of_units}}</td>
                                                    <td>{{$data->illness}}</td>
                                                    <td>
                                                        @if(!empty($data->status))
                                                            <div class="alert alert-success">{{__('Donated')}}</div>
                                                        @else
                                                            <from method="post" action="" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                                <button type="submit" class="btn btn-warning btn-xs">{{__('Waiting For Donor')}}</button>
                                                            </from>
                                                        @endif
                                                    </td>
                                                    <td>{{$data->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="" data-content="
                                                       <h6>Are you sure to delete this Blood Request?</h6>
                                                       <form method='post' action='{{route('admin.blood.request.delete',$data->id)}}'>
                                                       <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                                       <br>
                                                        <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                        </form>
                                                        " data-original-title="">
                                                            <i class="ti-trash"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="{{$data->id}}"
                                                           data-name="{{$data->name}}"
                                                           data-email="{{$data->email}}"
                                                           data-mobile="{{$data->mobile}}"
                                                           data-blood_group="{{$data->blood_group}}"
                                                           data-state="{{$data->state}}"
                                                           data-city="{{$data->city}}"
                                                           data-number_of_units="{{$data->number_of_units}}"
                                                           data-illness="{{$data->illness}}"
                                                           data-hospital_address="{{$data->hospital_address}}"
                                                           data-message="{{$data->message}}"
                                                           data-toggle="modal"
                                                           data-target="#blood_request_edit_modal"
                                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 blood_request_edit_btn"
                                                        >
                                                            <i class="ti-eye"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Primary table end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="blood_request_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Request Details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body">
                    <ul>
                        <li class="name"><strong>{{__('Name:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="email"><strong>{{__('Email:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="mobile"><strong>{{__('Mobile:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="blood_group"><strong>{{__('Blood Group:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="state"><strong>{{__('State:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="city"><strong>{{__('City:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="number_of_units"><strong>{{__('Number Of Units:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="illness"><strong>{{__('Illness:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="hospital_address"><strong>{{__('Hospital Address:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                        <li class="message"><strong>{{__('Message:')}}</strong> &nbsp; <span>{{__('dddd')}}</span></li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_blood_log_table').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
            $(document).on('click','.blood_request_edit_btn',function(e){
                e.preventDefault();
                var allData = $(this).data();
                delete allData.id;
                delete allData.target;
                delete allData.toggle;
                $.each(allData,function(key,value){
                    $('#blood_request_edit_modal').find('.'+key).children('span').text(value);
                });

                console.log(allData)
            })
        } );
    </script>
@endsection
