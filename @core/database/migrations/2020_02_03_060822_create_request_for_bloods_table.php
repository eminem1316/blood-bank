<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestForBloodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_for_bloods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_ud')->nullable();
            $table->text('name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('blood_group');
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('number_of_units')->nullable();
            $table->string('status')->nullable();
            $table->text('illness')->nullable();
            $table->text('hospital_address')->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_for_bloods');
    }
}
