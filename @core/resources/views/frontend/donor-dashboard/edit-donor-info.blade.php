@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Edit Donor Info')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Edit Donor Info')}}</h2>
                        @include('backend.partials.message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('donor.edit.donor.info')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="father_name">{{__('Father Name')}}</label>
                                <input type="text" class="form-control" name="father_name" id="father_name" value="{{auth()->user()->donor->father_name}}">
                            </div>
                            <div class="form-group">
                                <label for="mother_name">{{__('Mother Name')}}</label>
                                <input type="text" class="form-control" name="mother_name" id="mother_name" value="{{auth()->user()->donor->mother_name}}">
                            </div>
                            <div class="form-group">
                                <label for="present_address">{{__('Present Address')}}</label>
                                <textarea name="present_address" class="form-control max-height-100" id="present_address" cols="30" rows="10">{{auth()->user()->donor->present_address}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="permanent_address">{{__('Permanent Address')}}</label>
                                <textarea name="permanent_address" class="form-control max-height-100" id="permanent_address" cols="30" rows="10">{{auth()->user()->donor->permanent_address}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="mobile">{{__('Mobile')}}</label>
                                <input type="text" class="form-control" readonly value="{{auth()->user()->donor->mobile}}">
                            </div>
                            <div class="form-group">
                                <label for="birthday">{{__('Birthday')}}</label>
                                <input type="date" class="form-control " name="birthday" id="birthday" value="{{auth()->user()->donor->birthday}}">
                            </div>
                            <div class="form-group">
                                <label for="blood_group">{{__('Blood Group')}}</label>
                                <input type="text" class="form-control" name="blood_group" id="blood_group" value="{{auth()->user()->donor->blood_group}}">
                            </div>
                            @if(file_exists('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image))
                                <img style="max-width: 80px; margin-bottom: 20px" src="{{asset('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image)}}" alt="{{auth()->user()->name}}">
                                @php $img_url = asset('assets/uploads/donors/donor-pic-'.auth()->user()->donor->id.'.'.auth()->user()->donor->image) @endphp
                            @else
                                <img style="max-width: 80px; margin-bottom: 20px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                @php $img_url = asset('assets/uploads/no-image.png') @endphp
                            @endif
                            <div class="form-group">
                                <label for="image" class="d-block">{{__("Image")}}</label>
                                <input type="file" name="image" id="image">
                                <span class="text d-block text-danger">{{__('upload 300x300 size image will be better.')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="area_id">{{__('Area')}}</label>
                                <select name="area_id" class="form-control nice-select wide" id="area_id">
                                    <option value="">{{__('Area')}}</option>
                                    @foreach($all_areas as $data)
                                        <option @if(auth()->user()->donor->area_id == $data->id) selected @endif value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                               <button type="submit" class="submit-btn">{{__(('Save Change'))}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
