<?php

namespace App\Http\Controllers;

use App\Area;
use App\Donor;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_area = Area::all();
        return view('backend.donor-management.donors-area')->with([
            'all_area' => $all_area
        ]);
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:199|unique:areas'
        ]);

        Area::create($request->all());

        return redirect()->back()->with(['msg' => 'Area Added Success','type' => 'success']);
    }


    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name' => 'required|max:199|unique:areas'
        ]);

        Area::where('id',$id)->update(['name' => $request->name]);

        return redirect()->back()->with(['msg' => 'Area Update Success','type' => 'success']);
    }

    public function destroy(Request $request,$id)
    {
        if (Donor::where('area_id',$id)->first()){
            return redirect()->back()->with(['msg' => 'You can not delete this area, it associate with donors already.','type' => 'danger']);
        }else {

            Area::find($id)->delete();
            return redirect()->back()->with(['msg' => 'Area Deleted','type' => 'danger']);

        }
    }
}
