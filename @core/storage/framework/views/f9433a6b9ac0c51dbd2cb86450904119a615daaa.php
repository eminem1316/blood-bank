
<?php echo $__env->make('frontend.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="breadcrumb-area" style="background-image: url(<?php echo e(asset('assets/uploads/'.get_static_option('site_breadcrumb_bg'))); ?>);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h2 class="page-title"><?php echo $__env->yieldContent('page-title'); ?></h2>
                    <ul class="page-list">
                        <li><a href="<?php echo e(url('/')); ?>"><?php echo e(__('Home')); ?></a></li>
                        <li><a href="#"><?php echo $__env->yieldContent('page-title'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->yieldContent('content'); ?>

<?php echo $__env->make('frontend.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/frontend-page-master.blade.php ENDPATH**/ ?>