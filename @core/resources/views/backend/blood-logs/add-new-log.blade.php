@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('site-title')
    {{__('Add New Blood Log')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <!-- basic form start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('New Blood Log')}}</h4>
                        @include('backend/partials/message')
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('admin.new.blood.logs')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="mobile">{{__('Donor Username')}}</label>
                                <input type="text" class="form-control" id="mobile"  name="name" placeholder="{{__('Enter Donor Username')}}">
                                <small>Type username of donor all the below information will fill up automatically</small>
                            </div>
                            <div class="form-group">
                                <label for="name">{{__('Donor Name')}}</label>
                                <input type="text" class="form-control" id="name" readonly  name="name" placeholder="{{__('Enter Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="donors_id">{{__('Donors ID')}}</label>
                                <input type="text" class="form-control" readonly id="donors_id"  name="donors_id" placeholder="{{__('Donors ID')}}">
                            </div>
                            <div class="form-group">
                                <label for="blood_group_count">{{__('Total Blood Donate')}}</label>
                                <input type="text" class="form-control" readonly id="blood_group_count"  name="blood_group_count" placeholder="{{__('Total Blood Donate')}}">
                            </div>
                            <div class="form-group">
                                <label for="blood_group">{{__('Donor Blood Group')}}</label>
                                <input type="text" class="form-control" readonly id="blood_group" name="blood_group" placeholder="{{__('Blood Group')}}">
                            </div>
                            <div class="form-group">
                                <label for="receiver_name">{{__('Receiver Name')}}</label>
                                <input type="text" class="form-control" id="receiver_name" name="receiver_name" placeholder="{{__('Reveiver Name')}}">
                            </div>
                            <div class="form-group">
                                <label for="receiver_address">{{__('Receiver Address')}}</label>
                                <textarea class="form-control" id="receiver_address" name="receiver_address" placeholder="{{__('Receiver Address')}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="receiver_mobile">{{__('Receiver Mobile')}}</label>
                                <input type="text" class="form-control" id="receiver_mobile" name="receiver_mobile" placeholder="{{__('Receiver Mobile')}}">
                            </div>

                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Add New Blood Logs')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#mobile").autocomplete({
                minLength:1,
                source: function(request,response){
                    $.ajax({
                        url: "{{route('admin.search.donor')}}",
                        type:'post',
                        data: {
                            q: request.term,
                            _token: "{{csrf_token()}}"
                        },
                        success: function( data ) {
                             response( data );
                        }
                    });
                },
                select: function( event, ui ) {
                    $.ajax({
                        url: "{{route('admin.search.donor.details')}}",
                        type:'post',
                        data: {
                            q: ui.item.value,
                            _token: "{{csrf_token()}}"
                        },
                        success: function( data ) {
                            $('#name').val(data.name);
                            $('#donors_id').val(data.id);
                            $('#blood_group_count').val(data.blood_donate_count);
                            $('#blood_group').val(data.blood_group.toUpperCase());
                        }
                    });
                }

            });
        } );
    </script>
@endsection
