@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 0 !important;
        }
        div.dataTables_wrapper div.dataTables_length select {
            width: 60px;
            display: inline-block;
        }
    </style>
@endsection
@section('site-title')
    {{__('All Donor')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    @include('backend/partials/message')
                                    <h4 class="header-title">{{__('All Donors')}}</h4>
                                    <div class="data-tables datatable-primary">
                                        <table id="all_donor_table" class="text-center">
                                            <thead class="text-capitalize">
                                            <tr>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Image')}}</th>
                                                <th>{{__('Mobile')}}</th>
                                                <th>{{__('Blood Group')}}</th>
                                                <th>{{__('Area')}}</th>
                                                <th>{{__('Total Donate')}}</th>
                                                <th>{{__('Status')}}</th>
                                                <th>{{__('Action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($all_donor as $data)
                                                @php $img_url = null;@endphp
                                                <tr>
                                                    <td>{{$data->id}}</td>
                                                    <td>{{ucfirst($data->user->name)}}</td>
                                                    <td>
                                                        @if(file_exists('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image))
                                                            <img style="max-width: 80px" src="{{asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)}}" alt="{{__($data->name)}}">
                                                            @php $img_url = asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image) @endphp
                                                        @else
                                                            <img style="max-width: 80px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                                            @php $img_url = asset('assets/uploads/no-image.png') @endphp
                                                        @endif
                                                    </td>
                                                    <td>{{$data->mobile}}</td>
                                                    <td>{{strtoupper($data->blood_group)}}</td>
                                                    <td>{{ucfirst($data->area->name)}}</td>
                                                    <td>{{$data->blood_donate_count}}</td>
                                                    <td>
                                                        @if( 1 == $data->status)
                                                            <span class="btn btn-success">{{__('Approved')}}</span>
                                                        @else
                                                            <a href="#" class="btn btn-warning donor_approve_btn" >{{__('Awaiting for Approval')}}</a>
                                                            <form action="{{route('admin.donor.approve')}}" class="donor_approve" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="donor_id" value="{{$data->id}}">
                                                            </form>
                                                        @endif
                                                    </td>
                                                    <td>
                                                         <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="" data-content="
                                                       <h6>Are you sure to delete this Donor Information?</h6>
                                                       <form method='post' action='{{route('admin.donor.delete',$data->id)}}'>
                                                       <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                                       <br>
                                                        <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                        </form>
                                                        " data-original-title="">
                                                            <i class="ti-trash"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="{{$data->id}}"
                                                           data-name="{{$data->user->name}}"
                                                           data-username="{{$data->user->username}}"
                                                           data-email="{{$data->user->email}}"
                                                           data-father="{{$data->father_name}}"
                                                           data-mother="{{$data->mother_name}}"
                                                           data-presentAddress="{{$data->present_address}}"
                                                           data-permanentAddress="{{$data->permanent_address}}"
                                                           data-mobile="{{$data->mobile}}"
                                                           data-birthday="{{$data->birthday}}"
                                                           data-bloodGroup="{{$data->blood_group}}"
                                                           data-area="{{$data->area_id}}"
                                                           data-image="{{$img_url}}"
                                                           data-toggle="modal"
                                                           data-target="#donor_edit_modal"
                                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 donor_edit_btn"
                                                        >
                                                            <i class="ti-pencil"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="{{$data->id}}"
                                                           data-toggle="modal"
                                                           data-target="#donor_change_password_modal"
                                                           class="btn btn-lg btn-info btn-sm mb-3 mr-1 donor_change_password_btn"
                                                        >
                                                           {{__("Change Password")}}
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Primary table end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="donor_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Donor Edit Details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('admin.donor.update')}}" id="donor_edit_modal_form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="donor_id" id="donor_id">
                        <div class="form-group">
                            <label for="edit_username">{{__('Username')}}</label>
                            <input type="text" class="form-control" readonly id="edit_username">
                        </div>
                        <div class="form-group">
                            <label for="name">{{__('Name')}}</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{__('Enter Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="email">{{__('Email')}}</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="{{__('Enter Email')}}">
                        </div>
                        <div class="form-group">
                            <label for="father_name">{{__('Father Name')}}</label>
                            <input type="text" class="form-control" id="father_name" name="father_name" placeholder="{{__('Father Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="mother_name">{{__('Mother Name')}}</label>
                            <input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="{{__('Mother Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="present_address">{{__('Present Address')}}</label>
                            <textarea class="form-control" id="present_address" name="present_address" placeholder="{{__('Present Address')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="permanent_address">{{__('Permanent Address')}}</label>
                            <textarea class="form-control" id="permanent_address" name="permanent_address" placeholder="{{__('Permanent Address')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="mobile">{{__('Mobile')}}</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__('Mobile')}}">
                        </div>
                        <div class="form-group">
                            <label for="birthday">{{__('Birthday')}}</label>
                            <input type="date" class="form-control" id="birthday" name="birthday" placeholder="{{__('Birthday')}}">
                        </div>
                        <div class="form-group">
                            <label for="blood_group">{{__('Blood Group')}}</label>
                            <select name="blood_group"  id="blood_group" class="form-control">
                                <option value="o+">O+</option>
                                <option value="o-">O-</option>
                                <option value="b+">B+</option>
                                <option value="b-">B-</option>
                                <option value="a+">A+</option>
                                <option value="a-">A-</option>
                                <option value="ab+">AB+</option>
                                <option value="ab-">AB-</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="area">{{__('Areas')}}</label>
                            <select name="area"  id="area" class="form-control">
                                @foreach($all_donor_area as $area)
                                <option value="{{$area->id}}">{{$area->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <img src="" id="donor_edit_modal_prev_img" style="width:80px; margin-bottom: 10px;" alt="edit donor">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="image" id="image">
                            <label class="custom-file-label" for="image">{{__('Image')}}</label>
                        </div>
                    </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="donor_change_password_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Change Donor Password')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('admin.donor.password.change')}}" id="donor_password_change_modal_form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="ch_donor_id" id="ch_donor_id">
                        <div class="form-group">
                            <label for="password">{{__('Password')}}</label>
                            <input type="password" class="form-control" name="password" placeholder="{{__('Enter Password')}}">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{__('Confirm Password')}}</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="{{__('Confirm Password')}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Change Password')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_donor_table').DataTable( {
                "order": [[ 0, "desc" ]]
            } );
            $(document).on('click','.donor_approve_btn',function(e){
                e.preventDefault();
                $(this).next('form').submit();
            });

            $(document).on('click','.donor_edit_btn',function(e){
                e.preventDefault();
                var form = $('#donor_edit_modal_form');
                var el = $(this);
                form.find('#edit_username').val(el.data('username'));
                form.find('#donor_id').val(el.data('id'));
                form.find('#name').val(el.data('name'));
                form.find('#email').val(el.data('email'));
                form.find('#father_name').val(el.data('father'));
                form.find('#mother_name').val(el.data('mother'));
                form.find('#present_address').val(el.data('presentaddress'));
                form.find('#permanent_address').val(el.data('permanentaddress'));
                form.find('#mobile').val(el.data('mobile'));
                form.find('#birthday').val(el.data('birthday'));
                form.find('#blood_group').val(el.data('bloodgroup'));
                $('#area option[value="'+el.data('area')+'"]').attr('selected','selected');
                $('#donor_edit_modal_prev_img').attr('src',el.data('image'));
            });

            $(document).on('click','.donor_change_password_btn',function (e) {
                //donor_password_change_modal_form
                e.preventDefault();
                var form = $('#donor_password_change_modal_form');
                var el = $(this);
                form.find('#ch_donor_id').val(el.data('id'));

            });

        } );
    </script>
@endsection
