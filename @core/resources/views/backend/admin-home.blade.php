@extends('backend.admin-master')
@section('site-title')
    {{__('Dashboard')}}
@endsection
@section('content')

    <div class="main-content-inner">
        <div class="row">
            <!-- seo fact area start -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4 mt-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg1">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-thumb-up"></i> {{__('Total Donors')}}</div>
                                    <h2>{{$total_donor}}</h2>
                                </div>
                                <canvas id="seolinechart1" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg2">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-share"></i> {{__('Total Volunteer Member')}}</div>
                                    <h2>{{$total_volunteer_member}}</h2>
                                </div>
                                <canvas id="seolinechart2" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg3">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-share"></i> {{__('Total Blood Donate')}}</div>
                                    <h2>{{$total_blood_donate}}</h2>
                                </div>
                                <canvas id="seolinechart2" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- seo fact area end -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Last 10 Blood Donation List')}}</h4>
                        <div class="single-table">
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('Donor Name')}}</th>
                                        <th>{{__('Blood Group')}}</th>
                                        <th>{{__('Receiver Name')}}</th>
                                        <th>{{__('Receiver Address')}}</th>
                                        <th>{{__('Receiver Mobile')}}</th>
                                        <th>{{__('Date')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($all_blood_logs as $data)
                                        <tr>
                                            <td>{{$data->id}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{strtoupper($data->blood_group)}}</td>
                                            <td>{{$data->receiver_name}}</td>
                                            <td>{{$data->receiver_address}}</td>
                                            <td>{{$data->receiver_mobile}}</td>
                                            <td>{{$data->created_at->diffForHumans()}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
