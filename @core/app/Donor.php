<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected $table = 'donors';
    protected $fillable = [
        'father_name',
        'mother_name',
        'present_address',
        'permanent_address',
        'mobile',
        'image',
        'birthday',
        'blood_group',
        'area_id',
        'blood_donate_count',
        'status',
        'user_id'
    ];

    public function area(){
        return $this->belongsTo('App\Area');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
