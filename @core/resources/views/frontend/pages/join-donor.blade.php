@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Join As Donor')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="righti-content-area">
                        <div class="contact-page-form-wrap">
                            @include('backend.partials.message')
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('frontend.register.donor')}}" method="post" id="contact_page_form" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="{{__('Your Name')}}" value="{{(old('name'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="username" placeholder="{{__('Your Username')}}" value="{{(old('username'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="{{__('Your Email')}}" value="{{(old('email'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="{{__("Mobile")}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="date" class="form-control" id="birthday" name="birthday">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="{{__('Your Password')}}"  class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" placeholder="{{__('Confirm Password')}}"  class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <select name="blood_group" id="blood_group" class="form-control nice-select wide">
                                                <option value="">{{__('Blood Group')}}</option>
                                                <option value="o+">O+</option>
                                                <option value="o-">O-</option>
                                                <option value="b+">B+</option>
                                                <option value="b-">B-</option>
                                                <option value="a+">A+</option>
                                                <option value="a-">A-</option>
                                                <option value="ab+">AB+</option>
                                                <option value="ab-">AB-</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="area_id" class="form-control nice-select wide" id="area-id">
                                                <option value="">{{__('Select Area')}}</option>
                                                @foreach($all_area as $data)
                                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="{{__('Register As Donor')}}" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
