<div class="widget-area">
    <div class="widget widget_search">
        <form action="{{route('frontend.blog.search')}}" method="get" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" name="search" placeholder="Search">
            </div>
            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <div class="widget widget_nav_menu">
        <h3 class="widget-title">{{get_static_option('blog_page_category_widget_title')}}</h3>
        <ul>
            @foreach($all_categories as $data)
                <li><a href="{{route('frontend.blog.category',['id' => $data->id,'any'=> Str::slug($data->name,'-')])}}">{{ucfirst($data->name)}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="widget widget_recent_posts">
        <h4 class="widget-title">{{get_static_option('blog_page_recent_post_widget_title')}}</h4>
        <ul class="recent_post_item">
            @foreach($all_recent_blogs as $data)
                <li class="single-recent-post-item">
                    <div class="thumb">
                        @if(file_exists('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image))
                            <img src="{{asset('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)}}" alt="{{$data->title}}">
                        @endif
                    </div>
                    <div class="content">
                        <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                        <span class="time"><i class="fa fa-calendar"></i> {{$data->created_at->diffForHumans()}}</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
