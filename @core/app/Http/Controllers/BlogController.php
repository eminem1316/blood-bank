<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;


class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $all_blog = Blog::orderBy('id','desc')->get();
        return view('backend.pages.blog.index')->with([
            'all_blog' => $all_blog
        ]);
    }
    public function new_blog(){
        $all_category = BlogCategory::all();
        return view('backend.pages.blog.new')->with([
            'all_category' => $all_category
        ]);
    }
    public function store_new_blog(Request $request){
        $this->validate($request,[
           'category' => 'required',
           'blog_content' => 'required',
           'tags' => 'required',
           'title' => 'required',
           'image' => 'mimes:jpg,jpeg,png|max:2064',
        ]);

        $id = Blog::create([
            'blog_categories_id' => $request->category,
            'content' => $request->blog_content,
            'tags' => $request->tags,
            'title' => $request->title,
            'user_id' => Auth::user()->id
        ])->id;

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_grid = 'blog-grid-'.$id.'.'.$image_extenstion;
            $image_large = 'blog-large-'.$id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $folder_path = 'assets/uploads/blog/';
                $resize_grid_image = Image::make($image)->resize(350,270);
                $resize_large_image = Image::make($image)->resize(730,400);
                $resize_grid_image->save($folder_path.$image_grid);
                $resize_large_image->save($folder_path.$image_large);
                Blog::where('id',$id)->update(['image'=>$image_extenstion]);
            }
        }
        return redirect()->back()->with([
            'msg' => 'New Blog Post Added...',
            'type' => 'success'
        ]);
    }
    public function edit_blog($id){
        $all_category = BlogCategory::all();
        $blog_post = Blog::find($id);
        return view('backend.pages.blog.edit')->with([
            'all_category' => $all_category,
            'blog_post' => $blog_post,
        ]);
    }
    public function update_blog(Request $request,$id){
        $this->validate($request,[
            'category' => 'required',
            'blog_content' => 'required',
            'tags' => 'required',
            'title' => 'required',
            'image' => 'mimes:jpg,jpeg,png|max:2064',
        ]);
        Blog::where('id',$id)->update([
            'blog_categories_id' => $request->category,
            'content' => $request->blog_content,
            'tags' => $request->tags,
            'title' => $request->title
        ]);

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_grid = 'blog-grid-'.$id.'.'.$image_extenstion;
            $image_large = 'blog-large-'.$id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $folder_path = 'assets/uploads/blog/';
                $resize_grid_image = Image::make($image)->resize(350,270);
                $resize_large_image = Image::make($image)->resize(730,400);
                $resize_grid_image->save($folder_path.$image_grid);
                $resize_large_image->save($folder_path.$image_large);
                Blog::where('id',$id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with([
            'msg' => 'Blog Post updated...',
            'type' => 'success'
        ]);
    }
    public function delete_blog(Request $request,$id){
        Blog::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Blog Post Delete Success...',
            'type' => 'danger'
        ]);
    }

    public function category(){
        $all_category = BlogCategory::all();
        return view('backend.pages.blog.category')->with([
            'all_category' => $all_category
        ]);
    }
    public function new_category(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);

        BlogCategory::create($request->all());

        return redirect()->back()->with([
            'msg' => 'New Category Added...',
            'type' => 'success'
        ]);
    }

    public function update_category(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'status' => 'required|string|max:191'
        ]);

        BlogCategory::find($request->id)->update([
            'name' => $request->name,
            'status' => $request->status,
        ]);

        return redirect()->back()->with([
            'msg' => 'Category Update Success...',
            'type' => 'success'
        ]);
    }

    public function delete_category(Request $request,$id){
        if (Blog::where('blog_categories_id',$id)->first()){
            return redirect()->back()->with([
                'msg' => 'You Can Not Delete This Category, It Already Associated With A Post...',
                'type' => 'danger'
            ]);
        }
        BlogCategory::find($id)->delete();
        return redirect()->back()->with([
            'msg' => 'Category Delete Success...',
            'type' => 'danger'
        ]);
    }


}
