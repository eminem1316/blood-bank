@extends('frontend.frontend-page-master')
@section('page-title')
    {{get_static_option('contact_page_title')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content-area">
                        <div class="inner-title margin-bottom-55">
                            <h3 class="title">{{get_static_option('contact_page_get_title')}}</h3>
                            <p>{{get_static_option('contact_page_get_description')}}</p>
                        </div>
                        <ul class="contact-info-list">
                            @foreach($contact_info_items as $data)
                                <li class="single-info-item">
                                    <div class="icon">
                                        <i class="{{$data->icon}}"></i>
                                    </div>
                                    <div class="content">
                                        @php
                                           $info_text =  explode(';',$data->description);
                                        @endphp
                                        @foreach ($info_text as $text)
                                        <span class="details">{{$text}}</span>
                                        @endforeach
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="righti-content-area">
                        <div class="contact-page-form-wrap">
                            @include('backend/partials/message')
                            @if($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <form action="{{route('frontend.contact')}}" id="contact_page_form" method="post" class="contact-page-form" novalidate="novalidate">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="{{__('Your Name')}}" class="form-control" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="{{__('Your Email')}}" class="form-control" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="text" name="subject" placeholder="{{__('Your Subject')}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" cols="30" rows="10" required="" placeholder="{{__('Message')}}" ></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="{{__("Submit Message")}}" class="submit-btn">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-map">
        <div id="map" data-latitude="{{get_static_option('contact_page_latitude')}}" data-longitude="{{get_static_option('contact_page_longitude')}}"></div>
    </div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7eALQrRUekFNQX71IBNkxUXcz-ALS-MY&callback=initMap" async defer></script>
    <script src="{{asset('assets/frontend/js/goolge-map-activate.js')}}"></script>
@endsection
