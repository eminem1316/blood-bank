<?php
namespace App\Imports;

use App\Area;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;

class DonorsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
           User::create([
              'name' => $row[0]
           ]);
        }
    }

}

//namespace App\Imports;
//
//use App\Area;
//use App\User;
//use Maatwebsite\Excel\Concerns\ToModel;
//use Maatwebsite\Excel\Concerns\Importable;
//use Illuminate\Support\Str;
//
//class DonorsImport implements ToModel
//{
//    use Importable;
//
//    public function model(array $row)
//    {
//        return new Area([
//            'name' => $row[0],
//        ]);
//    }
//    public function batchSize(): int
//    {
//        return 1000;
//    }
//
//    public function chunkSize(): int
//    {
//        return 1000;
//    }
//
//}
//
//namespace App\Imports;
//
//use App\Donor;
//use App\User;
//use Illuminate\Support\Collection;
//use Illuminate\Support\Str;
//use Maatwebsite\Excel\Concerns\ToCollection;
//
//class DonorsImport implements ToCollection
//{
//    public function collection(Collection $rows)
//    {
//        $a = 10;
//        foreach ($rows as $row)
//        {
//            $get_user_info = User::where('email',$row[1])->first();
//            $a++;
//            if (empty($get_user_info)){
////                $name = $row[0];
////                $email = $row[1];
////                $id = User::create([
////                    'password' => bcrypt(12345678),
////                    'username' => Str::slug($row[0]),
////                    'email' => $row[1],
////                    'name' => $row[0],
////                ])->id;
//                $message = $row[0].' '.$row[1];
//                file_put_contents('excel.txt',$message);
//                  Donor::create([
//                    'father_name' => $row[2],
//                    'mother_name' => $row[3],
//                    'present_address' => $row[4],
//                    'permanent_address' => $row[5],
//                    'mobile' => $row[6],
//                    'birthday' => $row[7],
//                    'blood_group' => $row[8],
//                    'area_id' => $row[9],
//                    'user_id' => $a,
//                    'status' => 1
//                ]);
//            }
//        }
//    }
//}
//
