@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Forget Password')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="contact-page-form-wrap login-page">
                            <h2 class="title">{{__('Forget Password')}}</h2>
                            @include('backend.partials.message')
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('user.forget.password')}}" method="post" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="username">{{__('Your Username Or Email')}}</label>
                                    <input type="text" name="username" placeholder="Enter Username or Email" class="form-control" required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="{{__('Login')}}" class="submit-btn register-as-donor">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
