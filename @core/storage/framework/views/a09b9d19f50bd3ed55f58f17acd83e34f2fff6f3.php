<?php $__env->startSection('page-title'); ?>
    <?php echo e(__("All Available Donors In ".strtoupper(request()->route()->parameters['id']))); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <section class="dedicated-team-area padding-120 ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php if(count($all_donors) < 1): ?>
                        <div class="col-lg-12">
                            <div class="alert alert-danger"><?php echo e(__('Sorry No '.strtoupper(request()->route()->parameters['id']).' Group Donor Available !!')); ?></div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php $__currentLoopData = $all_donors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donors-item margin-bottom-30">
                        <div class="thumb">
                            <?php if(file_exists('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)): ?>
                                <img src="<?php echo e(asset('assets/uploads/donors/donor-pic-'.$data->id.'.'.$data->image)); ?>" alt="<?php echo e(__($data->name)); ?>">
                            <?php else: ?>
                                <img src="<?php echo e(asset('assets/uploads/no-image.png')); ?>" alt="no image available">
                            <?php endif; ?>
                        </div>
                        <div class="content">
                            <a href="<?php echo e(route('frontend.donors.profile',['id' => $data->id,'slug' => Str::slug($data->user->name)])); ?>"> <h4 class="title"><?php echo e($data->user->name); ?></h4></a>
                            <span class="blood-group"><?php echo e(__('Blood Group:')); ?> <strong><?php echo e(strtoupper($data->blood_group)); ?></strong></span>
                            <span class="total-donate"><?php echo e(__('Total Donate:')); ?> <strong><?php echo e(get_total_donate($data->id)); ?></strong> <?php echo e(__('Times')); ?></span>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-12">
                    <nav class="pagination-wrapper" aria-label="Page navigation ">
                        <?php echo e($all_donors->links()); ?>

                    </nav>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-page-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/pages/donors-by-blood-group.blade.php ENDPATH**/ ?>