@extends('backend.admin-master')
@section('site-title')
    {{__('Header Settings')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <!-- basic form start -->
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('Header Settings')}}</h4>
                        <form action="{{route('admin.home.header')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="home_page_header_title">{{__('Title')}}</label>
                                <input type="text" class="form-control"  id="home_page_header_title" value="{{get_static_option('home_page_header_title')}}" name="home_page_header_title" placeholder="{{__('Title')}}">
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_description">{{__('Description')}}</label>
                                <textarea class="form-control"  id="home_page_header_description" name="home_page_header_description" placeholder="{{__('Description')}}">{{get_static_option('home_page_header_description')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_btn_one_text">{{__('Button One Text')}}</label>
                                <input type="text" class="form-control"  id="home_page_header_btn_one_text" value="{{get_static_option('home_page_header_btn_one_text')}}" name="home_page_header_btn_one_text" placeholder="{{__('Button One Text')}}">
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_btn_one_url">{{__('Button One URL')}}</label>
                                <input type="text" class="form-control"  id="home_page_header_btn_one_url" value="{{get_static_option('home_page_header_btn_one_url')}}" name="home_page_header_btn_one_url" placeholder="{{__('Button One URL')}}">
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_btn_two_text">{{__('Button Two Text')}}</label>
                                <input type="text" class="form-control"  id="home_page_header_btn_two_text" value="{{get_static_option('home_page_header_btn_two_text')}}" name="home_page_header_btn_two_text" placeholder="{{__('Button Two Text')}}">
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_btn_two_url">{{__('Button Two URL')}}</label>
                                <input type="text" class="form-control"  id="home_page_header_btn_two_url" value="{{get_static_option('home_page_header_btn_two_url')}}" name="home_page_header_btn_two_url" placeholder="{{__('Button Two Url')}}">
                            </div>
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('home_page_header_right_image')))
                                    <img src="{{asset('assets/uploads/'.get_static_option('home_page_header_right_image'))}}" style="max-width: 200px" alt="">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="home_page_header_right_image">{{__('Right Image')}}</label>
                                <input type="file" class="form-control"  id="home_page_header_right_image" name="home_page_header_right_image">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Header Info')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
