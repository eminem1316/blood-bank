@extends('frontend.frontend-page-master')
@section('page-title')
    {{get_static_option('volunteer_page_title')}}
@endsection
@section('content')
    <section class="dedicated-team-area padding-120 ">
        <div class="container">
            <div class="row">
                @foreach($all_volunteer as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="single-team-member margin-bottom-30">
                        <div class="thumb">
                            <span class="blood-group">{{$data->blood_group}}</span>
                            @if(file_exists('assets/uploads/volunteers/volunteers-pic-'.$data->id.'.'.$data->image))
                                <img style="max-width: 200px" src="{{asset('assets/uploads/volunteers/volunteers-pic-'.$data->id.'.'.$data->image)}}" alt="{{__($data->name)}}">
                            @else
                                <img style="max-width: 200px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                            @endif
                        </div>
                        <div class="content">
                            <h4 class="title">{{ucfirst($data->name)}}</h4>
                            <span class="designation">{{ucfirst($data->designation)}}</span>
                            <ul class="social-icon">
                                @if(!empty($data->facebook))
                                    <li><a href="{{$data->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                @endif
                                @if(!empty($data->twitter))
                                     <li><a href="{{$data->twitter}}"><i class="fab fa-twitter"></i></a></li>
                                @endif
                                @if(!empty($data->instagram))
                                     <li><a href="{{$data->instagram}}"><i class="fab fa-instagram"></i></a></li>
                                @endif
                                @if(!empty($data->linkedin))
                                    <li><a href="{{$data->linkedin}}"><i class="fab fa-linkedin"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="col-lg-12">
                    <nav class="pagrination-wrapper" aria-label="Page navigation ">
                        {{$all_volunteer->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
