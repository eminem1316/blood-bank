@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Edit Profile')}}
@endsection
@section('content')
    <div class="donor-dashboard-page-content padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="donor-content-area">
                        <h2 class="title">{{__('Edit Profile')}}</h2>
                        @include('backend.partials.message')
                        <form action="{{route('user.edit.profile')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" readonly value="{{auth()->user()->username}}">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" value="{{auth()->user()->name}}">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" id="email" value="{{auth()->user()->email}}">
                            </div>
                            <div class="form-group">
                               <button type="submit" class="submit-btn">{{__(('Save Change'))}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
