@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 0 !important;
        }
        div.dataTables_wrapper div.dataTables_length select {
            width: 60px;
            display: inline-block;
        }
    </style>
@endsection
@section('site-title')
    {{__('All Blood Logs')}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    @include('backend/partials/message')
                                    <h4 class="header-title">{{__('All Blood Logs')}}</h4>
                                    <div class="data-tables datatable-primary">
                                        <table id="all_blood_log_table" class="text-center">
                                            <thead class="text-capitalize">
                                            <tr>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Donor Name')}}</th>
                                                <th>{{__('Blood Group')}}</th>
                                                <th>{{__('Receiver Name')}}</th>
                                                <th>{{__('Receiver Address')}}</th>
                                                <th>{{__('Receiver Mobile')}}</th>
                                                <th>{{__('Date')}}</th>
                                                <th>{{__('Action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($all_blood_logs as $data)
                                                <tr>
                                                    <td>{{$data->id}}</td>
                                                    <td>{{$data->name}}</td>
                                                    <td>{{strtoupper($data->blood_group)}}</td>
                                                    <td>{{$data->receiver_name}}</td>
                                                    <td>{{$data->receiver_address}}</td>
                                                    <td>{{$data->receiver_mobile}}</td>
                                                    <td>{{$data->created_at->diffForHumans()}}</td>
                                                    <td>
                                                        <a tabindex="0" class="btn btn-lg btn-danger btn-sm mb-3 mr-1" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="" data-content="
                                                       <h6>Are you sure to delete this Blood Log?</h6>
                                                       <form method='post' action='{{route('admin.blood.log.delete',$data->id)}}'>
                                                       <input type='hidden' name='_token' value='{{csrf_token()}}'>
                                                       <br>
                                                        <input type='submit' class='btn btn-danger btn-sm' value='Yes,Delete'>
                                                        </form>
                                                        " data-original-title="">
                                                            <i class="ti-trash"></i>
                                                        </a>
                                                        <a href="#"
                                                           data-id="{{$data->id}}"
                                                           data-donorName="{{$data->name}}"
                                                           data-donorId="{{$data->donors_id}}"
                                                           data-receiverName="{{$data->receiver_name}}"
                                                           data-receiverAddress="{{$data->receiver_address}}"
                                                           data-receiverMobile="{{$data->receiver_mobile}}"
                                                           data-bloodGroup="{{$data->bloodgroup}}"
                                                           data-toggle="modal"
                                                           data-target="#blood_log_edit_modal"
                                                           class="btn btn-lg btn-primary btn-sm mb-3 mr-1 blood_log_edit_btn"
                                                        >
                                                            <i class="ti-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Primary table end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="blood_log_edit_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('Donor Edit Details')}}</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('admin.blood.log.update')}}" id="blood_log_edit_modal_form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="blood_log_id" id="blood_log_id">
                        <div class="form-group">
                            <label for="donor_id">{{__('Donor ID')}}</label>
                            <input type="text" class="form-control" id="donor_id" readonly  name="donor_id" placeholder="{{__('Enter Donor Mobile')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{__('Donor Name')}}</label>
                            <input type="text" class="form-control" id="name" readonly  name="name" placeholder="{{__('Enter Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="blood_group">{{__('Donor Blood Group')}}</label>
                            <input type="text" class="form-control" readonly id="blood_group" name="blood_group" placeholder="{{__('Blood Group')}}">
                        </div>
                        <div class="form-group">
                            <label for="receiver_name">{{__('Receiver Name')}}</label>
                            <input type="text" class="form-control" id="receiver_name" name="receiver_name" placeholder="{{__('Reveiver Name')}}">
                        </div>
                        <div class="form-group">
                            <label for="receiver_address">{{__('Receiver Address')}}</label>
                            <textarea class="form-control" id="receiver_address" name="receiver_address" placeholder="{{__('Receiver Address')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="receiver_mobile">{{__('Receiver Mobile')}}</label>
                            <input type="text" class="form-control" id="receiver_mobile" name="receiver_mobile" placeholder="{{__('Receiver Mobile')}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_blood_log_table').DataTable( {
                "order": [[ 0, "desc" ]]
            } );

            $(document).on('click','.blood_log_edit_btn',function(e){
                e.preventDefault();
                var form = $('#blood_log_edit_modal_form');
                var el = $(this);
                form.find('#blood_log_id').val(el.data('id'));
                form.find('#name').val(el.data('donorname'));
                form.find('#donor_id').val(el.data('donorid'));
                form.find('#receiver_name').val(el.data('receivername'));
                form.find('#receiver_address').val(el.data('receiveraddress'));
                form.find('#receiver_mobile').val(el.data('receivermobile'));
                form.find('#blood_group').val(el.data('bloodgroup'));
            })

        } );
    </script>
@endsection
