<?php
use App\StaticOption;
use App\BloodLog;
use App\Donor;

function active_menu($url){

    return $url == request()->path() ? 'active' : '';
}
function active_menu_frontend($url){

    return $url == request()->path() ? 'current-menu-item' : '';
}


function check_image_extension($file){
    $extension = strtolower($file->getClientOriginalExtension());
    if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png' && $extension = 'gif') {
       return false ;
    }
    return true;
}


function sendEmail($to, $name, $subject, $message ,$from = '' ){
    $template = get_static_option('site_global_email_template');
    $from = get_static_option('site_global_email') ;

    $headers = "From: ".$from." \r\n";
    $headers .= "Reply-To: <$from> \r\n";
    $headers .= "Return-Path: ".($from) . "\r\n";;
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $headers .= "X-Priority: 2\nX-MSmail-Priority: high";;
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n";

    $mm = str_replace("@username",$name,$template);
    $message = str_replace("@message",$message,$mm);
    $message = str_replace("@company",get_static_option('site_title'),$message);

    if (mail($to, $subject, $message, $headers)){
        return true;
    }

}

function sendPlanEmail($to, $name, $subject, $message,$from){

    $headers = "From: ".$from." \r\n";
    $headers .= "Reply-To: <$from> \r\n";
    $headers .= "Return-Path: ".($from) . "\r\n";;
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $headers .= "X-Priority: 2\nX-MSmail-Priority: high";;
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n";

    if (mail($to, $subject, $message, $headers)){
        return true;
    }

}


function set_static_option($key,$value){
    if (!StaticOption::where('option_name',$key)->first()){
        StaticOption::create([
            'option_name' => $key,
            'option_value' => $value
        ]);
        return true;
    }
    return false;
}
function get_static_option($key){
    if (StaticOption::where('option_name',$key)->first()){
        $return_val = StaticOption::where('option_name',$key)->first();
        return $return_val->option_value;
    }
    return null;
}
function update_static_option($key,$value){
    if (!StaticOption::where('option_name',$key)->first()){
        StaticOption::create([
            'option_name' => $key,
            'option_value' => $value
        ]);
        return true;
    }else{
        StaticOption::where('option_name',$key)->update([
            'option_name' => $key,
            'option_value' => $value
        ]);
        return true;
    }
    return false;
}
function delete_static_option($key){
    if (!StaticOption::where('option_name',$key)->first()){
        StaticOption::where('option_name', $key)->delete();
        return true;
    }
    return false;
}

function get_total_donate($id){
    $return_val = 0;
    if (BloodLog::where('donors_id',$id)->count()){
        $return_val = BloodLog::where('donors_id',$id)->count();
    }

    return $return_val;

}

function is_donor(){
    $donor_info = Donor::where('user_id',auth()->user()->id)->first();
    if (!empty($donor_info)){
        return true;
    }
    return false;
}

function minify_css_lines($css){
    // some of the following functions to minimize the css-output are directly taken
    // from the awesome CSS JS Booster: https://github.com/Schepp/CSS-JS-Booster
    // all credits to Christian Schaefer: http://twitter.com/derSchepp
    // remove comments
    $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
    // backup values within single or double quotes
    preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
    for ($i=0; $i < count($hit[1]); $i++) {
        $css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);
    }
    // remove traling semicolon of selector's last property
    $css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);
    // remove any whitespace between semicolon and property-name
    $css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);
    // remove any whitespace surrounding property-colon
    $css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);
    // remove any whitespace surrounding selector-comma
    $css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);
    // remove any whitespace surrounding opening parenthesis
    $css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);
    // remove any whitespace between numbers and units
    $css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);
    // shorten zero-values
    $css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);
    // constrain multiple whitespaces
    $css = preg_replace('/\p{Zs}+/ims',' ', $css);
    // remove newlines
    $css = str_replace(array("\r\n", "\r", "\n"), '', $css);
    // Restore backupped values within single or double quotes
    for ($i=0; $i < count($hit[1]); $i++) {
        $css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);
    }

    return $css;
}