<?php $__env->startSection('content'); ?>
    <header class="header-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="left-content-area">
                        <h1 class="title"><?php echo e(get_static_option('home_page_header_title')); ?></h1>
                        <p><?php echo e(get_static_option('home_page_header_description')); ?></p>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(get_static_option('home_page_header_btn_one_url')); ?>" class="boxed-btn"><?php echo e(get_static_option('home_page_header_btn_one_text')); ?></a>
                            <a href="<?php echo e(get_static_option('home_page_header_btn_two_url')); ?>" class="boxed-btn blank"><?php echo e(get_static_option('home_page_header_btn_two_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="img-wrapper">
                            <?php if(file_exists('assets/uploads/'.get_static_option('home_page_header_right_image'))): ?>
                                <img src="<?php echo e(asset('assets/uploads/'.get_static_option('home_page_header_right_image'))); ?>"  alt="<?php echo e(get_static_option('home_page_header_title')); ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?php if(!empty(get_static_option('home_donor_search_section_status'))): ?>
    <?php echo $__env->make('frontend.partials.donor-search-form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_blood_donation_process_section_status'))): ?>
    <section class="blood-donation-process-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title"><?php echo e(get_static_option('blood_donation_process_section_title')); ?></h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="blood-donation-process">
                        <ul class="donation-process-list">
                            <?php $__currentLoopData = $all_donate_process; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="single-donation-process">
                                <span class="number"><?php echo e($data->number); ?></span>
                                <h4 class="title"><?php echo e($data->title); ?></h4>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_available_donor_section_status'))): ?>
    <!-- our valuable donor area start  -->
    <section class="our-valuable-donor-area padding-bottom-90">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title"><?php echo e(get_static_option('valuable_donors_section_title')); ?></h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">A+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','a+')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">A-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','a-')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">O+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','o+')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">O-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','o-')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">B+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','b+')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">B-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','b-')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">AB+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','ab+')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">AB-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="<?php echo e(route('frontend.donors.by.blood.group','ab-')); ?>" class="boxed-btn"><?php echo e(get_static_option('valuable_donors_button_text')); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- our valuable donor area end  -->
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_request_for_blood_section_status'))): ?>
    <div class="recently-requested-area padding-bottom-90">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title"><?php echo e(get_static_option('home_recent_request_for_blood_section_title')); ?></h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="recent-requested-blood-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col"><?php echo e(__('Name')); ?></th>
                                <th scope="col"><?php echo e(__('Email')); ?></th>
                                <th scope="col"><?php echo e(__('Blood Group')); ?></th>
                                <th scope="col"><?php echo e(__('Number Of Units')); ?></th>
                                <th scope="col"><?php echo e(__('Illness')); ?></th>
                                <th scope="col"><?php echo e(__('Contact Number')); ?></th>
                                <th scope="col"><?php echo e(__('Hospital Address')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $recent_blood_request; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th scope="row"><?php echo e($data->name); ?></th>
                                <td><?php echo e($data->email); ?></td>
                                <td><?php echo e($data->blood_group); ?></td>
                                <td><?php echo e($data->number_of_units); ?></td>
                                <td><?php echo e($data->illness); ?></td>
                                <td><?php echo e($data->mobile); ?></td>
                                <td><?php echo e($data->hospital_address); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_recent_donor_section_status'))): ?>
    <!-- recently Donated memeber start -->
    <div class="recently-donated-donors padding-bottom-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title"><?php echo e(get_static_option('recent_donors_section_title')); ?></h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="recently-donated-donors-carousel owl-carousel">
                        <?php $__currentLoopData = $all_recent_donors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-donors-item">
                            <div class="thumb">
                                <?php if(file_exists('assets/uploads/donors/donor-pic-'.$data->donor->id.'.'.$data->donor->image)): ?>
                                    <img style="max-width: 300px;" src="<?php echo e(asset('assets/uploads/donors/donor-pic-'.$data->donor->id.'.'.$data->donor->image)); ?>" alt="<?php echo e(__($data->donor->name)); ?>">
                                <?php else: ?>
                                    <img style="max-width: 300px" src="<?php echo e(asset('assets/uploads/no-image.png')); ?>" alt="no image available">
                                <?php endif; ?>
                            </div>
                            <div class="content">
                                <a href="<?php echo e(route('frontend.donors.profile',['id' => $data->donor->id,'slug' => Str::slug($data->donor->user->name)])); ?>"><h4 class="title"><?php echo e($data->donor->user->name); ?></h4></a>
                                <span class="blood-group"><?php echo e(__('Blood Group:')); ?> <strong><?php echo e(strtoupper($data->donor->blood_group)); ?></strong></span>
                                <span class="total-donate"><?php echo e(__('Total Donate:')); ?> <strong><?php echo e(get_total_donate($data->donor->id)); ?></strong> <?php echo e(__('Times')); ?></span>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- recently Donated memeber end -->
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_health_benefit_section_status'))): ?>
    <!-- benifits-of-donation area start  -->
    <section class="benefit-of-donation-area  gray-bg padding-top-120 padding-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="single-benefit-item">
                        <div class="left-content-area">
                            <?php echo get_static_option('health_benefit_first_item_content'); ?>

                        </div>
                        <div class="right-content-area">
                            <div class="img-wrapper">
                                <?php if(file_exists('assets/uploads/'.get_static_option('health_benefit_first_item_image'))): ?>
                                    <img src="<?php echo e(asset('assets/uploads/'.get_static_option('health_benefit_first_item_image'))); ?>" alt="">
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="single-benefit-item">

                        <div class="left-content-area">
                            <?php echo get_static_option('health_benefit_second_item_content'); ?>

                        </div>
                        <div class="right-content-area">
                            <div class="img-wrapper">
                                <?php if(file_exists('assets/uploads/'.get_static_option('health_benefit_second_item_image'))): ?>
                                    <img src="<?php echo e(asset('assets/uploads/'.get_static_option('health_benefit_second_item_image'))); ?>" alt="">
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- benifits-of-donation area end -->
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_our_volunteer_section_status'))): ?>
<?php echo $__env->make('frontend.partials.volunteers-area', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
    <?php if(!empty(get_static_option('home_counterup_section_status'))): ?>
<?php echo $__env->make('frontend.partials.counterup', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
    <?php if(!empty(get_static_option('home_testimonial_section_status'))): ?>
<?php echo $__env->make('frontend.partials.testimonial', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>
    <?php if(!empty(get_static_option('home_call_to_action_section_status'))): ?>
    <?php $cta_bg_image_url = ''; ?>
    <?php if(file_exists('assets/uploads/'.get_static_option('home_cta_bg'))): ?>
        <?php  $cta_bg_image_url = asset('assets/uploads/'.get_static_option('home_cta_bg')); ?>
    <?php endif; ?>
    <section class="call-to-action-area padding-100" style="background-image: url(<?php echo e($cta_bg_image_url); ?>);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-inner">
                        <div class="left-content-area">
                            <h2 class="title"><?php echo e(get_static_option('home_cta_title')); ?></h2>
                        </div>
                        <div class="right-content-area">
                            <div class="btn-wrapper">
                                <a href="<?php echo e(get_static_option('home_cta_btn_url')); ?>" class="boxed-btn"><?php echo e(get_static_option('home_cta_text')); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <?php if(!empty(get_static_option('home_news_update_section_status'))): ?>
    <section class="news-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title"><?php echo e(get_static_option('news_update_section_title')); ?></h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php $__currentLoopData = $all_blog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-4 col-md-6">
                    <div class="single-new-item">
                        <div class="thumb">
                            <?php if(file_exists('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)): ?>
                                <img src="<?php echo e(asset('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)); ?>" alt="<?php echo e($data->title); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="content">
                            <span class="posted_by"><?php echo e(__('Posted By ')); ?> <?php echo e($data->user->name); ?></span>
                            <h4 class="title"><a href="<?php echo e(route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])); ?>"><?php echo e($data->title); ?></a></h4>
                            <div class="description">
                                <?php echo Str::words($data->content,55); ?>

                            </div>
                            <a href="<?php echo e(route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])); ?>" class="readmore"><?php echo e(__('Read More')); ?></a>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.frontend-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/frontend/frontend-home.blade.php ENDPATH**/ ?>