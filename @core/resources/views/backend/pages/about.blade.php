@extends('backend.admin-master')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/backend/css/summernote-bs4.css')}}">
@endsection
@section('site-title')
    {{__("About Page")}}
@endsection
@section('content')
    <div class="col-lg-12 col-ml-12 padding-bottom-30">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-top-40"></div>
                @include('backend/partials/message')
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{__('About Page Settings')}}</h4>
                        <form action="{{route('admin.about.page')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="action" value="about_us_page_settings_form">
                            <div class="form-group">
                                <label for="about_page_title">{{__('Page Title')}}</label>
                                <input type="text" class="form-control"  id="about_page_title" value="{{get_static_option('about_page_title')}}" name="about_page_title" placeholder="{{__('Page Title')}}">
                            </div>
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update About Page Settings')}}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">{{__('About Us Content')}}</h4>
                            <form action="{{route('admin.about.page')}}" class="about_content_form" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="action" value="about_us_content_form">
                                <div class="form-group">
                                    <label for="about_us_content">{{__('Content Area')}}</label>
                                    <input type="hidden" name="about_us_content" id="about_us_content">
                                    <div class="summernote" data-content='{{get_static_option("about_us_content")}}'></div>
                                </div>
                                <div class="img-wrapper margin-bottom-20">
                                    @if(file_exists('assets/uploads/'.get_static_option('about_us_image')))
                                        <img style="max-width: 200px" src="{{asset('assets/uploads/'.get_static_option('about_us_image'))}}" alt="">
                                    @else
                                        <img style="max-width: 200px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="about_us_image">{{__('Image Area')}}</label>
                                    <input type="file" class="form-control" name="about_us_image">
                                    <small class="text-dager">{{__('Supported format jpg,jpeg,png. max image size : 2MB')}}</small>
                                </div>
                                <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update About Content Settings')}}</button>
                            </form>
                        </div>
                    </div>
            </div>
            <div class="col-lg-6 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">{{__('Why Donate Area')}}</h4>
                            <form action="{{route('admin.about.page')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="action" value="why_done_form">
                                <div class="form-group">
                                    <label for="why_us_title">{{__('Section Title')}}</label>
                                    <input type="text" class="form-control"  id="why_us_title" value="{{get_static_option('why_us_title')}}" name="why_us_title" placeholder="{{__('Section Title')}}">
                                </div>
                                <div class="form-group">
                                    <label for="why_donate_content">{{__('Content Area')}}</label>
                                    <input type="hidden" name="why_donate_content" id="why_donate_content" >
                                    <div class="summernote" data-content='{{get_static_option("why_donate_content")}}'></div>
                                </div>
                                <div class="img-wrapper margin-bottom-20">
                                    @if(file_exists('assets/uploads/'.get_static_option('why_donate_image')))
                                        <img style="max-width: 200px" src="{{asset('assets/uploads/'.get_static_option('why_donate_image'))}}" alt="">
                                    @else
                                        <img style="max-width: 200px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="why_donate_image">{{__('Image Area')}}</label>
                                    <input type="file" class="form-control" name="why_donate_image">
                                </div>
                                <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">{{__('Update Why donate Content')}}</button>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('assets/backend/js/summernote-bs4.js')}}"></script>
    <script>

        $(document).ready(function(){
            $('.summernote').summernote({
                height: 150,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                },
                callbacks: {
                    onChange: function(contents, $editable) {
                        $(this).prev('input').val(contents);
                    }
                }
            });
            if($('.summernote').length > 1){
                $('.summernote').each(function(index,value){
                    $(this).summernote('code', $(this).data('content'));
                });
            }
        })

    </script>
@endsection
