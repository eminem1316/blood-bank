@extends('frontend.frontend-page-master')
@section('page-title')
    {{__('Register')}}
@endsection
@section('content')
    <div class="page-content contact-page-content-area padding-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="righti-content-area">
                        <div class="contact-page-form-wrap">
                            @include('backend.partials.message')
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('register')}}" method="post" id="contact_page_form" class="contact-page-form" novalidate="novalidate" enctype="multipart/form-data">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" name="name" placeholder="{{__('Your Name')}}" value="{{(old('name'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="username" placeholder="{{__('Your Username')}}" value="{{(old('username'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="{{__('Your Email')}}" value="{{(old('email'))}}" class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="{{__('Your Password')}}"  class="form-control" required="" aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" placeholder="{{__('Confirm Password')}}"  class="form-control" required="" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="{{__('Register')}}" class="submit-btn register-as-donor">
                                </div>
                            </form></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
