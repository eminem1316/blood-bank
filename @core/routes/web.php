<?php
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache',function (){
   Artisan::call('route:clear');
   Artisan::call('view:clear');
   Artisan::call('config:clear');
   Artisan::call('cache:clear');
});

Route::group(['middleware' => 'setlang'],function (){
    //frontend routes
    Route::get('/','FrontendController@index')->name('homepage');
    Route::get('/join-donor','FrontendController@join_donor')->name('frontend.join.donor');
    Route::post('/join-donor','FrontendController@store_new_donor');
    Route::post('/contact','FrontendController@contact_mail');
    Route::post('/donor-contact','FrontendController@send_mail_to_user')->name('frontend.donor.contact');
    Route::get('/contact','FrontendController@contact_index')->name('frontend.contact');
    Route::get('/donors','FrontendController@donors_index')->name('frontend.donors');
    Route::get('/donors/search','FrontendController@donor_search')->name('frontend.search.donors');
    Route::get('/volunteer','FrontendController@volunteer_index')->name('frontend.volunteer');
    Route::get('/about','FrontendController@about_index')->name('frontend.about');
    Route::get('/donors/{id}','FrontendController@donors_by_blood_group')->name('frontend.donors.by.blood.group');
    Route::get('/donor-profile/{id}/{slug}','FrontendController@donors_profile')->name('frontend.donors.profile');
    Route::get('/blog','FrontendController@blog_page')->name('frontend.blog');
    Route::get('/blog/{id}/{any}','FrontendController@blog_single_page')->name('frontend.blog.single');
    Route::get('/blog/search/','FrontendController@blog_search_page')->name('frontend.blog.search');
    Route::get('/blog/category/{id}/{any}','FrontendController@category_wise_blog_page')->name('frontend.blog.category');
    Route::get('/blood-request','FrontendController@blood_request')->name('frontend.blood.request');
    Route::post('/blood-request','FrontendController@add_blood_request');
});

//language change
Route::get('/lang','FrontendController@lang_change')->name('frontend.langchange');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::post('register-as-donor', 'FrontendController@join_as_donor')->name('frontend.register.donor');

//user email verify
Route::get('/email-verify','FrontendController@email_verify_form')->name('user.email.verify');
Route::post('/email-verify','FrontendController@email_verify_mail_send');
Route::get('/email-verify-token/{token}/{username}','FrontendController@email_verify_token_form')->name('email.verify.token');

//admin login
Route::get('/login/admin','Auth\LoginController@showAdminLoginForm')->name('admin.login');
Route::get('/login/admin/forget-password','FrontendController@showAdminForgetPasswordForm')->name('admin.forget.password');
Route::get('/login/admin/reset-password/{user}/{token}','FrontendController@showAdminResetPasswordForm')->name('admin.reset.password');
Route::post('/login/admin/reset-password','FrontendController@AdminResetPassword')->name('admin.reset.password.change');
Route::post('/login/admin/forget-password','FrontendController@sendAdminForgetPasswordMail');
Route::post('/logout/admin','AdminDashboardController@adminLogout')->name('admin.logout');
Route::post('/login/admin','Auth\LoginController@adminLogin');

//user dashboard
Route::group(['middleware' => ['auth','check_user','check_email_verify']],function (){ //have to add another middleware for check is normal user also check is email verified
    Route::get('/user-dashboard','UserDashboardController@index')->name('user.dashboard');
    Route::get('/user-edit-profile','UserDashboardController@edit_profile')->name('user.edit.profile');
    Route::post('/user-edit-profile','UserDashboardController@update_profile');
    Route::get('/user-change-password','UserDashboardController@change_password')->name('user.change.password');
    Route::post('/user-change-password','UserDashboardController@update_password');
    Route::get('user-join-donor','UserDashboardController@join_as_donor_index')->name('user.join.donor');
    Route::post('/user-join-donor','UserDashboardController@join_as_donor');
});
//donor dashboard
Route::group(['middleware' => ['auth','check_donor','check_email_verify']],function (){ //have to add another middleware for check is donor also check is email verified
    Route::get('/donor-dashboard','DonorDashboardController@index')->name('donor.dashboard');
    Route::get('/donor-edit-donor-info','DonorDashboardController@edit_donor_info')->name('donor.edit.donor.info');
    Route::post('/donor-edit-donor-info','DonorDashboardController@update_donor_info');
    Route::get('/donor-edit-profile','DonorDashboardController@edit_profile')->name('donor.edit.profile');
    Route::post('/donor-edit-profile','DonorDashboardController@update_profile');
});

//user forget password
Route::get('/user-forget-password','FrontendController@user_forget_password')->name('user.forget.password');
Route::post('/user-forget-password','FrontendController@user_send_password_reset_mail');
Route::get('/user-reset-password/{user}/{token}','FrontendController@user_reset_password_form')->name('user.password.reset.mail');
Route::post('/user-reset-password/','FrontendController@user_reset_password')->name('user.password.reset');

//admin dashboard routes
Route::prefix('admin-home')->group(function (){


    Route::get('/', 'AdminDashboardController@adminIndex')->name('admin.home');
    //page settings
    Route::get('/contact','AdminDashboardController@contact')->name('admin.contact');
    Route::post('/contact','AdminDashboardController@update_contact');
    Route::post('/new-contact-info','ContactInfoController@new_contact_info')->name('admin.new.contact.info');
    Route::post('/update-contact-info','ContactInfoController@update_contact_info')->name('admin.update.contact.info');
    Route::post('/delete-contact-info/{id}','ContactInfoController@delete_contact_info')->name('admin.delete.contact.info');
    Route::get('/donors-page','AdminDashboardController@donor_page')->name('admin.donor.page');
    Route::post('/donors-page','AdminDashboardController@donor_page_update');
    Route::get('/volunteer-page','AdminDashboardController@volunteer_page')->name('admin.volunteer.page');
    Route::post('/volunteer-page','AdminDashboardController@volunteer_page_update');
    Route::get('/about-page','AdminDashboardController@about_page')->name('admin.about.page');
    Route::post('/about-page','AdminDashboardController@about_page_update');
    Route::get('/blog-page','AdminDashboardController@blog_page')->name('admin.blog.page');
    Route::post('/blog-page','AdminDashboardController@blog_page_update');
    Route::get('/counterup','CounterUpController@index')->name('admin.counterup');
    Route::post('/counterup','CounterUpController@store');
    Route::post('/update-counterup','CounterUpController@update')->name('admin.counterup.update');
    Route::post('/delete-counterup/{id}','CounterUpController@delete')->name('admin.counterup.delete');
    Route::get('/testimonial','TestimonialController@index')->name('admin.testimonial');
    Route::post('/testimonial','TestimonialController@store');
    Route::post('/update-testimonial','TestimonialController@update')->name('admin.testimonial.update');
    Route::post('/delete-testimonial/{id}','TestimonialController@delete')->name('admin.testimonial.delete');
    //request for blood
    Route::get('/request-for-blood','RequestForBloodController@index')->name('admin.blood.request');
    Route::post('/request-for-blood/{id}','RequestForBloodController@delete')->name('admin.blood.request.delete');
    //homepage
    Route::get('/home-page/header','HomePageController@header_area')->name('admin.home.header');
    Route::post('/home-page/header','HomePageController@header_area_update');
    Route::get('/home-page/blood-donation','HomePageController@blood_donation')->name('admin.blood.donation');
    Route::post('/home-page/blood-donation','HomePageController@blood_donation_update');
    Route::post('/home-page/new-blood-donation-item','HomePageController@new_blood_donation')->name('admin.blood.donation.new');
    Route::post('/home-page/update-blood-donation-item','HomePageController@update_blood_donation')->name('admin.blood.donation.update');
    Route::post('/home-page/delete-blood-donation-item/{id}','HomePageController@delete_blood_donation')->name('admin.blood.donation.delete');
    Route::get('/home-page/our-valuable-donor','HomePageController@our_valuable_donor')->name('admin.valuable.donor');
    Route::post('/home-page/our-valuable-donor','HomePageController@our_valuable_donor_update');
    Route::get('/home-page/recent-donors','HomePageController@recent_donors')->name('admin.recent.donors');
    Route::post('/home-page/recent-donors','HomePageController@recent_donors_update');
    Route::get('/home-page/health-benefit','HomePageController@health_benefit')->name('admin.health.benefit');
    Route::post('/home-page/health-benefit','HomePageController@update_health_benefit');
    Route::get('/home-page/cta','HomePageController@call_action')->name('admin.cta');
    Route::post('/home-page/cta','HomePageController@update_call_action');
    Route::get('/home-page/news','HomePageController@news_area')->name('admin.home.news');
    Route::post('/home-page/news','HomePageController@update_news_area');

    Route::get('/home-page/recent-request-for-blood','HomePageController@recent_blood_request')->name('admin.recent.request.section');
    Route::post('/home-page/recent-request-for-blood','HomePageController@update_recent_blood_request');

    //section manage
    Route::get('/home-section-manage','HomePageController@home_section_manage')->name('admin.home.section.manage');
    Route::post('/home-section-manage','HomePageController@update_home_section_manage');
    Route::get('/about-section-manage','HomePageController@about_section_manage')->name('admin.about.section.manage');
    Route::post('/about-section-manage','HomePageController@update_about_section_manage');

    //footer
    Route::get('/footer/about','FooterController@about_widget')->name('admin.footer.about');
    Route::post('/footer/about','FooterController@update_about_widget');
    Route::get('/footer/useful-links','FooterController@useful_links_widget')->name('admin.footer.useful.link');
    Route::post('/footer/useful-links/widget','FooterController@update_widget_useful_links')->name('admin.footer.useful.link.widget');
    Route::post('/footer/useful-links','FooterController@new_useful_links_widget');
    Route::post('/footer/useful-links/update','FooterController@update_useful_links_widget')->name('admin.footer.useful.link.update');
    Route::post('/footer/useful-links/update/{delete}','FooterController@delete_useful_links_widget')->name('admin.footer.useful.link.delete');
    Route::get('/footer/recent-post','FooterController@recent_post_widget')->name('admin.footer.recent.post');
    Route::post('/footer/recent-post','FooterController@update_recent_post_widget');

    Route::get('/footer/important-links','FooterController@important_links_widget')->name('admin.footer.important.link');
    Route::post('/footer/important-links/widget','FooterController@update_widget_important_links')->name('admin.footer.important.link.widget');
    Route::post('/footer/important-links','FooterController@new_important_links_widget');
    Route::post('/footer/important-links/update','FooterController@update_important_links_widget')->name('admin.footer.important.link.update');
    Route::post('/footer/important-links/update/{delete}','FooterController@delete_important_links_widget')->name('admin.footer.important.link.delete');
    //topbar area
    Route::get('/topbar','TopBarController@index')->name('admin.topbar');
    Route::post('/topbar/new-support-info','TopBarController@new_support_info')->name('admin.new.support.info');
    Route::post('/topbar/update-support-info','TopBarController@update_support_info')->name('admin.update.support.info');
    Route::post('/topbar/delete-support-info/{id}','TopBarController@delete_support_info')->name('admin.delete.support.info');
    Route::post('/topbar/new-social-item','TopBarController@new_social_item')->name('admin.new.social.item');
    Route::post('/topbar/update-social-item','TopBarController@update_social_item')->name('admin.update.social.item');
    Route::post('/topbar/delete-social-item/{id}','TopBarController@delete_social_item')->name('admin.delete.social.item');

    //blog
    Route::get('/blog','BlogController@index')->name('admin.blog');
    Route::get('/new-blog','BlogController@new_blog')->name('admin.blog.new');
    Route::post('/new-blog','BlogController@store_new_blog');
    Route::get('/blog-edit/{id}','BlogController@edit_blog')->name('admin.blog.edit');
    Route::post('/blog-update/{id}','BlogController@update_blog')->name('admin.blog.update');
    Route::post('/blog-delete/{id}','BlogController@delete_blog')->name('admin.blog.delete');
    Route::get('/blog-category','BlogController@category')->name('admin.blog.category');
    Route::post('/blog-category','BlogController@new_category');
    Route::post('/delete-blog-category/{id}','BlogController@delete_category')->name('admin.blog.category.delete');
    Route::post('/update-blog-category','BlogController@update_category')->name('admin.blog.category.update');

    //user role management
    Route::get('/new-user','UserRoleManageController@new_user')->name('admin.new.user');
    Route::post('/new-user','UserRoleManageController@new_user_add');
    Route::post('/user-update','UserRoleManageController@user_update')->name('admin.user.update');
    Route::post('/user-password-chnage','UserRoleManageController@user_password_change')->name('admin.user.password.change');
    Route::post('/delete-user/{id}','UserRoleManageController@new_user_delete')->name('admin.delete.user');
    Route::get('/all-user','UserRoleManageController@all_user')->name('admin.all.user');

    //admin settings
    Route::get('/settings','AdminDashboardController@admin_settings')->name('admin.profile.settings');
    Route::get('/profile-update','AdminDashboardController@admin_profile')->name('admin.profile.update');
    Route::post('/profile-update','AdminDashboardController@admin_profile_update');
    Route::get('/password-change','AdminDashboardController@admin_password')->name('admin.password.change');
    Route::post('/password-change','AdminDashboardController@admin_password_chagne');

    //donor
    Route::get('/new-donor','DonorsController@new_donor')->name('admin.new.donor');
    Route::post('/new-donor','DonorsController@store_donor');
    Route::get('/import-donor','DonorsController@import_donor_index')->name('admin.import.donor');
    Route::post('/import-donor','DonorsController@import_donor');
    Route::post('/new-donor/edit','DonorsController@update_donor')->name('admin.donor.update');
    Route::post('/new-donor/delete/{id}','DonorsController@delete_donor')->name('admin.donor.delete');
    Route::post('/donor-approve','DonorsController@donor_approve')->name('admin.donor.approve');
    Route::post('/donor-password-change','DonorsController@donor_password_change')->name('admin.donor.password.change');
    Route::get('/all-donor','DonorsController@all_donors')->name('admin.all.donor');
    Route::post('/search-donor','DonorsController@search_donor')->name('admin.search.donor');
    Route::post('/search-donor-details','DonorsController@search_donor_details')->name('admin.search.donor.details');

    //volunteer
    Route::get('/new-volunteer','VolunteerController@new_volunteer')->name('admin.new.volunteer');
    Route::post('/new-volunteer','VolunteerController@store_volunteer');
    Route::post('/new-volunteer/edit','VolunteerController@update_volunteer')->name('admin.volunteer.update');
    Route::post('/new-volunteer/delete/{id}','VolunteerController@delete_volunteer')->name('admin.volunteer.delete');
    Route::post('/volunteer-approve','VolunteerController@volunteer_approve')->name('admin.volunteer.approve');
    Route::get('/all-volunteer','VolunteerController@all_volunteer')->name('admin.all.volunteer');

    //blood logs
    Route::get('/all-blood-log','BloodLogsController@all_blood_logs')->name('admin.blood.index');
    Route::get('/new-blood-log','BloodLogsController@new_blood_log')->name('admin.new.blood.logs');
    Route::post('/new-blood-log','BloodLogsController@store_new_blood_log');
    Route::post('/blood-log/update','BloodLogsController@update_blood_log')->name('admin.blood.log.update');
    Route::post('/blood-log/delete/{id}','BloodLogsController@delete_blood_log')->name('admin.blood.log.delete');

    //donor area
    Route::get('/donor-area','AreaController@index')->name('admin.donor.area');
    Route::post('/donor-area','AreaController@store');
    Route::post('/donor-area/edit/{id}','AreaController@update')->name('admin.donor.area.update');
    Route::post('/donor-area/delete/{id}','AreaController@destroy')->name('admin.donor.area.destroy');

    //general settings
    Route::get('/general-settings/site-identity','AdminDashboardController@site_identity')->name('admin.general.site.identity');
    Route::post('/general-settings/site-identity','AdminDashboardController@update_site_identity');
    Route::get('/general-settings/basic-settings','AdminDashboardController@basic_settings')->name('admin.general.basic.settings');
    Route::post('/general-settings/basic-settings','AdminDashboardController@update_basic_settings');
    Route::get('/general-settings/seo-settings','AdminDashboardController@seo_settings')->name('admin.general.seo.settings');
    Route::post('/general-settings/seo-settings','AdminDashboardController@update_seo_settings');
    Route::get('/general-settings/scripts','AdminDashboardController@scripts_settings')->name('admin.general.scripts.settings');
    Route::post('/general-settings/scripts','AdminDashboardController@update_scripts_settings');
    Route::get('/general-settings/email-template','AdminDashboardController@email_template_settings')->name('admin.general.email.template');
    Route::post('/general-settings/email-template','AdminDashboardController@update_email_template_settings');

    //new settings
    Route::get('/general-settings/cache-settings','AdminDashboardController@cache_settings')->name('admin.general.cache.settings');
    Route::post('/general-settings/cache-settings','AdminDashboardController@update_cache_settings');

    Route::get('/general-settings/custom-css','AdminDashboardController@custom_css_settings')->name('admin.general.custom.css');
    Route::post('/general-settings/custom-css','AdminDashboardController@update_custom_css_settings');

    //language
    Route::get('/languages','LanguageController@index')->name('admin.languages');
    Route::get('/languages/words/edit/{id}','LanguageController@edit_words')->name('admin.languages.words.edit');
    Route::post('/languages/words/update/{id}','LanguageController@update_words')->name('admin.languages.words.update');
    Route::post('/languages/new','LanguageController@store')->name('admin.languages.new');
    Route::post('/languages/update','LanguageController@update')->name('admin.languages.update');
    Route::post('/languages/delete/{id}','LanguageController@delete')->name('admin.languages.delete');
    Route::post('/languages/default/{id}','LanguageController@make_default')->name('admin.languages.default');
});
