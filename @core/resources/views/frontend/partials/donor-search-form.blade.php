<div class="header-bottom-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="blood-search-warpper">
                    @if($errors->any())
                        <ul class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    <form action="{{route('frontend.search.donors')}}" class="blood-search-form" method="get">
                        <ul class="fields-list">
                            <li>
                                <div class="form-group">
                                    <select class="form-control nice-select wide" name="area">
                                        <option value="">{{__('Areas')}}</option>
                                        @foreach($all_areas as $data)
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="form-group ">
                                    <select class="form-control nice-select wide" name="blood_group">
                                        <option value="">{{__('Blood Group')}}</option>
                                        <option value="o+">O+</option>
                                        <option value="o-">O-</option>
                                        <option value="b+">B+</option>
                                        <option value="b-">B-</option>
                                        <option value="a+">A+</option>
                                        <option value="a-">A-</option>
                                        <option value="ab+">AB+</option>
                                        <option value="ab-">AB-</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <input type="submit" value="{{__('Search Donor')}}" class="submit-btn">
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
