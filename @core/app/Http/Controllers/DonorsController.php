<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Area;
use App\BloodLog;
use App\Donor;
use App\Imports\DonorsImport;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;


class DonorsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }

    public function all_donors(){

        $all_donor = Donor::all();
        $all_donor_area = Area::all();

        return view('backend.donor-management.all-donors')->with(
            [
                'all_donor' => $all_donor,
                'all_donor_area' => $all_donor_area
            ]
        );
    }

    public function new_donor(){
        $all_donor_area = Area::all();
        return view('backend.donor-management.add-new-donor')->with(
            [
            'all_donor_area' => $all_donor_area
            ]);
    }
    public function store_donor(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users',
            'password' => 'required|string|min:8',
            'username' => 'required|string|max:191|unique:users',
            'father_name' => 'string|max:191',
            'mother_name' => 'string|max:191',
            'present_address' => 'string',
            'permanent_address' => 'string',
            'mobile' => 'required|string|max:191|unique:donors',
            'birthday' => 'string',
            'blood_group' => 'required|string',
            'areas_id' => 'required|string',
            'image' => 'mimes:jpg,jpeg,png|max:2060',
        ]);
        $user_id = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ])->id;
        $donor_id = Donor::create([
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'mobile' => $request->mobile,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->areas_id,
            'status' => 1,
            'user_id' => $user_id,
        ])->id;

      if ($request->hasFile('image')){
          $image = $request->image;
          $image_extenstion = $request->image->getClientOriginalExtension();
          $image_name = 'donor-pic-'.$donor_id.'.'.$image_extenstion;
          if (check_image_extension($request->image)){
              $image->move('assets/uploads/donors',$image_name);
              Donor::where('id',$donor_id)->update(['image'=>$image_extenstion]);
          }
      }

        return redirect()->back()->with(['msg' => 'New Donor Added Success','type' => 'success']);
    }

    public function donor_approve(Request $request){
        Donor::find($request->donor_id)->update(['status'=>1]);
        return redirect()->back()->with(['msg' => 'Donor Approved','type' => 'success']);
    }

    public function update_donor(Request $request){
        //working on this funciton
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191',
            'father_name' => 'required|string|max:191',
            'mother_name' => 'required|string|max:191',
            'present_address' => 'string',
            'permanent_address' => 'string',
            'mobile' => 'required|string|max:191',
            'birthday' => 'string',
            'blood_group' => 'required|string',
            'area' => 'required|string',
            'image' => 'mimes:jpg,jpeg,png|max:2060',
        ]);
        $donor_info = Donor::find($request->donor_id);
        $donor_info->update([
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'mobile' => $request->mobile,
            'birthday' => $request->birthday,
            'blood_group' => $request->blood_group,
            'area_id' => $request->area,
        ]);

        User::find($donor_info->user_id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'donor-pic-'.$request->donor_id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/donors',$image_name);
                Donor::where('id',$request->donor_id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'Donor Update Success','type' => 'success']);
    }

    public function delete_donor(Request $request, $id){
        if (BloodLog::where('donors_id',$id)->first()){
            return redirect()->back()->with(['msg' => 'This Donor Is Associated With A Blood Log, You Can Not Delete This Donor.','type' => 'danger']);
        }
        Donor::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Donor Delete Success','type' => 'danger']);
    }

    public function search_donor(Request $request){
       $all_donor =  User::where('username','LIKE','%'.$request->q.'%')->get();
       $all_name = array();
       foreach ($all_donor as $donor){
           array_push($all_name,$donor->username);
       }
        return response()->json($all_name);
    }

    public function search_donor_details(Request $request){
        $user_details = User::where('username',$request->q)->first();
        $donor_details = Donor::where('user_id',$user_details->id)->first();
        $value['name'] = $user_details->name;
        $value['id'] = $donor_details->id;
        $value['blood_donate_count'] = $donor_details->blood_donate_count;
        $value['blood_group'] = $donor_details->blood_group;
        return response()->json($value);
    }

    public function donor_password_change(Request $request){

        $this->validate($request, [
            'password' => 'required|string|min:8|confirmed'
        ]);
        $donor_info = Donor::find($request->ch_donor_id);
        $user = User::findOrFail($donor_info->user_id);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with(['msg'=>'Password Change Success..','type'=> 'success']);
    }

    public function import_donor(Request $request){
        $this->validate($request,[
           'import_excel' => 'required|max:15000'
        ]);

        if ($request->hasFile('import_excel')){

            $file_extenstion = $request->import_excel->getClientOriginalExtension();
            if ($file_extenstion == 'xlsx'){
                $request->file('import_excel')->move('assets/uploads/','import_donor_from_excel_file.xlsx');
                $reader = IOFactory::createReader('Xlsx');

                $spreadsheet = $reader->load('assets/uploads/import_donor_from_excel_file.xlsx');
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

           foreach ($sheetData as $row){
            if (empty($row['A'])){
                continue;
            }
            $get_user_info = User::where('email',$row['B'])->first();
            if (empty($get_user_info)){
                $id = User::create([
                    'password' => bcrypt(12345678),
                    'username' => Str::slug($row['A']),
                    'email' => $row['B'],
                    'name' => $row['A'],
                ])->id;
                Donor::create([
                    'father_name' => $row['D'],
                    'mother_name' => $row['C'],
                    'present_address' => $row['E'],
                    'permanent_address' => $row['F'],
                    'mobile' => $row['G'],
                    'birthday' => $row['H'],
                    'blood_group' => $row['I'],
                    'area_id' => $row['J'],
                    'user_id' => $id,
                    'status' => 1
                ]);
            }

        }
           return redirect()->back()->with(['msg' => 'Donor Import Success..','type' => 'success']);

            }else{
                return redirect()->back()->with(['msg' => 'Upload A Valid Excel File, You Can Check Example Excel File','type' => 'danger']);
            }
        }
        return redirect()->back()->with(['msg' => 'Donor Import Success..','type' => 'success']);
    }

    public function import_donor_index(){
        return view('backend.donor-management.import-donor');
    }

}//end class
