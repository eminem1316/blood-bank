<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $table = 'volunteers';
    protected $fillable = [
        'name',
        'email',
        'image',
        'mobile',
        'designation',
        'blood_group',
        'facebook',
        'twitter',
        'linkedin',
        'instagram',
    ];

}
