<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestForBlood extends Model
{
    protected $table = 'request_for_bloods';
    protected $fillable = ['user_id','name','status','email','mobile','blood_group','state','city','number_of_units','illness','hospital_address','message'];
}
