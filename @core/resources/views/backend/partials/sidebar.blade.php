<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="{{route('admin.home')}}">
                @if(file_exists('assets/uploads/'.get_static_option('site_logo')))
                    <img src="{{asset('assets/uploads/'.get_static_option('site_logo'))}}" alt="{{get_static_option('site_title')}}">
                @endif
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li class="{{active_menu('admin-home')}}">
                        <a href="{{route('admin.home')}}"
                           aria-expanded="true">
                            <i class="ti-dashboard"></i>
                            <span>@lang('dashboard')</span>
                        </a>
                    </li>
                    <li
                        class="
                            {{active_menu('admin-home/new-donor')}}
                            {{active_menu('admin-home/all-donor')}}
                            {{active_menu('admin-home/donor-area')}}
                            "
                        >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span>{{__('Donors Manage')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/all-donor')}}"><a href="{{route('admin.all.donor')}}">{{__('All Donors')}}</a></li>
                            <li class="{{active_menu('admin-home/donor-area')}}"><a href="{{route('admin.donor.area')}}">{{__('Donors Area')}}</a></li>
                            <li class="{{active_menu('admin-home/new-donor')}}"><a href="{{route('admin.new.donor')}}">{{__('Add New Donor')}}</a></li>
                            <li class="{{active_menu('admin-home/import-donor')}}"><a href="{{route('admin.import.donor')}}">{{__('Import Donor')}}</a></li>
                        </ul>
                    </li>
                    <li
                        class="
                        {{active_menu('admin-home/new-volunteer')}}
                        {{active_menu('admin-home/all-volunteer')}}
                         "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i><span>{{__('Volunteer Manage')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/all-volunteer')}}"><a href="{{route('admin.all.volunteer')}}">{{__('All Volunteer')}}</a></li>
                            <li class="{{active_menu('admin-home/new-volunteer')}}"><a href="{{route('admin.new.volunteer')}}">{{__('Add New Volunteer')}}</a></li>
                        </ul>
                    </li>
                    <li
                        class="
                            {{active_menu('admin-home/all-blood-log')}}
                            {{active_menu('admin-home/new-blood-log')}}
                            "
                        >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-loop"></i> <span>{{__('Blood Logs')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/all-blood-log')}}"><a href="{{__(route('admin.blood.index'))}}">{{__('All Logs')}}</a></li>
                            <li class="{{active_menu('admin-home/new-blood-log')}}"><a href="{{__(route('admin.new.blood.logs'))}}">{{__('Add New Log')}}</a></li>
                        </ul>
                    </li>
                    <li class="{{active_menu('admin-home/request-for-blood')}}"><a href="{{route('admin.blood.request')}}"><i class="ti-loop"></i> <span>{{__('All Blood Request')}}</span></a></li>
                    @if('super_admin' == auth()->user()->role)
                    <li
                        class="
                        {{active_menu('admin-home/new-user')}}
                        {{active_menu('admin-home/all-user')}}
                        "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-user"></i> <span>{{__('Admin Role Manage')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/all-user')}}"><a href="{{route('admin.all.user')}}">{{__('All Admin')}}</a></li>
                            <li class="{{active_menu('admin-home/new-user')}}"><a href="{{route('admin.new.user')}}">{{__('Add New Admin')}}</a></li>
                        </ul>
                    </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/topbar')}}">
                            <a href="{{route('admin.topbar')}}"
                               aria-expanded="true">
                                <i class="ti-dashboard"></i>
                                <span>{{__('Top Bar Settings')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="
                        {{active_menu('admin-home/home-page')}}
                        @if(request()->is('admin-home/home-page/*'))
                        {{active_menu('admin-home/home-page/header')}}
                        active
                        @endif
                            ">
                            <a href="javascript:void(0)"
                               aria-expanded="true">
                                <i class="ti-home"></i>
                                <span>{{__('Home Page')}}</span>
                            </a>
                            <ul class="collapse">
                                <li class="{{active_menu('admin-home/home-page/header')}}"><a href="{{route('admin.home.header')}}">{{__('Header Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/blood-donation')}}"><a href="{{route('admin.blood.donation')}}">{{__('Blood Donation Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/our-valuable-donor')}}"><a href="{{route('admin.valuable.donor')}}">{{__('Available Donor Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/recent-request-for-blood')}}"><a href="{{route('admin.recent.request.section')}}">{{__('Recent Request For Blood Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/recent-donors')}}"><a href="{{route('admin.recent.donors')}}">{{__('Recent Donor Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/health-benefit')}}"><a href="{{route('admin.health.benefit')}}">{{__('Health Benefits Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/cta')}}"><a href="{{route('admin.cta')}}">{{__('Call To Action Area')}}</a></li>
                                <li class="{{active_menu('admin-home/home-page/news')}}"><a href="{{route('admin.home.news')}}">{{__('News Update Area')}}</a></li>
                            </ul>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/home-section-manage')}}">
                            <a href="{{route('admin.home.section.manage')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('Home Sections Manage')}}</span>
                            </a>
                        </li>
                        <li class="{{active_menu('admin-home/about-section-manage')}}">
                            <a href="{{route('admin.about.section.manage')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('About Sections Manage')}}</span>
                            </a>
                        </li>
                        <li class="{{active_menu('admin-home/about-page')}}">
                            <a href="{{route('admin.about.page')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('About Page')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/volunteer-page')}}">
                            <a href="{{route('admin.volunteer.page')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('Volunteer Page')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/donors-page')}}">
                            <a href="{{route('admin.donor.page')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('Donors Page')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/blog-page')}}">
                            <a href="{{route('admin.blog.page')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('Blog Page')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="{{active_menu('admin-home/contact')}}">
                            <a href="{{route('admin.contact')}}"
                               aria-expanded="true">
                                <i class="ti-file"></i>
                                <span>{{__('Contact Page')}}</span>
                            </a>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                    <li
                        class="
                        {{active_menu('admin-home/blog')}}
                        {{active_menu('admin-home/blog-category')}}
                        {{active_menu('admin-home/new-blog')}}
                        @if(request()->is('admin-home/blog-edit/*')) active @endif
                        "
                    >
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-write"></i> <span>{{__('Blog')}}</span></a>
                        <ul class="collapse">
                            <li class="{{active_menu('admin-home/blog')}}"><a href="{{route('admin.blog')}}">{{__('All Blog')}}</a></li>
                            <li class="{{active_menu('admin-home/blog-category')}}"><a href="{{route('admin.blog.category')}}">{{__('Category')}}</a></li>
                            <li class="{{active_menu('admin-home/new-blog')}}"><a href="{{route('admin.blog.new')}}">{{__('Add New Post')}}</a></li>
                        </ul>
                    </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                    <li
                        class="
                        {{active_menu('admin-home/testimonial')}}
                        "
                    >
                        <a href="{{route('admin.testimonial')}}" aria-expanded="true"><i class="ti-control-forward"></i> <span>{{__('Testimonial')}}</span></a>
                    </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                    <li
                        class="  {{active_menu('admin-home/counterup')}} ">
                        <a href="{{route('admin.counterup')}}" aria-expanded="true"><i class="ti-exchange-vertical"></i> <span>{{__('Counterup')}}</span></a>
                    </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li class="@if(request()->is('admin-home/footer/*')) active @endif">
                            <a href="javascript:void(0)"
                               aria-expanded="true">
                                <i class="ti-layout"></i>
                                <span>{{__('Footer Area')}}</span>
                            </a>
                            <ul class="collapse">
                                <li class="{{active_menu('admin-home/footer/about')}}"><a href="{{route('admin.footer.about')}}">{{__('About Widget')}}</a></li>
                                <li class="{{active_menu('admin-home/footer/useful-links')}}"><a href="{{route('admin.footer.useful.link')}}">{{__('Useful Links Widget')}}</a></li>
                                <li class="{{active_menu('admin-home/footer/recent-post')}}"><a href="{{route('admin.footer.recent.post')}}">{{__('Recent Posts Widget')}}</a></li>
                                <li class="{{active_menu('admin-home/footer/important-links')}}"><a href="{{route('admin.footer.important.link')}}">{{__('Important Links Widget')}}</a></li>
                            </ul>
                        </li>
                    @endif
                    @if('super_admin' == auth()->user()->role || 'admin' == auth()->user()->role || 'volunteer' == auth()->user()->role)
                    <li class="@if(request()->is('admin-home/general-settings/*')) active @endif">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-settings"></i> <span>{{__('General Settings')}}</span></a>
                        <ul class="collapse ">
                            <li class="{{active_menu('admin-home/general-settings/site-identity')}}"><a href="{{route('admin.general.site.identity')}}">{{__('Site Identity')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/basic-settings')}}"><a href="{{route('admin.general.basic.settings')}}">{{__('Basic Settings')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/seo-settings')}}"><a href="{{route('admin.general.seo.settings')}}">{{__('SEO Settings')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/scripts')}}"><a href="{{route('admin.general.scripts.settings')}}">{{__('Third Party Scripts')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/email-template')}}"><a href="{{route('admin.general.email.template')}}">{{__('Email Template')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/cache-settings')}}"><a href="{{route('admin.general.cache.settings')}}">{{__('Cache Settings')}}</a></li>
                            <li class="{{active_menu('admin-home/general-settings/custom-css')}}"><a href="{{route('admin.general.custom.css')}}">{{__('Custom Css')}}</a></li>
                        </ul>
                    </li>
                    @endif
                    @if('super_admin' == auth()->user()->role)
                        <li
                            class="@if(request()->is('admin-home/languages/*') || request()->is('admin-home/languages') ) active @endif">
                            <a href="{{route('admin.languages')}}" aria-expanded="true"><i class="ti-signal"></i> <span>{{__('Languages')}}</span></a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
