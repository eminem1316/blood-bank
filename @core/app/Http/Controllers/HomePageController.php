<?php

namespace App\Http\Controllers;

use App\DonationProcess;
use App\Donor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class HomePageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function header_area()
    {
        return view('backend.pages.home.header');
    }

    public function header_area_update(Request $request)
    {
        $this->validate($request, [
            'home_page_header_title' => 'required|string|max:191',
            'home_page_header_description' => 'required|string',
            'home_page_header_btn_one_text' => 'required|string|max:191',
            'home_page_header_btn_one_url' => 'required|string|max:191',
            'home_page_header_btn_two_text' => 'required|string|max:191',
            'home_page_header_btn_two_url' => 'required|string|max:191',
            'home_page_header_right_image' => 'mimes:jpg,jpeg,png|max:2064',
        ]);
        if ($request->hasFile('home_page_header_right_image')) {
            $image = $request->home_page_header_right_image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'header-right-image-' .rand(999,99999).'.'. $image_extenstion;
            if (check_image_extension($image)) {
                $image->move('assets/uploads/', $image_name);
                update_static_option('home_page_header_right_image', $image_name);
            }
        }
        update_static_option('home_page_header_title', $request->home_page_header_title);
        update_static_option('home_page_header_description', $request->home_page_header_description);
        update_static_option('home_page_header_btn_one_text', $request->home_page_header_btn_one_text);
        update_static_option('home_page_header_btn_one_url', $request->home_page_header_btn_one_url);
        update_static_option('home_page_header_btn_two_text', $request->home_page_header_btn_two_text);
        update_static_option('home_page_header_btn_two_url', $request->home_page_header_btn_two_url);

        return redirect()->back()->with([
            'msg' => 'Header Settings Updated ...',
            'type' => 'success'
        ]);
    }

    public function blood_donation()
    {
        $all_process = DonationProcess::all();
        return view('backend.pages.home.blood-donation')->with([
            'all_process' => $all_process
        ]);
    }

    public function new_blood_donation(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:191',
            'number' => 'required|string|max:191',
        ]);
        DonationProcess::create($request->all());

        return redirect()->back()->with([
                'msg' => 'New Blood Process Item Added ...',
                'type' => 'success'
            ]);
    }
    public function update_blood_donation(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:191',
            'number' => 'required|string|max:191',
        ]);
        DonationProcess::find($request->id)->update([
            'number' => $request->number,
            'title' => $request->title
        ]);

        return redirect()->back()->with([
                'msg' => 'Blood Process Item updated ...',
                'type' => 'success'
            ]);
    }

    public function delete_blood_donation(Request $request, $id)
    {

        DonationProcess::find($id)->delete();

        return redirect()->back()->with([
            'msg' => 'Blood Donation Process Item Delete Success',
            'type' => 'danger'
        ]);
    }

    public function blood_donation_update(Request $request){
        $this->validate($request,[
           'blood_donation_process_section_title' => 'required|string|max:191'
        ]);
        update_static_option('blood_donation_process_section_title',$request->blood_donation_process_section_title);
        return redirect()->back()->with([
            'msg' => 'Donation Process Settings Update..',
            'type' => 'success'
        ]);
    }
    public function our_valuable_donor(){
        return view('backend.pages.home.valuable-donors');
    }
    public function our_valuable_donor_update(Request $request){
        $this->validate($request,[
            'valuable_donors_section_title' => 'required|string|max:191',
            'valuable_donors_button_text' => 'required|string|max:191',
        ]);
        update_static_option('valuable_donors_section_title',$request->valuable_donors_section_title);
        update_static_option('valuable_donors_button_text',$request->valuable_donors_button_text);
        return redirect()->back()->with([
            'msg' => 'Available Donor Settings Updated..',
            'type' => 'success'
        ]);
    }
    public function recent_donors(){
        return view('backend.pages.home.recent-donors');
    }

    public function recent_donors_update(Request $request){
        $this->validate($request,[
            'recent_donors_section_title' => 'required|string|max:191',
            'recent_donors_number' => 'required|string|max:191',
        ]);
        update_static_option('recent_donors_section_title',$request->recent_donors_section_title);
        update_static_option('recent_donors_number',$request->recent_donors_number);
        return redirect()->back()->with([
            'msg' => 'Recent Donor Settings Updated..',
            'type' => 'success'
        ]);
    }
    public function health_benefit(){
        return view('backend.pages.home.health-benefit');
    }
    public function update_health_benefit(Request $request){
        $this->validate($request, [
            'health_benefit_first_item_image' => 'mimes:jpg,jpeg,png|max:2064',
            'health_benefit_second_item_image' => 'mimes:jpg,jpeg,png|max:2064',
        ]);
        if ($request->hasFile('health_benefit_first_item_image')) {
            $image = $request->health_benefit_first_item_image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'health-benefit-one-' .rand(999,99999).'.'. $image_extenstion;
            if (check_image_extension($image)) {
                $image->move('assets/uploads/', $image_name);
                update_static_option('health_benefit_first_item_image', $image_name);
            }
        }
        if ($request->hasFile('health_benefit_second_item_image')) {
            $image = $request->health_benefit_second_item_image;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'health-benefit-two-' .rand(999,99999).'.'. $image_extenstion;
            if (check_image_extension($image)) {
                $image->move('assets/uploads/', $image_name);
                update_static_option('health_benefit_second_item_image', $image_name);
            }
        }
        update_static_option('health_benefit_first_item_content',$request->health_benefit_first_item_content);
        update_static_option('health_benefit_second_item_content',$request->health_benefit_second_item_content);

        return redirect()->back()->with([
            'msg' => 'Health Benefit Settings Updated..',
            'type' => 'success'
        ]);
    }
    public function call_action(){
        return view('backend.pages.home.cta');
    }
    public function update_call_action(Request $request){
        $this->validate($request, [
            'home_cta_text' => 'required|string',
            'home_cta_title' => 'required|string',
            'home_cta_btn_url' => 'required|string',
            'home_cta_bg' => 'mimes:jpg,jpeg,png|max:2064',
        ]);

        if ($request->hasFile('home_cta_bg')) {
            $image = $request->home_cta_bg;
            $image_extenstion = $image->getClientOriginalExtension();
            $image_name = 'call-to-action-bg-' .rand(999,99999).'.'. $image_extenstion;
            if (check_image_extension($image)) {
                $image->move('assets/uploads/', $image_name);
                update_static_option('home_cta_bg', $image_name);
            }
        }
        update_static_option('home_cta_text',$request->home_cta_text);
        update_static_option('home_cta_title',$request->home_cta_title);
        update_static_option('home_cta_btn_url',$request->home_cta_btn_url);

        return redirect()->back()->with([
            'msg' => 'Call To Action Settings Updated..',
            'type' => 'success'
        ]);
    }

    public function news_area(){
        return view('backend.pages.home.news');
    }
    public function update_news_area(Request $request){
        $this->validate($request,[
            'news_update_section_title' => 'required',
            'news_update_number' => 'required',
        ]);

        update_static_option('news_update_number',$request->news_update_number);
        update_static_option('news_update_section_title',$request->news_update_section_title);
        return redirect()->back()->with([
            'msg' => 'News Area Settings Updated..',
            'type' => 'success'
        ]);
    }

    public function home_section_manage(){
        return view('backend.pages.home.home-section-manage');
    }
    public function update_home_section_manage(Request $request){
        $this->validate($request,[
           'home_donor_search_section_status' => 'nullable|string',
           'home_blood_donation_process_section_status' => 'nullable|string',
           'home_available_donor_section_status' => 'nullable|string',
           'home_recent_donor_section_status' => 'nullable|string',
           'home_health_benefit_section_status' => 'nullable|string',
           'home_our_volunteer_section_status' => 'nullable|string',
           'home_counterup_section_status' => 'nullable|string',
           'home_testimonial_section_status' => 'nullable|string',
           'home_call_to_action_section_status' => 'nullable|string',
           'home_request_for_blood_section_status' => 'nullable|string',
           'home_news_update_section_status' => 'nullable|string',
        ]);
        update_static_option('home_donor_search_section_status',$request->home_donor_search_section_status);
        update_static_option('home_blood_donation_process_section_status',$request->home_blood_donation_process_section_status);
        update_static_option('home_available_donor_section_status',$request->home_available_donor_section_status);
        update_static_option('home_recent_donor_section_status',$request->home_recent_donor_section_status);
        update_static_option('home_health_benefit_section_status',$request->home_health_benefit_section_status);
        update_static_option('home_our_volunteer_section_status',$request->home_our_volunteer_section_status);
        update_static_option('home_counterup_section_status',$request->home_counterup_section_status);
        update_static_option('home_testimonial_section_status',$request->home_testimonial_section_status);
        update_static_option('home_call_to_action_section_status',$request->home_call_to_action_section_status);
        update_static_option('home_news_update_section_status',$request->home_news_update_section_status);
        update_static_option('home_request_for_blood_section_status',$request->home_request_for_blood_section_status);

        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        Artisan::call('cache:clear');

        return redirect()->back()->with(['msg' => 'Section Manage Settings Updated.....','type' => 'success']);
    }

    public function about_section_manage(){
        return view('backend.pages.home.about-section-manage');
    }
    public function update_about_section_manage(Request $request){

        $this->validate($request,[
           'about_page_testimonial_section_status' => 'nullable|string',
           'about_page_counterup_section_status' => 'nullable|string',
           'about_page_volunteer_section_status' => 'nullable|string',
           'about_page_why_donate_section_status' => 'nullable|string',
           'about_page_about_us_section_status' => 'nullable|string',
        ]);

        update_static_option('about_page_testimonial_section_status',$request->about_page_testimonial_section_status);
        update_static_option('about_page_counterup_section_status',$request->about_page_counterup_section_status);
        update_static_option('about_page_volunteer_section_status',$request->about_page_volunteer_section_status);
        update_static_option('about_page_why_donate_section_status',$request->about_page_why_donate_section_status);
        update_static_option('about_page_about_us_section_status',$request->about_page_about_us_section_status);

        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        Artisan::call('cache:clear');

        return redirect()->back()->with(['msg' => 'Section Manage Settings Updated.....','type' => 'success']);
    }
    public function recent_blood_request(){
        return view('backend.pages.home.recent-request-for-blood');
    }

    public function update_recent_blood_request(Request $request){
        $this->validate($request,[
            'home_recent_request_for_blood_section_title' => 'nullable|string',
            'home_recent_request_for_blood_number' => 'nullable|string',
        ]);
        update_static_option('home_recent_request_for_blood_number',$request->home_recent_request_for_blood_number);
        update_static_option('home_recent_request_for_blood_section_title',$request->home_recent_request_for_blood_section_title);

        return redirect()->back()->with([
            'msg' => 'Settings Update Success....',
            'type' => 'success'
        ]);
    }
}
