<?php

namespace App\Http\Controllers;

use App\Donor;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $all_testimonial = Testimonial::all();
        return view('backend.pages.testimonial')->with([
            'all_testimonial' => $all_testimonial
        ]);
    }
    public function store(Request $request){
        $this->validate($request,[
           'name' => 'required|string|max:191',
           'description' => 'required',
           'rating' => 'string|max:191',
           'designation' => 'string|max:191',
           'image' => 'mimes:jpg,jpeg,png|max:2064',
        ]);
        $id = Testimonial::create([
            'name' => $request->name,
            'description' => $request->description,
            'rating' => $request->rating,
            'designation' => $request->designation
        ])->id;

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'testimonial-pic-'.$id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/testimonial',$image_name);
                Testimonial::where('id',$id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'New Testimonial Added Success','type' => 'success']);
    }

    public function update(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'description' => 'required',
            'rating' => 'string|max:191',
            'designation' => 'string|max:191',
        ]);
         Testimonial::find($request->id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'rating' => $request->rating,
            'designation' => $request->designation
        ]);

        if ($request->hasFile('image')){
            $image = $request->image;
            $image_extenstion = $request->image->getClientOriginalExtension();
            $image_name = 'testimonial-pic-'.$request->id.'.'.$image_extenstion;
            if (check_image_extension($request->image)){
                $image->move('assets/uploads/testimonial',$image_name);
                Testimonial::where('id',$request->id)->update(['image'=>$image_extenstion]);
            }
        }

        return redirect()->back()->with(['msg' => 'Testimonial Update Success','type' => 'success']);
    }

    public function delete(Request $request,$id){
        Testimonial::find($id)->delete();
        return redirect()->back()->with(['msg' => 'Testimonial Delete Success','type' => 'danger']);
    }
}
