<nav class="navbar navbar-area navbar-expand-lg">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper">
                <a href="{{url('/')}}" class="logo">
                    @if(file_exists('assets/uploads/'.get_static_option('site_logo')))
                        <img src="{{asset('assets/uploads/'.get_static_option('site_logo'))}}" alt="site logo">
                    @endif
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#weforyou_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="weforyou_main_menu">
            <ul class="navbar-nav">
                <li class="{{active_menu_frontend('/')}}"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                <li class="{{active_menu_frontend('donors')}}"><a href="{{route('frontend.donors')}}">{{__('Donors')}}</a></li>
                <li class="{{active_menu_frontend('about')}}"><a href="{{route('frontend.about')}}">{{__('About')}}</a></li>
                <!-- <li class="{{active_menu_frontend('volunteer')}}"><a href="{{route('frontend.volunteer')}}">{{__('Volunteer')}}</a></li> -->
                
               <li class="{{active_menu_frontend('blog')}}"><a href="{{route('frontend.blog')}}">{{__('Blog')}}</a></li> 
                <li class="{{active_menu_frontend('contact')}}"><a href="{{route('frontend.contact')}}">{{__('Contact')}}</a></li>
                @if(Auth::check())
                <li class="menu-item-has-children">
                    <a href="javascript:void(0)">{{auth()->user()->username}}</a>
                    <ul class="sub-menu">
                        @if(is_donor())
                            <li><a href="{{route('donor.dashboard')}}">{{__('Dashboard')}}</a></li>
                            <li><a href="{{route('donor.edit.profile')}}">{{__('Edit Profile')}}</a></li>
                            <li><a href="{{route('donor.edit.donor.info')}}">{{__('Edit Donor Info')}}</a></li>
                            <li><a href="{{route('user.change.password')}}">{{__('Change Password')}}</a></li>
                        @else
                        <li><a href="{{route('user.dashboard')}}">{{__('Dashboard')}}</a></li>
                        <li><a href="{{route('user.edit.profile')}}">{{__('Edit Profile')}}</a></li>
                        <li><a href="{{route('user.change.password')}}">{{__('Change Password')}}</a></li>
                        <li><a href="{{route('user.join.donor')}}">{{__('Join As Donor')}}</a></li>
                        @endif
                       <li>
                           <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                               {{ __('Logout') }}
                           </a>
                       </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </li>
                @else
                <li class="menu-btn"><a href="{{route('frontend.join.donor')}}">{{__('Join as Donor')}}</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
