<?php $__env->startSection('site-title'); ?>
    <?php echo e(__('Dashboard')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="main-content-inner">
        <div class="row">
            <!-- seo fact area start -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4 mt-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg1">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-thumb-up"></i> <?php echo e(__('Total Donors')); ?></div>
                                    <h2><?php echo e($total_donor); ?></h2>
                                </div>
                                <canvas id="seolinechart1" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg2">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-share"></i> <?php echo e(__('Total Volunteer Member')); ?></div>
                                    <h2><?php echo e($total_volunteer_member); ?></h2>
                                </div>
                                <canvas id="seolinechart2" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-5 mb-3">
                        <div class="card">
                            <div class="seo-fact sbg3">
                                <div class="p-4 d-flex justify-content-between align-items-center">
                                    <div class="seofct-icon"><i class="ti-share"></i> <?php echo e(__('Total Blood Donate')); ?></div>
                                    <h2><?php echo e($total_blood_donate); ?></h2>
                                </div>
                                <canvas id="seolinechart2" height="50"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- seo fact area end -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title"><?php echo e(__('Last 10 Blood Donation List')); ?></h4>
                        <div class="single-table">
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th><?php echo e(__('ID')); ?></th>
                                        <th><?php echo e(__('Donor Name')); ?></th>
                                        <th><?php echo e(__('Blood Group')); ?></th>
                                        <th><?php echo e(__('Receiver Name')); ?></th>
                                        <th><?php echo e(__('Receiver Address')); ?></th>
                                        <th><?php echo e(__('Receiver Mobile')); ?></th>
                                        <th><?php echo e(__('Date')); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $all_blood_logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($data->id); ?></td>
                                            <td><?php echo e($data->name); ?></td>
                                            <td><?php echo e(strtoupper($data->blood_group)); ?></td>
                                            <td><?php echo e($data->receiver_name); ?></td>
                                            <td><?php echo e($data->receiver_address); ?></td>
                                            <td><?php echo e($data->receiver_mobile); ?></td>
                                            <td><?php echo e($data->created_at->diffForHumans()); ?></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/shopijbr/blood-donation.weavingwords.in/@core/resources/views/backend/admin-home.blade.php ENDPATH**/ ?>