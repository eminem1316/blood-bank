@extends('frontend.frontend-master')

@section('content')
    <header class="header-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="left-content-area">
                        <h1 class="title">{{get_static_option('home_page_header_title')}}</h1>
                        <p>{{get_static_option('home_page_header_description')}}</p>
                        <div class="btn-wrapper">
                            <a href="{{get_static_option('home_page_header_btn_one_url')}}" class="boxed-btn">{{get_static_option('home_page_header_btn_one_text')}}</a>
                            <a href="{{get_static_option('home_page_header_btn_two_url')}}" class="boxed-btn blank">{{get_static_option('home_page_header_btn_two_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-area">
                        <div class="img-wrapper">
                            @if(file_exists('assets/uploads/'.get_static_option('home_page_header_right_image')))
                                <img src="{{asset('assets/uploads/'.get_static_option('home_page_header_right_image'))}}"  alt="{{get_static_option('home_page_header_title')}}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if(!empty(get_static_option('home_donor_search_section_status')))
    @include('frontend.partials.donor-search-form')
    @endif
    @if(!empty(get_static_option('home_blood_donation_process_section_status')))
    <section class="blood-donation-process-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('blood_donation_process_section_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="blood-donation-process">
                        <ul class="donation-process-list">
                            @foreach($all_donate_process as $data)
                            <li class="single-donation-process">
                                <span class="number">{{$data->number}}</span>
                                <h4 class="title">{{$data->title}}</h4>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @if(!empty(get_static_option('home_available_donor_section_status')))
    <!-- our valuable donor area start  -->
    <section class="our-valuable-donor-area padding-bottom-90">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('valuable_donors_section_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">A+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','a+')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">A-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','a-')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">O+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','o+')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">O-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','o-')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">B+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','b+')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">B-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','b-')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">AB+</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','ab+')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-donor-group-item">
                        <div class="icon">
                            <span class="blood-group">AB-</span>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('frontend.donors.by.blood.group','ab-')}}" class="boxed-btn">{{get_static_option('valuable_donors_button_text')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- our valuable donor area end  -->
    @endif
    @if(!empty(get_static_option('home_request_for_blood_section_status')))
    <div class="recently-requested-area padding-bottom-90">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('home_recent_request_for_blood_section_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="recent-requested-blood-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{__('Name')}}</th>
                                <th scope="col">{{__('Email')}}</th>
                                <th scope="col">{{__('Blood Group')}}</th>
                                <th scope="col">{{__('Number Of Units')}}</th>
                                <th scope="col">{{__('Illness')}}</th>
                                <th scope="col">{{__('Contact Number')}}</th>
                                <th scope="col">{{__('Hospital Address')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_blood_request as $data)
                            <tr>
                                <th scope="row">{{$data->name}}</th>
                                <td>{{$data->email}}</td>
                                <td>{{$data->blood_group}}</td>
                                <td>{{$data->number_of_units}}</td>
                                <td>{{$data->illness}}</td>
                                <td>{{$data->mobile}}</td>
                                <td>{{$data->hospital_address}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(!empty(get_static_option('home_recent_donor_section_status')))
    <!-- recently Donated memeber start -->
    <div class="recently-donated-donors padding-bottom-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('recent_donors_section_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="recently-donated-donors-carousel owl-carousel">
                        @foreach($all_recent_donors as $data)
                        <div class="single-donors-item">
                            <div class="thumb">
                                @if(file_exists('assets/uploads/donors/donor-pic-'.$data->donor->id.'.'.$data->donor->image))
                                    <img style="max-width: 300px;" src="{{asset('assets/uploads/donors/donor-pic-'.$data->donor->id.'.'.$data->donor->image)}}" alt="{{__($data->donor->name)}}">
                                @else
                                    <img style="max-width: 300px" src="{{asset('assets/uploads/no-image.png')}}" alt="no image available">
                                @endif
                            </div>
                            <div class="content">
                                <a href="{{route('frontend.donors.profile',['id' => $data->donor->id,'slug' => Str::slug($data->donor->user->name)])}}"><h4 class="title">{{$data->donor->user->name}}</h4></a>
                                <span class="blood-group">{{__('Blood Group:')}} <strong>{{strtoupper($data->donor->blood_group)}}</strong></span>
                                <span class="total-donate">{{__('Total Donate:')}} <strong>{{get_total_donate($data->donor->id)}}</strong> {{__('Times')}}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- recently Donated memeber end -->
    @endif
    @if(!empty(get_static_option('home_health_benefit_section_status')))
    <!-- benifits-of-donation area start  -->
    <section class="benefit-of-donation-area  gray-bg padding-top-120 padding-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="single-benefit-item">
                        <div class="left-content-area">
                            {!! get_static_option('health_benefit_first_item_content') !!}
                        </div>
                        <div class="right-content-area">
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('health_benefit_first_item_image')))
                                    <img src="{{asset('assets/uploads/'.get_static_option('health_benefit_first_item_image'))}}" alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="single-benefit-item">

                        <div class="left-content-area">
                            {!! get_static_option('health_benefit_second_item_content') !!}
                        </div>
                        <div class="right-content-area">
                            <div class="img-wrapper">
                                @if(file_exists('assets/uploads/'.get_static_option('health_benefit_second_item_image')))
                                    <img src="{{asset('assets/uploads/'.get_static_option('health_benefit_second_item_image'))}}" alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- benifits-of-donation area end -->
    @endif
    @if(!empty(get_static_option('home_our_volunteer_section_status')))
@include('frontend.partials.volunteers-area')
@endif
    @if(!empty(get_static_option('home_counterup_section_status')))
@include('frontend.partials.counterup')
@endif
    @if(!empty(get_static_option('home_testimonial_section_status')))
@include('frontend.partials.testimonial')
@endif
    @if(!empty(get_static_option('home_call_to_action_section_status')))
    @php $cta_bg_image_url = ''; @endphp
    @if(file_exists('assets/uploads/'.get_static_option('home_cta_bg')))
        @php  $cta_bg_image_url = asset('assets/uploads/'.get_static_option('home_cta_bg')); @endphp
    @endif
    <section class="call-to-action-area padding-100" style="background-image: url({{$cta_bg_image_url}});">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-inner">
                        <div class="left-content-area">
                            <h2 class="title">{{get_static_option('home_cta_title')}}</h2>
                        </div>
                        <div class="right-content-area">
                            <div class="btn-wrapper">
                                <a href="{{get_static_option('home_cta_btn_url')}}" class="boxed-btn">{{get_static_option('home_cta_text')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @if(!empty(get_static_option('home_news_update_section_status')))
    <section class="news-area padding-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title margin-bottom-60">
                        <h2 class="title">{{get_static_option('news_update_section_title')}}</h2>
                        <span class="separator"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($all_blog as $data)
                <div class="col-lg-4 col-md-6">
                    <div class="single-new-item">
                        <div class="thumb">
                            @if(file_exists('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image))
                                <img src="{{asset('assets/uploads/blog/blog-grid-'.$data->id.'.'.$data->image)}}" alt="{{$data->title}}">
                            @endif
                        </div>
                        <div class="content">
                            <span class="posted_by">{{__('Posted By ')}} {{$data->user->name}}</span>
                            <h4 class="title"><a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}">{{$data->title}}</a></h4>
                            <div class="description">
                                {!! Str::words($data->content,55) !!}
                            </div>
                            <a href="{{route('frontend.blog.single',['id' => $data->id, 'any' => Str::slug($data->title,'-')])}}" class="readmore">{{__('Read More')}}</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif
@endsection
